<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerbaikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perbaikans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gerbang');
            $table->string('cabang');
            $table->string('gardu');
            $table->time('w_datang');
            $table->time('w_gardu_mati');
            $table->time('w_gardu_hidup');
            $table->string('alat');
            $table->string('komponen');
            $table->string('kerusakan');
            $table->text('uraian_kerusakan');
            $table->text('uraian_perbaikan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perbaikans');
    }
}
