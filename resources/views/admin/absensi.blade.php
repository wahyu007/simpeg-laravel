@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Data Cuti Terbaru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah Cuti" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_pegawai'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    <br>
    <center>
      {{$newCutis->links('pagination.uk')}}
    </center>
    <div class="content-background">
    <div class="uk-section-large">
      <div class="uk-container uk-container-large">
        <div uk-grid class="uk-child-width-1-1@s uk-child-width-2-3@l">
          <div class="uk-width-1-1@s uk-width-1-5@l uk-width-1-3@xl"></div>
          <div class="uk-width-1-1@s uk-width-3-5@l uk-width-1-3@xl">
            @include('layouts.main.nav_menu')
          </div>
          <div class="uk-width-1-1@s uk-width-1-5@l uk-width-1-3@xl"></div>
          <div class="uk-width-1-1@s uk-width-1-5@l uk-width-1-3@xl"></div>
          <div class="uk-width-1-1@s uk-width-3-5@l uk-width-1-3@xl">
            <div class="uk-card uk-card-default uk-animation-slide-top">
              <div class="uk-card-body">
                <h4>Absensi Pegawai</h4>
                <form method="POST" action="{{route('post-absen')}}">
                  {{ csrf_field() }}
                  <fieldset class="uk-fieldset">
                    <div class="uk-margin">
                      <div class="uk-position-relative">
                        <span class="uk-form-icon ion-android-person"></span>
                        <select id="pegawai" name="pegawai_id">
                          <option value="">Find Your Name...</option>
                          @foreach($pegawais as $p)
                          <option value="{{$p->nik}}">{{$p->nama}} _ID : {{$p->nik}}</option>
                          @endforeach
                        </select>
                        <script>
                          new SlimSelect({
                            select: '#pegawai'
                          })

                          function handle(e){
                            if(e.keyCode === 13){
                                e.preventDefault(); // Ensure it is only this code that rusn
                                alert("Enter was pressed was presses");
                            }
                        }
                        </script>
                      </div>
                    </div>
                    <div class="uk-margin">
                      <button type="submit" class="uk-button uk-button-primary">
                        <span class="ion-forward"></span>&nbsp; Submit
                      </button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
          {{--  <hr class="uk-divider-icon"> --}}
          <div class="uk-width-1-1@s uk-width-1-5@l uk-width-1-3@xl"></div> 
        </div>
      </div>
    </div>
  </div>
  <script src="{{asset('js/script.js')}}"></script>
  @if(Session::has('sudah_absen'))
  <script>
    swal({
      title: "Maaf!",
      text: "Hari ini Anda Sudah Absen!",
      icon: "warning",
      button: "ok!",
    });
  </script>
  @elseif(Session::has('gagal_absen'))
  <script>
    swal({
      title: "Maaf!",
      text: "Gagal Absen!",
      icon: "error",
      button: "ok!",
    });
  </script>
  @elseif(Session::has('sukses_absen'))
  <script>
    swal({
      title: "Terimakasih!",
      text: "Absen Berhasil!",
      icon: "success",
      button: "ok!",
    });
  </script>
  @elseif(Session::has('req_error'))
  <script>
    swal({
      title: "Maaf",
      text: "ID tidak ditemukan!",
      icon: "info",
      button: "ok!",
    });
  </script>
  @endif
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('absen_errval'))
  @if($errors->has('pegawai_id'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('absen_notfound'))
  swal("warning!", "ID absen tidak ada.", "info");
  @elseif(Session::has('absen_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('absen_created'))
  swal("sukses!", "absen berhasil ditambahkan.", "success");
  @elseif(Session::has('absen_failed_creared'))
  swal("Maaf!", "gagal menambahkan data absen.", "error");

  @elseif(Session::has('absen_success_updated'))
  swal("Berhasil!", "data absen berhasil di ubah.", "success");
  @elseif(Session::has('absen_failed_updated'))
  swal("Maaf!", "data absen gagal di ubah.", "error");

  @elseif(Session::has('absen_success_deleted'))
  swal("Berhasil!", "data absen berhasil dihapus.", "success");
  @elseif(Session::has('absen_failed_deleted'))
  swal("Maaf!", "data Pegawai gagal dihapus.", "error");
  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection