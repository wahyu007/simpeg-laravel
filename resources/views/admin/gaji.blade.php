@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
    <div id="page_content">
        <div id="page_content_inner">
            <div>
                <h4 class="heading_a uk-margin-bottom">List gaji
                <a href="{{route('get-gaji-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
                    <i class="md-icon material-icons uk-text-primary">cloud_download</i>
                </a>
                </h4>
                <div class="md-card uk-margin-medium-bottom">
                    <div class="md-card-content">
                    <table id="dt_tableTools" class="uk-table uk-table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Total Absen</th>
                        <th>Gaji Pokok</th>
                        <th>Tunjangan</th>
                        <th>Potongan</th>
                        <th>Pendapatan</th>
                        <th>Tanggal</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($gaji as $ins)
                        <tr>
                        <td>{{$ins->rekapAbsen->pegawai->id_pegawai}}</td>
                        <td>{{$ins->rekapAbsen->pegawai->nama}}</td>
                        <td> <span class="uk-badge">{{$ins->rekapAbsen->total}} hari</span></td>
                        <td>Rp. {{ number_format($ins->gaji_pokok,2)}}</td>
                        <td>Rp. {{number_format($ins->tunjangan,2)}}</td>
                        <td>Rp. {{number_format($ins->potongan,2)}}</td>
                        <td>Rp. {{number_format($ins->pendapatan,2)}}</td>
                        <td>{{$ins->updated_at->toDateString()}}</td>
                        <td>
                            <a data-uk-tooltip="{pos:'top'}" title="Lihat gaji" href="{{route('get-gaji-view',$ins->id)}}">
                                <i class="md-icon material-icons uk-text-info">remove_red_eye</i>
                            </a>
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<script>
  @if(Session::has('mobil_errval'))
  @if($errors->has('mobil'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('mobil_notfound'))
  swal("warning!", "ID mobil tidak ada.", "info");
  @elseif(Session::has('mobil_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('mobil_created'))
  swal("sukses!", "mobil berhasil ditambahkan.", "success");
  @elseif(Session::has('mobil_failed_creared'))
  swal("Maaf!", "gagal menambahkan data mobil.", "error");

  @elseif(Session::has('mobil_success_updated'))
  swal("Berhasil!", "data mobil berhasil di ubah.", "success");
  @elseif(Session::has('mobil_failed_updated'))
  swal("Maaf!", "data mobil gagal di ubah.", "error");

  @elseif(Session::has('mobil_success_deleted'))
  swal("Berhasil!", "data mobil berhasil dihapus.", "success");
  @elseif(Session::has('mobil_failed_deleted'))
  swal("Maaf!", "data mobil gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection