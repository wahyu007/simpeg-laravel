@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-7-10">
                    <div class="md-card">
                        <div class="user_heading">
                            <div class="user_heading_avatar">
                                <div class="thumbnail">
                                    <img src="{{asset('images/avatar_01.png')}}" alt="user avatar"/>
                                </div>
                            </div>
                            @foreach($pegawai as $p)
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate">{{$p->nama}}</span><span class="sub-heading">Teknisi Pemeliharaan</span></h2>
                            </div>
                            <a class="md-fab md-fab-small md-fab-accent" href="#">
                                <i class="material-icons">&#xE150;</i>
                            </a>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">About</a></li>
                                <!-- <li><a href="#">Photos</a></li>
                                <li><a href="#">Posts</a></li> -->
                            </ul>
                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                                                       
                                    <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Detail Pegawai</h4>
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{$p->nik}}</span>
                                                        <span class="uk-text-small uk-text-muted">NIK</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{$p->nama}}</span>
                                                        <span class="uk-text-small uk-text-muted">Nama</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-chevron-right"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{$p->no_ktp}}</span>
                                                        <span class="uk-text-small uk-text-muted">No KTP</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-chevron-right"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{$p->alamat}}</span>
                                                        <span class="uk-text-small uk-text-muted">Alamat</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{$p->agama}}</span>
                                                        <span class="uk-text-small uk-text-muted">Agama</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-chevron-right"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{$p->jenis_kelamin}}</span>
                                                        <span class="uk-text-small uk-text-muted">Jenis Kelamin</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-chevron-right"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{$p->tgl_lahir}}</span>
                                                        <span class="uk-text-small uk-text-muted">Tanggal Lahir</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        @endforeach
                                        @foreach($user as $usr)
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Details User</h4>
                                            <!-- <br> -->
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{$usr->email}}</span>
                                                        <span class="uk-text-small uk-text-muted">Email</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-chevron-right"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">@if($usr->role == 1) Admin @else User @endif</span>
                                                        <span class="uk-text-small uk-text-muted">role</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        @endforeach
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- <div class="uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-margin-medium-bottom">
                                <h3 class="heading_c uk-margin-bottom">Alerts</h3>
                                <ul class="md-list md-list-addon">
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">Nostrum praesentium.</span>
                                            <span class="uk-text-small uk-text-muted uk-text-truncate">Soluta sed neque rem error perspiciatis.</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">Ut ullam.</span>
                                            <span class="uk-text-small uk-text-muted uk-text-truncate">Illum tempora aperiam modi est aut.</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-addon-element">
                                            <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                        </div>
                                        <div class="md-list-content">
                                            <span class="md-list-heading">Sit sed aut.</span>
                                            <span class="uk-text-small uk-text-muted uk-text-truncate">Hic voluptas ut harum accusamus ea.</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <h3 class="heading_c uk-margin-bottom">Friends</h3>
                            <ul class="md-list md-list-addon uk-margin-bottom">
                                <li>
                                    <div class="md-list-addon-element">
                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_02_tn.png" alt=""/>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Miss Madeline O'Hara IV</span>
                                        <span class="uk-text-small uk-text-muted">Quia doloremque corrupti delectus.</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_07_tn.png" alt=""/>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Mrs. Alverta Roberts</span>
                                        <span class="uk-text-small uk-text-muted">Enim ipsa voluptatem et doloremque.</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_06_tn.png" alt=""/>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Nya Schiller IV</span>
                                        <span class="uk-text-small uk-text-muted">Molestias excepturi dolor consequatur.</span>
                                    </div>
                                </li>
                            </ul>
                            <a class="md-btn md-btn-flat md-btn-flat-primary" href="#">Show all</a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
{{-- update user modal --}} @if(Session::has('user_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_user" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-user-update-id',Session('usera')->id)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update User</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nama">Username</label>
          <input required  value="{{Session('usera')->name}}" class="
                  md-input 
                  {{$errors->has('usera') ? ' md-input-danger' : ''}}
                  " type="text" id="nama" name="nama" />
        </div>
        <div class="uk-form-row">
          <label for="email">Email</label>
          <input required  value="{{Session('usera')->email}}" class="
                  md-input 
                  {{$errors->has('usera') ? ' md-input-danger' : ''}}
                  " type="email" id="email" name="email" />
        </div>
        <div class="uk-form-row">
          <label for="password">Password</label>
          <input class="
                  md-input 
                  {{$errors->has('usera') ? ' md-input-danger' : ''}}
                  " type="password" id="password" name="password" />
          <input type="hidden" name="password_old" value="{{Session('usera')->password}}">
        </div>
        <div class="uk-form-row">
          <select name="pegawai_id" class="uk-select">
            <option value="{{Session('usera')->pegawai_id}}">Select Pegawai</option>
            @foreach($pegawai as $p)
            <option value="{{$p->id}}">{{$p->nama}}</option>
            @endforeach
          </select>
          <select name="role" class="uk-select">
            <option value="{{Session('usera')->role}}">Select Role</option>
            <option value="1">Admin</option>
            <option value="2">User</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_user()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_user() {
              $('#update_user').hide()
              console.log("hidden");
            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update user modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_user" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-user')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah user</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nama">Username</label>
          <input required   class="
                  md-input 
                  {{$errors->has('user') ? ' md-input-danger' : ''}}
                  " type="text" id="nama" name="nama" />
        </div>
        <div class="uk-form-row">
          <label for="email">Email</label>
          <input required   class="
                  md-input 
                  {{$errors->has('user') ? ' md-input-danger' : ''}}
                  " type="email" id="email" name="email" />
        </div>
        <div class="uk-form-row">
          <label for="password">Password</label>
          <input required   class="
                  md-input 
                  {{$errors->has('user') ? ' md-input-danger' : ''}}
                  " type="password" id="password" name="password" />
        </div>
        <div class="uk-form-row">
          <label for="pegawai_id">Pegawai ID</label>
        </div>
        <div class="uk-margin">
          <select name="pegawai_id" class="uk-select">
            <option value="">Select Pegawai</option>
            @foreach($pegawai as $p)
            <option value="{{$p->id}}">{{$p->nama}}</option>
            @endforeach
          </select>
          <select name="role" class="uk-select">
            <option value="">Select Role</option>
            <option value="1">Admin</option>
            <option value="2">User</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('user_errval'))
//   @if($errors->has('user'))
  swal("Warning!", "Error Request!", "warning");
  @endif
  @elseif(Session::has('user_notfound'))
  swal("warning!", "ID user tidak ada.", "info");
  @elseif(Session::has('user_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('user_created'))
  swal("sukses!", "user berhasil ditambahkan.", "success");
  @elseif(Session::has('user_failed_creared'))
  swal("Maaf!", "gagal menambahkan data user.", "error");

  @elseif(Session::has('user_success_updated'))
  swal("Berhasil!", "data user berhasil di ubah.", "success");
  @elseif(Session::has('user_failed_updated'))
  swal("Maaf!", "data user gagal di ubah.", "error");

  @elseif(Session::has('user_success_deleted'))
  swal("Berhasil!", "data user berhasil dihapus.", "success");
  @elseif(Session::has('user_failed_deleted'))
  swal("Maaf!", "data user gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection