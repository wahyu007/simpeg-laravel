@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
    <div id="page_content_inner">
    <!-- statistics (small charts) -->
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a">
                    Tambah perbaikan
                </h3>
                <br>
                @foreach($perbaikan as $p)
                <form action="{{route('get-perbaikan-update',$p->id)}}" method="POST">
                    <div class="uk-grid" data-uk-grid-margin>
                        {{ csrf_field() }}
                        <div class="uk-width-medium-1-3 uk-width-1-1">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-database"></i></span>
                                <label for="cabang">Cabang</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="cabang" 
                                    name="cabang" value="{{$p->cabang}}"/>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-home"></i></span>
                                <label for="gerbang">Gerbang</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="gerbang" 
                                    name="gerbang" value="{{$p->gerbang}}" />
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-desktop"></i></span>
                                <label for="gardu">Gardu</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="gardu" 
                                    name="gardu" value="{{$p->gardu}}" />
                            </div>
                        </div>
                        <div class="uk-width-large-1-3 uk-width-1-1">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="error">Waktu Gardu Error</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="waktu_error" 
                                    name="waktu_error" data-uk-timepicker value="{{Carbon\carbon::parse($p->w_gardu_mati)->format('H:i')}}"/>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="datang">Waktu Datang</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="waktu_datang" 
                                    name="waktu_datang" data-uk-timepicker value="{{Carbon\carbon::parse($p->w_datang)->format('H:i')}}" />
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="waktu_selesai">Waktu Selesai</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="waktu_selesai" 
                                    name="waktu_selesai" data-uk-timepicker value="{{Carbon\carbon::parse($p->w_gardu_hidup)->format('H:i')}}" />
                            </div>
                        </div>
                        <div class="uk-width-large-1-3 uk-width-1-1">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-tablet"></i></span>
                                <select id="select_demo_1" name="alat" data-md-selectize>
                                    <option value="{{$p->alat}}">{{$p->alat}}</option>
                                    @foreach($alat as $alt)
                                    <option value="{{$alt->nama_alat}}">{{$alt->nama_alat}}</option>
                                    @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-cog"></i></span>
                                <label for="komponen">Komponen</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="komponen" 
                                    name="komponen" value="{{$p->komponen}}" />
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-close"></i></span>
                                <label for="kerusakan">Kerusakan</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="kerusakan" 
                                    name="kerusakan" value="{{$p->kerusakan}}" />
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="uk-grid">
                        <div class="uk-width-medium">
                            <div class="uk-grid">
                                <div class="uk-width-small-1-2">
                                    <label for="uraian_kerusakan">Uraian Kerusakan</label>
                                    <textarea
                                        cols="30" rows="4"
                                        class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                        id="uraian_kerusakan" 
                                        name="uraian_kerusakan" >{{$p->uraian_kerusakan}}
                                    </textarea>
                                </div>
                                <div class="uk-width-small-1-2">
                                    <label for="uraian_perbaikan">Uraian Perbaikan</label>
                                    <textarea 
                                        cols="30" rows="4"
                                        class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                        id="uraian_perbaikan" 
                                        name="uraian_perbaikan" > {{$p->uraian_perbaikan}}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid ">
                        <div class="uk-width-medium" >
                            <a href="{{ URL::previous()}}">
                                <button type="button" class="uk-align-right md-btn md-btn-flat md-btn-flat-danger">Cancel</button>
                            </a>
                            <button type="submit" class="uk-align-right md-btn md-btn-flat md-btn-primary ">Save</button>
                        </div>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>

{{-- end update modal --}}
<script>
  @if(Session::has('perbaikan_errval'))
  @if($errors->has('perbaikan'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('perbaikan_notComplete'))
  swal("Maaf!", "Uraian Tidak Boleh Kosong.", "error");
  @elseif(Session::has('perbaikan_notMatch'))
  swal("Maaf!", "Waktu Perbaikan Tidak Sesuai.", "error");
  @elseif(Session::has('perbaikan_notfound'))
  swal("warning!", "ID perbaikan tidak ada.", "info");
  @elseif(Session::has('perbaikan_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('perbaikan_created'))
  swal("sukses!", "perbaikan berhasil ditambahkan.", "success");
  @elseif(Session::has('perbaikan_failed_creared'))
  swal("Maaf!", "gagal menambahkan data perbaikan.", "error");

  @elseif(Session::has('perbaikan_success_updated'))
  swal("Berhasil!", "data perbaikan berhasil di ubah.", "success");
  @elseif(Session::has('perbaikan_failed_updated'))
  swal("Maaf!", "data perbaikan gagal di ubah.", "error");

  @elseif(Session::has('perbaikan_success_deleted'))
  swal("Berhasil!", "data perbaikan berhasil dihapus.", "success");
  @elseif(Session::has('perbaikan_failed_deleted'))
  swal("Maaf!", "data perbaikan gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection