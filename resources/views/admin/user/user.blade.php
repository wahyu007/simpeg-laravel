@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Data user Terbaru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah user" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_user'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    <h4 class="heading_a uk-margin-bottom">List user
      <a href="#" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>username</th>
              <th>Email</th>
              <th>Pegawai ID</th>
              <th>Role</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($user as $ins)
            <tr>
              <td>{{$ins->id}}</td>
              <td>{{$ins->name}}</td>
              <td>{{$ins->email}}</td>
              <td>{{$ins->pegawai_id}}</td>
              @if($ins->role == 1)
                <td>Admin</td>
              @else
                <td>User</td>
              @endif
              <td class="uk-text-center">
                <a data-uk-tooltip="{pos:'top'}" title="Update user" href="{{route('get-user',$ins->id)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus user" href="{{route('get-user-delete-id',$ins->id)}}">
                  <i class="md-icon material-icons uk-text-danger">remove_circle</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update user modal --}} @if(Session::has('user_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_user" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-user-update-id',Session('usera')->id)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update User</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nama">Username</label>
          <input required  value="{{Session('usera')->name}}" class="
                  md-input 
                  {{$errors->has('usera') ? ' md-input-danger' : ''}}
                  " type="text" id="nama" name="nama" />
        </div>
        <div class="uk-form-row">
          <label for="email">Email</label>
          <input required  value="{{Session('usera')->email}}" class="
                  md-input 
                  {{$errors->has('usera') ? ' md-input-danger' : ''}}
                  " type="email" id="email" name="email" />
        </div>
        <div class="uk-form-row">
          <label for="password">Password</label>
          <input class="
                  md-input 
                  {{$errors->has('usera') ? ' md-input-danger' : ''}}
                  " type="password" id="password" name="password" />
          <input type="hidden" name="password_old" value="{{Session('usera')->password}}">
        </div>
        <div class="uk-form-row">
          <select name="pegawai_id" class="uk-select">
            <option value="{{Session('usera')->pegawai_id}}">Select Pegawai</option>
            @foreach($pegawai as $p)
            <option value="{{$p->id}}">{{$p->nama}}</option>
            @endforeach
          </select>
          <select name="role" class="uk-select">
            <option value="{{Session('usera')->role}}">Select Role</option>
            <option value="1">Admin</option>
            <option value="2">User</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_user()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_user() {
              $('#update_user').hide()
              console.log("hidden");
            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update user modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_user" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-user')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah user</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nama">Username</label>
          <input required   class="
                  md-input 
                  {{$errors->has('user') ? ' md-input-danger' : ''}}
                  " type="text" id="nama" name="nama" />
        </div>
        <div class="uk-form-row">
          <label for="email">Email</label>
          <input required   class="
                  md-input 
                  {{$errors->has('user') ? ' md-input-danger' : ''}}
                  " type="email" id="email" name="email" />
        </div>
        <div class="uk-form-row">
          <label for="password">Password</label>
          <input required   class="
                  md-input 
                  {{$errors->has('user') ? ' md-input-danger' : ''}}
                  " type="password" id="password" name="password" />
        </div>
        <div class="uk-form-row">
          <label for="pegawai_id">Pegawai ID</label>
        </div>
        <div class="uk-margin">
          <select name="pegawai_id" class="uk-select">
            <option value="">Select Pegawai</option>
            @foreach($pegawai as $p)
            <option value="{{$p->id}}">{{$p->nama}}</option>
            @endforeach
          </select>
          <select name="role" class="uk-select">
            <option value="">Select Role</option>
            <option value="1">Admin</option>
            <option value="2">User</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('user_errval'))
//   @if($errors->has('user'))
  swal("Warning!", "Error Request!", "warning");
  @endif
  @elseif(Session::has('user_notfound'))
  swal("warning!", "ID user tidak ada.", "info");
  @elseif(Session::has('user_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('user_created'))
  swal("sukses!", "user berhasil ditambahkan.", "success");
  @elseif(Session::has('user_failed_creared'))
  swal("Maaf!", "gagal menambahkan data user.", "error");

  @elseif(Session::has('user_success_updated'))
  swal("Berhasil!", "data user berhasil di ubah.", "success");
  @elseif(Session::has('user_failed_updated'))
  swal("Maaf!", "data user gagal di ubah.", "error");

  @elseif(Session::has('user_success_deleted'))
  swal("Berhasil!", "data user berhasil dihapus.", "success");
  @elseif(Session::has('user_failed_deleted'))
  swal("Maaf!", "data user gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection