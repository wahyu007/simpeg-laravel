@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Data Mobil Terbaru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah mobil" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_mobil'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    <div class="uk-grid uk-grid-width-large-1-6 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show"
      data-uk-sortable data-uk-grid-margin>
      @php $i=1; $colors = ['pink','teal','red','cyan','orange','teal']; @endphp @foreach($newmobils as $np)
      <div>
        <div class="md-card md-card-hover md-card-overlay" data-snippet-title="Smooth scrolling to top of page">
          <div class="md-card-content md-bg-light-blue-800 uk-text-left " style="cursor: pointer; ">
            <br>
            <br>
            <h3 class="uk-text-contrast">{{$np->mobil}}</h3>
            <hr class="divider">
          </div>
          <div class="md-card-overlay-content">
            <div class="uk-clearfix md-card-overlay-header">
              <i title="detail" data-uk-tooltip="{pos:'left'}" class="md-icon 
                  md-icon 
                  material-icons 
                  md-card-overlay-toggler"></i>
              <h4>
                <i class="material-icons md-20 uk-text-success">nature</i> ID : {{$np->id}}
              </h4>
            </div>
          </div>
        </div>
      </div>
      @php $i++; if($i>5){$i=1;}@endphp @endforeach
    </div>
    <br>
    <center>
      {{$newmobils->links('pagination.uk')}}
    </center>
    <h4 class="heading_a uk-margin-bottom">List mobil
      <a href="{{route('get-mobil-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nopol</th>
              <th>Odometer</th>
              <th>Merk</th>
              <th>Type</th>
              <th>Tahun</th>
              <th>Pajak</th>
              <th>Update</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($mobils as $ins)
            <tr>
              <td>{{$ins->id}}</td>
              <td>{{$ins->nopol}}</td>
              <td>{{$ins->odometer}}</td>
              <td>{{$ins->merk}}</td>
              <td>{{$ins->type}}</td>
              <td>{{$ins->tahun}}</td>
              <td>{{\Carbon\Carbon::parse($ins->status_pajak)->format('M')}}</td>
              <td>{{$ins->updated_at}}</td>
              <td class="uk-text-center">
                <a data-uk-tooltip="{pos:'top'}" title="Update mobil" href="{{route('get-mobil-update',$ins->id)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus mobil" href="{{route('get-mobil-delete',$ins->id)}}">
                  <i class="md-icon material-icons uk-text-danger">remove_circle</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update mobil modal --}} @if(Session::has('mobil_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_mobil" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('get-mobil-updated',Session('mobil')->id)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update mobil</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nopol">nopol mobil</label>
          <input required value="{{Session('mobil')->nopol}}" class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="text" id="nopol" name="nopol" />
        </div>
        <div class="uk-form-row">
          <label for="odometer">odometer</label>
          <input required  value="{{Session('mobil')->odometer}}" class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="number" id="odometer" name="odometer" />
        </div>
        <div class="uk-form-row">
          <label for="merk">Merk</label>
          <input required  value="{{Session('mobil')->merk}}" class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="text" id="merk" name="merk" />
        </div>
        <div class="uk-form-row">
          <label for="type">Type</label>
          <input required  value="{{Session('mobil')->type}}" class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="text" id="type" name="type" />
        </div>
        <div class="uk-form-row">
          <label for="tahun">Tahun</label>
          <input required  value="{{Session('mobil')->tahun}}" class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="number" id="tahun" name="tahun"  />
        </div>
        <div class="uk-form-row">
          <label for="pajak">Pajak</label>
          <input required value="{{\Carbon\Carbon::parse(Session('mobil')->pajak)->format('yy-m-d')}}"class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="date" id="pajak" name="pajak" />
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_mobil()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_mobil() {
              $('#update_mobil').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update mobil modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_mobil" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-mobil-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah mobil</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nopol">Nopol mobil</label>
          <input required class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="text" id="nopol" name="nopol" />
        </div>
        <div class="uk-form-row">
          <label for="odometer">Odometer</label>
          <input required class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="number" id="odometer" name="odometer" />
        </div>
        <div class="uk-form-row">
          <label for="merk">Merk</label>
          <input required class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="text" id="merk" name="merk" />
        </div>
        <div class="uk-form-row">
          <label for="type">Type</label>
          <input required  class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="text" id="type" name="type" />
        </div>
        <div class="uk-form-row">
          <label for="tahun">Tahun</label>
          <input required class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="number" id="tahun" name="tahun" />
        </div>
        <div class="uk-form-row">
          <label for="pajak">Pajak</label>
          <input required class="
                  md-input 
                  {{$errors->has('mobil') ? ' md-input-danger' : ''}}
                  " type="date" id="pajak" name="pajak" value="2019-01-01"/>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('mobil_errval'))
  @if($errors->has('mobil'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('mobil_notfound'))
  swal("warning!", "ID mobil tidak ada.", "info");
  @elseif(Session::has('mobil_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('mobil_created'))
  swal("sukses!", "mobil berhasil ditambahkan.", "success");
  @elseif(Session::has('mobil_failed_creared'))
  swal("Maaf!", "gagal menambahkan data mobil.", "error");

  @elseif(Session::has('mobil_success_updated'))
  swal("Berhasil!", "data mobil berhasil di ubah.", "success");
  @elseif(Session::has('mobil_failed_updated'))
  swal("Maaf!", "data mobil gagal di ubah.", "error");

  @elseif(Session::has('mobil_success_deleted'))
  swal("Berhasil!", "data mobil berhasil dihapus.", "success");
  @elseif(Session::has('mobil_failed_deleted'))
  swal("Maaf!", "data mobil gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection