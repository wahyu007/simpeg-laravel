@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
    <div id="page_content_inner">
    <!-- statistics (small charts) -->
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a">
                    Tambah perbaikan
                </h3>
                <br>
                <form action="{{route('post-perbaikan-add')}}" method="POST">
                    <div class="uk-grid" data-uk-grid-margin>
                        {{ csrf_field() }}
                        <div class="uk-width-medium-1-3 uk-width-1-1">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-database"></i></span>
                                <label for="id_jadwal">Id Jadwal</label>
                                <select required class="
                                        md-input 
                                        {{$errors->has('id_jadwal') ? ' md-input-danger' : ''}}
                                        " type="text" id="id_jadwal" name="id_jadwal">
                                        <option value="NULL">Pilih Jadwal</option>
                                        @foreach($jadwal as $op)
                                        <option value="{{$op->jadwal_id}}">{{$op->pegawai->nama}} {{$op->tugas->tugas}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-database"></i></span>
                                <label for="id_lokasi">Lokasi</label>
                                <select required class="
                                        md-input 
                                        {{$errors->has('id_lokasi') ? ' md-input-danger' : ''}}
                                        " type="text" id="id_lokasi" name="id_lokasi">
                                        <option value="NULL">Pilih Lokasi</option>
                                        @foreach($lokasi as $op)
                                        <option value="{{$op->id_lokasi}}">{{$op->gerbang}}| gardu :{{$op->gardu}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-database"></i></span>
                                <label for="id_component">Komponen</label>
                                <select required class="
                                        md-input 
                                        {{$errors->has('id_component') ? ' md-input-danger' : ''}}
                                        " type="text" id="id_component" name="id_component">
                                        <option value="NULL">Pilih Komponen</option>
                                        @foreach($component as $op)
                                        <option value="{{$op->id_component}}">{{$op->alat}} | {{$op->component}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-large-1-3 uk-width-1-1">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="waktu_kerusakan">Waktu Gardu Error</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="waktu_kerusakan" 
                                    name="waktu_kerusakan" data-uk-timepicker />
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="waktu_perbaikan">Waktu Datang</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="waktu_perbaikan" 
                                    name="waktu_perbaikan" data-uk-timepicker />
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="waktu_selesai">Waktu Selesai</label>
                                <input required 
                                    class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="waktu_selesai" 
                                    name="waktu_selesai" data-uk-timepicker />
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="uk-grid">
                        <div class="uk-width-medium">
                            <div class="uk-grid">
                                <div class="uk-width-small-1-2">
                                    <label for="kerusakan">Uraian Kerusakan</label>
                                    <textarea
                                        cols="30" rows="4"
                                        class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                        id="kerusakan" 
                                        name="kerusakan" >
                                    </textarea>
                                </div>
                                <div class="uk-width-small-1-2">
                                    <label for="perbaikan">Uraian Perbaikan</label>
                                    <textarea 
                                        cols="30" rows="4"
                                        class="md-input {{$errors->has('perbaikan') ? ' md-input-danger' : ''}}" 
                                        id="perbaikan" 
                                        name="perbaikan" >
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid ">
                        <div class="uk-width-medium" >
                            <a href="{{ URL::previous()}}">
                                <button type="button" class="uk-align-right md-btn md-btn-flat md-btn-flat-danger">Cancel</button>
                            </a>
                            <button type="submit" class="uk-align-right md-btn md-btn-flat md-btn-primary ">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- end update modal --}}
<script>
  @if(Session::has('perbaikan_errval'))
  @if($errors->has('perbaikan'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('perbaikan_notComplete'))
  swal("Maaf!", "Uraian Tidak Boleh Kosong.", "error");
  @elseif(Session::has('perbaikan_notMatch'))
  swal("Maaf!", "Waktu Perbaikan Tidak Sesuai.", "error");
  @elseif(Session::has('perbaikan_notfound'))
  swal("warning!", "ID perbaikan tidak ada.", "info");
  @elseif(Session::has('perbaikan_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('perbaikan_created'))
  swal("sukses!", "perbaikan berhasil ditambahkan.", "success");
  @elseif(Session::has('perbaikan_failed_creared'))
  swal("Maaf!", "gagal menambahkan data perbaikan.", "error");

  @elseif(Session::has('perbaikan_success_updated'))
  swal("Berhasil!", "data perbaikan berhasil di ubah.", "success");
  @elseif(Session::has('perbaikan_failed_updated'))
  swal("Maaf!", "data perbaikan gagal di ubah.", "error");

  @elseif(Session::has('perbaikan_success_deleted'))
  swal("Berhasil!", "data perbaikan berhasil dihapus.", "success");
  @elseif(Session::has('perbaikan_failed_deleted'))
  swal("Maaf!", "data perbaikan gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection