@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Data lokasi Terbaru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah Pegawai" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_lokasi'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    <h4 class="heading_a uk-margin-bottom">List lokasi
      <a href="{{route('get-lokasi-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Cabang</th>
              <th>Gerbang</th>
              <th>Gardu</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($lokasis as $ins)
            <tr>
              <td>{{$i++}}</td>
              <td>{{$ins->cabang}}</td>
              <td>{{$ins->gerbang}}</td>
              <td>{{$ins->gardu}}</td>
              <td class="uk-text-center">
                <a data-uk-tooltip="{pos:'top'}" title="Update lokasi" href="{{route('get-lokasi-update',$ins->id_lokasi)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus lokasi" href="{{route('get-lokasi-delete',$ins->id_lokasi)}}">
                  <i class="md-icon material-icons uk-text-danger">delete</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update lokasi modal --}} @if(Session::has('lokasi_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_lokasi" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('get-lokasi-updated',Session('lokasi')->id_lokasi)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update Pegawai</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="cabang">Nama lokasi</label>
          <input required value="{{Session('lokasi')->cabang}}" class="
                  md-input 
                  {{$errors->has('lokasi') ? ' md-input-danger' : ''}}
                  " type="text" id="cabang" name="cabang" />
        </div>
        <div class="uk-form-row">
          <label for="gerbang">Nama lokasi</label>
          <input required value="{{Session('lokasi')->gerbang}}" class="
                  md-input 
                  {{$errors->has('lokasi') ? ' md-input-danger' : ''}}
                  " type="text" id="gerbang" name="gerbang" />
        </div>
        <div class="uk-form-row">
          <label for="gardu">Nama lokasi</label>
          <input required value="{{Session('lokasi')->gardu}}" class="
                  md-input 
                  {{$errors->has('lokasi') ? ' md-input-danger' : ''}}
                  " type="text" id="gardu" name="gardu" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_lokasi()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_lokasi() {
              $('#update_lokasi').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update lokasi modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_lokasi" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-lokasi-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah lokasi</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="cabang">Nama Cabang</label>
          <input required class="
                  md-input 
                  {{$errors->has('cabang') ? ' md-input-danger' : ''}}
                  " type="text" id="cabang" name="cabang" />
        </div>
        <div class="uk-form-row">
          <label for="gerbang">Nama Gerbang</label>
          <input required class="
                  md-input 
                  {{$errors->has('gerbang') ? ' md-input-danger' : ''}}
                  " type="text" id="gerbang" name="gerbang" />
        </div>
        <div class="uk-form-row">
          <label for="gardu">Nama Gardu</label>
          <input required class="
                  md-input 
                  {{$errors->has('gardu') ? ' md-input-danger' : ''}}
                  " type="text" id="gardu" name="gardu" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('lokasi_errval'))
  @if($errors->has('lokasi'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('lokasi_notfound'))
  swal("warning!", "ID lokasi tidak ada.", "info");
  @elseif(Session::has('lokasi_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('lokasi_created'))
  swal("sukses!", "lokasi berhasil ditambahkan.", "success");
  @elseif(Session::has('lokasi_failed_creared'))
  swal("Maaf!", "gagal menambahkan data lokasi.", "error");

  @elseif(Session::has('lokasi_success_updated'))
  swal("Berhasil!", "data lokasi berhasil di ubah.", "success");
  @elseif(Session::has('lokasi_failed_updated'))
  swal("Maaf!", "data lokasi gagal di ubah.", "error");

  @elseif(Session::has('lokasi_success_deleted'))
  swal("Berhasil!", "data lokasi berhasil dihapus.", "success");
  @elseif(Session::has('lokasi_failed_deleted'))
  swal("Maaf!", "data Pegawai gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection