@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Tambah Data component Baru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah Pegawai" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_component'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    <h4 class="heading_a uk-margin-bottom">List component
      <a href="{{route('get-component-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Alat</th>
              <th>Komponen</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($components as $ins)
            <tr>
              <td>{{$i++}}</td>
              <td>{{$ins->alat}}</td>
              <td>{{$ins->component}}</td>
              <td class="uk-text-center">
                <a data-uk-tooltip="{pos:'top'}" title="Update component" href="{{route('get-component-update',$ins->id_component)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus component" href="{{route('get-component-delete',$ins->id_component)}}">
                  <i class="md-icon material-icons uk-text-danger">delete</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update component modal --}} @if(Session::has('component_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_component" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('get-component-updated',Session('component')->id_component)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update Pegawai</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="alat">Nama Alat</label>
          <input required value="{{Session('component')->alat}}" class="
                  md-input 
                  {{$errors->has('component') ? ' md-input-danger' : ''}}
                  " type="text" id="alat" name="alat" />
        </div>
        <div class="uk-form-row">
          <label for="component">Nama Komponen</label>
          <input required value="{{Session('component')->component}}" class="
                  md-input 
                  {{$errors->has('component') ? ' md-input-danger' : ''}}
                  " type="text" id="component" name="component" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_component()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_component() {
              $('#update_component').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update component modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_component" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('post-component-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah component</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="alat">Nama Alat</label>
          <input required class="
                  md-input 
                  {{$errors->has('alat') ? ' md-input-danger' : ''}}
                  " type="text" id="alat" name="alat" />
        </div>
        <div class="uk-form-row">
          <label for="component">Nama Komponent</label>
          <input required class="
                  md-input 
                  {{$errors->has('component') ? ' md-input-danger' : ''}}
                  " type="text" id="component" name="component" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('component_errval'))
  @if($errors->has('component'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('component_notfound'))
  swal("warning!", "ID component tidak ada.", "info");
  @elseif(Session::has('component_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('component_created'))
  swal("sukses!", "component berhasil ditambahkan.", "success");
  @elseif(Session::has('component_failed_creared'))
  swal("Maaf!", "gagal menambahkan data component.", "error");

  @elseif(Session::has('component_success_updated'))
  swal("Berhasil!", "data component berhasil di ubah.", "success");
  @elseif(Session::has('component_failed_updated'))
  swal("Maaf!", "data component gagal di ubah.", "error");

  @elseif(Session::has('component_success_deleted'))
  swal("Berhasil!", "data component berhasil dihapus.", "success");
  @elseif(Session::has('component_failed_deleted'))
  swal("Maaf!", "data Pegawai gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection