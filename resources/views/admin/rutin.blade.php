@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Tambah Data rutin Baru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah rutin" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_rutin'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
      <h4 class="heading_a uk-margin-right">List rutin
      <a href="{{route('get-rutin-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    </div>
    
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Id Rutin</th>
              <th>ID Jadwal</th>
              <th>ID Coponent</th>
              <th>Kelas</th>
              <th>Kondisi</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($rutins as $ins)
            <tr>
              <td>{{$ins->id_rutin}}</td>
              <td>{{$ins->jadwal->id_tugas}}</td>
              <td>{{$ins->komponen->alat}}</td>
              <td>{{$ins->kelas}}</td>
              <td>{{$ins->kondisi}}</td>
              <td class="uk-text-left">
                <a data-uk-tooltip="{pos:'top'}" title="Update rutin" href="{{route('get-rutin-update',$ins->id_rutin)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus rutin" href="{{route('get-rutin-delete',$ins->id_rutin)}}">
                  <i class="md-icon material-icons uk-text-danger">delete</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update rutin modal --}} @if(Session::has('rutin_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_rutin" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('get-rutin-updated',Session('rutin')->id_rutin)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update Alat</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="id_jadwal">ID Jadwal</label>
          <select 
              id="id_jadwal" 
              name="id_jadwal" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="{{Session('rutin')->id_jadwal}}">{{Session('rutin')->jadwal->tugas->tugas}}</option>
              @foreach($id_jadwal as $op)
              <option value="{{$op->jadwal_id}}">{{$op->tugas->tugas}}</option>
              @endforeach
          </select>
        <div class="uk-form-row">
          <label for="id_component">ID Component</label>
          <select 
              id="id_component" 
              name="id_component" 
              class="md-input {{$errors->has('component') ? ' md-input-danger' : ''}}">
              <option value="{{Session('rutin')->id_component}}">{{Session('rutin')->komponen->alat}}</option>
              @foreach($component as $op)
              <option value="{{$op->id_component}}">{{$op->alat}}</option>
              @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="kelas">Kelas Perawatan</label>
          <select 
              id="kelas" 
              name="kelas" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="{{Session('rutin')->kelas}}">{{Session('rutin')->kelas}}</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
          </select>
        </div>
        <div class="uk-form-row">
          <label for="kondisi">Kondisi</label>
          <select 
              id="kondisi" 
              name="kondisi" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="{{Session('rutin')->kondisi}}">{{Session('rutin')->kondisi}}</option>
              <option value="normal">normal</option>
              <option value="perbaikan">perbaikan</option>
              <option value="error">error</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_rutin()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_rutin() {
              $('#update_rutin').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update rutin modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_rutin" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-rutin-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah rutin</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="id_jadwal">ID Jadwal</label>
          <select 
              id="id_jadwal" 
              name="id_jadwal" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="NULL">ID Jadwal</option>
              @foreach($id_jadwal as $op)
              <option value="{{$op->jadwal_id}}">{{$op->tugas->tugas}}</option>
              @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="id_component">ID Component</label>
          <select 
              id="id_component" 
              name="id_component" 
              class="md-input {{$errors->has('component') ? ' md-input-danger' : ''}}">
              <option value="NULL">ID component</option>
              @foreach($component as $op)
              <option value="{{$op->id_component}}">{{$op->alat}}</option>
              @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="kelas">Kelas</label>
          <select 
              id="kelas" 
              name="kelas" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="NULL">kelas</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
          </select>
        </div>
        <div class="uk-form-row">
          <label for="kondisi">Kondisi</label>
          <select 
              id="kondisi" 
              name="kondisi" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="NULL">kondisi</option>
              <option value="normal">normal</option>
              <option value="perbaikan">perbaikan</option>
              <option value="error">error</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('rutin_errval'))
  @if($errors->has('rutin'))
  swal("Warning!", "Error Request! {{$errors->first('rutin')}}", "warning");
  @endif
  @elseif(Session::has('rutin_notfound'))
  swal("warning!", "ID rutin tidak ada.", "info");
  @elseif(Session::has('rutin_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('rutin_created'))
  swal("sukses!", "rutin berhasil ditambahkan.", "success");
  @elseif(Session::has('rutin_failed_creared'))
  swal("Maaf!", "gagal menambahkan data rutin.", "error");

  @elseif(Session::has('rutin_success_updated'))
  swal("Berhasil!", "data rutin berhasil di ubah.", "success");
  @elseif(Session::has('rutin_failed_updated'))
  swal("Maaf!", "data rutin gagal di ubah.", "error");

  @elseif(Session::has('rutin_success_deleted'))
  swal("Berhasil!", "data rutin berhasil dihapus.", "success");
  @elseif(Session::has('rutin_failed_deleted'))
  swal("Maaf!", "data rutin gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection