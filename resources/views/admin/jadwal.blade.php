@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>
        <a href="{{route('get-tambah-jadwal-index')}}">
            <button data-uk-tooltip="{pos:'right'}" title="Tambah jadwal" class="
                md-btn 
                md-btn-primary 
                md-btn-small 
                md-btn-wave-light 
                waves-effect 
                waves-button 
                waves-light
                uk-align-right" > tambah
                <span class="menu_icon">
                    <i class="material-icons uk-text-contrast">add</i>
                </span>
            </button>
        </a>
      </h3>
    </div>
    <h4 class="heading_a uk-margin-bottom">List jadwal
      <a href="{{route('get-jadwal-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Tugas</th>
              <th>Masuk</th>
              <th>Selesai</th>
              <th>Tanggal</th>
              <th>Tanggal Buat</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($jadwals as $ins)
            <tr>
              <td>{{$i++}}</td>
              <td>{{$ins->pegawai->nama}}</td>
              <td>{{$ins->tugas->tugas}}</td>
              <td>{{$ins->tugas->jam_masuk}}</td>
              <td>{{$ins->tugas->jam_selesai}}</td>
              <td>{{$ins->tanggal}}</td>
              <td>{{$ins->created_at->toDateString()}}</td>
              <td>
                @if(Auth::user()->role == 1)
              <a data-uk-tooltip="{pos:'top'}" title="Lihat jadwal" href="{{route('get-jadwal-tugas',$ins->jadwal_id)}}">
                  <i class="md-icon material-icons uk-text-info">remove_red_eye</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Update jadwal" href="{{route('get-jadwal-update',$ins->jadwal_id)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus jadwal" href="{{route('get-jadwal-delete',$ins->jadwal_id)}}">
                  <i class="md-icon material-icons uk-text-danger">remove_circle</i>
                </a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update tugas modal --}} @if(Session::has('tugas_view'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="tugas_view" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
    <h2 class="uk-modal-title">Data Tugas</h2>
      <table>
        <tbody>
        <tr>
            <td>Tanggal</td>
            <td> : </td>
            <td>{{Session('tugas')->tanggal}}</td>
          </tr>
        <tr>
            <td>Nama</td>
            <td> : </td>
            <td>{{Session('tugas')->nama}}</td>
          </tr>
          <tr>
            <td>Tugas</td>
            <td> : </td>
            <td>{{Session('tugas')->tugas}}</td>
          </tr>
          <tr>
            <td>Jam Masuk</td>
            <td> : </td>
            <td>{{Session('tugas')->jam_masuk}}</td>
          </tr>
          <tr>
            <td>Jam Selesai</td>
            <td> : </td>
            <td>{{Session('tugas')->jam_selesai}}</td>
          </tr>
        </tbody>
      </table>
      <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_tugas()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <script>
            function close_update_tugas() {
              $('#tugas_view').hide()
              console.log("hidden");

            }
          </script>
        </div>
    </div>
  </div>
</div>
@endif
<script>
  @if(Session::has('jadwal_errval'))
//   @if($errors->has('jadwal'))
  swal("Warning!", "Error Request!", "warning");
  @endif
  @elseif(Session::has('jadwal_notfound'))
  swal("warning!", "ID jadwal tidak ada.", "info");
  @elseif(Session::has('jadwal_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('jadwal_created'))
  swal("sukses!", "jadwal berhasil ditambahkan.", "success");
  @elseif(Session::has('jadwal_failed_creared'))
  swal("Maaf!", "gagal menambahkan data jadwal.", "error");

  @elseif(Session::has('jadwal_success_updated'))
  swal("Berhasil!", "data jadwal berhasil di ubah.", "success");
  @elseif(Session::has('jadwal_failed_updated'))
  swal("Maaf!", "data jadwal gagal di ubah.", "error");

  @elseif(Session::has('jadwal_success_deleted'))
  swal("Berhasil!", "data jadwal berhasil dihapus.", "success");
  @elseif(Session::has('jadwal_failed_deleted'))
  swal("Maaf!", "data jadwal gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection