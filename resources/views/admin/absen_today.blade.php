@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="{{asset('css/notyf.min.css')}}" /> {{--
<script src="{{asset('js/uikit.min.js')}}"></script> --}} {{--
<script src="{{asset('js/uikit-icons.min.js')}}"></script> --}}
@endsection @section('content') {{-- content --}}
  <div id="page_content_inner">
  <div id="page_content">
    <div class="uk-width-1-1@s uk-width-3-5@l uk-width-1-3@xl">
      <div class="uk-card uk-card-default uk-animation-slide-top">
        <div class="uk-card-body">
          <h4 class="heading_a uk-margin-bottom">Absensi Pegawai</h4>
          <form method="POST" action="{{route('post-absen')}}">
            {{ csrf_field() }}
              <div class="uk-margin uk-margin-medium-bottom">
                <div class="uk-position-relative">
                  <span class="uk-form-icon" uk-icon="icon: user"></span>
                  <select id="pegawai" name="pegawai_id" data-md-selectize>
                    <option value="">Find Your Name...</option>
                    @foreach($all as $p)
                    <option value="{{$p->id_pegawai}}">{{$p->nama}} _ID : {{$p->id_pegawai}}</option>
                    @endforeach
                  </select>
                  <script>
                    new SlimSelect({
                      select: '#pegawai'
                    })

                    function handle(e){
                      if(e.keyCode === 13){
                          e.preventDefault(); // Ensure it is only this code that rusn
                          alert("Enter was pressed was presses");
                      }
                  }
                  </script>
                </div>
              </div>
              <div class="uk-margin">
                <button type="submit" class="uk-button uk-button-primary">
                  <span class="ion-forward"></span>&nbsp; Submit
                </button>
              </div>
          </form>
        </div>
      </div>
    </div>
    @if($absenRecordsToday->count() <1 ) 
    <div>
      <h3 class="uk-text-danger">Belum Ada Pegawai yg Absen</h3>
    </div>
    <div class="uk-grid uk-grid-width-large-1-5 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show"
      data-uk-sortable data-uk-grid-margin>
    @else
      <h4 class="heading_a uk-margin-bottom">Absen Hari Ini
        <a href="{{route('get-absen-records-download-all')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
          <i class="md-icon material-icons uk-text-primary">cloud_download</i>
        </a>
      </h4>
        <div class="md-card uk-margin-medium-bottom">
          <div class="md-card-content">
            <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Pegawai ID</th>
                  <th>Nama</th>
                  <th>Masuk</th>
                  <th>Pulang</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($absenRecordsToday as $ins)
                <tr>
                  <td>{{$no++}}</td>
                  <td>{{$ins->pegawai_id}}</td>
                  <td>{{$ins->pegawai->nama}}</td>
                  <td>{{$ins->jam_masuk}}</td>
                  @if($ins->jam_selesai == null)
                  <td><span class="uk-badge">Belum</span></td>
                  <td class="uk-text-center">
                  @if($ins->pegawai_id == Auth::user()->pegawai_id)
                    <form action="{{route('get-absen-update-id',$ins->id)}}" method="post">
                    {{ csrf_field() }}
                      <a data-uk-tooltip="{pos:'left'}" title="Pulang" href="{{route('get-absen-update-id',$ins->id)}}">
                      <button type="submit" class="uk-button uk-button-primary">pulang</button>
                      </a>
                    </form>
                  @endif
                  </td>
                  @else
                  <td>{{$ins->jam_selesai}}</td>
                  @endif
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      @endif
  </div>
</div>
{{-- end content --}} @endsection @section('_addscript')
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<script>
  @if(Session::has('absen_errval'))
  @if($errors->has('pegawai_id'))
  swal("Warning!", "Error Request! ", "warning");
  @endif
  @elseif(Session::has('absen_notfound'))
  swal("warning!", "ID absen tidak ada.", "info");
  @elseif(Session::has('absen_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('absen_created'))
  swal("sukses!", "absen berhasil ditambahkan.", "success");
  @elseif(Session::has('absen_failed_created'))
  swal("Maaf!", "gagal menambahkan data absen.", "error");

  @elseif(Session::has('absen_success_updated'))
  swal("Berhasil!", "data absen berhasil di ubah.", "success");
  @elseif(Session::has('absen_failed_updated'))
  swal("Maaf!", "data absen gagal di ubah.", "error");
  @elseif(Session::has('absen_success_deleted'))
  swal("Berhasil!", "data absen berhasil dihapus.", "success");
  @elseif(Session::has('absen_failed_deleted'))
  swal("Maaf!", "data Pegawai gagal dihapus.", "error");
  @elseif(Session::has('sudah_absen'))
  swal("Warning!", "Sudah Absen", "error");
  @endif
</script>
@endsection