@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>
        <a href="{{route('get-tambah-perbaikan-index')}}">
            <button data-uk-tooltip="{pos:'right'}" title="Tambah perbaikan" class="
                md-btn 
                md-btn-primary 
                md-btn-small 
                md-btn-wave-light 
                waves-effect 
                waves-button 
                waves-light
                uk-align-right" > tambah
                <span class="menu_icon">
                    <i class="material-icons uk-text-contrast">add</i>
                </span>
            </button>
        </a>
      </h3>
    </div>
    <h4 class="heading_a uk-margin-bottom">List perbaikan
      <a href="{{route('get-perbaikan-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Jadwal</th>
              <th>Nama</th>
              <th>Lokasi</th>
              <th>Komponen</th>
              <th>w_kerusakan</th>
              <th>w_perbaikan</th>
              <th>w_selesai</th>
              <th>Kerusakan</th>
              <th>Perbaikan</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($perbaikans as $ins)
            <tr>
              <td>{{$i++}}</td>
              <td>{{$ins->jadwal->tugas->tugas}}</td>
              <td>{{$ins->jadwal->pegawai->nama}}</td>
              <td>{{$ins->lokasi->gerbang}} | {{$ins->lokasi->gardu}}</td>
              <td>{{$ins->component->component}}</td>
              <td>{{$ins->w_kerusakan}}</td>
              <td>{{$ins->w_perbaikan}}</td>
              <td>{{$ins->w_selesai_perbaikan}}</td>
              <td>{{$ins->kerusakan}}</td>
              <td>{{$ins->perbaikan}}</td>
              <td>
              <a data-uk-tooltip="{pos:'top'}" title="Lihat perbaikan" href="{{route('get-perbaikan-view',$ins->id_jadwal)}}">
                  <i class="md-icon material-icons uk-text-info">remove_red_eye</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Update perbaikan" href="{{route('get-perbaikan-update',$ins->id_jadwal)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus perbaikan" href="{{route('get-perbaikan-delete',$ins->id_jadwal)}}">
                  <i class="md-icon material-icons uk-text-danger">delete</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  @if(Session::has('perbaikan_errval'))
//   @if($errors->has('perbaikan'))
  swal("Warning!", "Error Request!", "warning");
  @endif
  @elseif(Session::has('perbaikan_notfound'))
  swal("warning!", "ID perbaikan tidak ada.", "info");
  @elseif(Session::has('perbaikan_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('perbaikan_created'))
  swal("sukses!", "perbaikan berhasil ditambahkan.", "success");
  @elseif(Session::has('perbaikan_failed_creared'))
  swal("Maaf!", "gagal menambahkan data perbaikan.", "error");

  @elseif(Session::has('perbaikan_success_updated'))
  swal("Berhasil!", "data perbaikan berhasil di ubah.", "success");
  @elseif(Session::has('perbaikan_failed_updated'))
  swal("Maaf!", "data perbaikan gagal di ubah.", "error");

  @elseif(Session::has('perbaikan_success_deleted'))
  swal("Berhasil!", "data perbaikan berhasil dihapus.", "success");
  @elseif(Session::has('perbaikan_failed_deleted'))
  swal("Maaf!", "data perbaikan gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection