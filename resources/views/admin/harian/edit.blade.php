@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
    <div id="page_content_inner">
    <!-- statistics (small charts) -->
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a">
                    Edit harian
                </h3>
                <br>
                @foreach($harian as $p)
                <form action="{{route('get-laporan-harian-update',$p->id_harian)}}" method="POST">
                    <div class="uk-grid" data-uk-grid-margin>
                        {{ csrf_field() }}
                        <div class="uk-width-medium-1-3 uk-width-1-1">
                        <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-database"></i></span>
                                <label for="id_jadwal">ID Jadwal</label>
                                <select required class="
                                        md-input 
                                        {{$errors->has('id_jadwal') ? ' md-input-danger' : ''}}
                                        " type="text" id="id_jadwal" name="id_jadwal">
                                        <option value="{{$p->id_jadwal}}">{{$p->jadwal->tugas->tugas}}</option>
                                        @foreach($id_jadwal as $op)
                                        <option value="{{$op->id_jadwal}}">{{$op->tugas->tugas}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-home"></i></span>
                                <label for="id_mobil">ID Mobil</label>
                                <select required class="
                                        md-input 
                                        {{$errors->has('id_jadwal') ? ' md-input-danger' : ''}}
                                        " type="text" id="id_mobil" name="id_mobil">
                                        <option value="{{$p->id_mobil}}">{{$p->mobil->nopol}}</option>
                                        @foreach($mobil as $op)
                                        <option value="{{$op->id}}">{{$op->nopol}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-desktop"></i></span>
                                <label for="next_teknisi">ID Teknisi Selanjutnya</label>
                                <select required class="
                                        md-input 
                                        {{$errors->has('id_jadwal') ? ' md-input-danger' : ''}}
                                        " type="text" id="next_teknisi" name="next_teknisi">
                                        <option value="{{$p->id_next_teknisi}}">{{$p->nextPegawai->nama}}</option>
                                        @foreach($pegawai as $op)
                                        <option value="{{$op->id_pegawai}}">{{$op->nama}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-large-1-3 uk-width-1-1">
                        <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="id_alat">ID Alat</label>
                                <select required class="
                                        md-input 
                                        {{$errors->has('id_jadwal') ? ' md-input-danger' : ''}}
                                        " type="text" id="id_alat" name="id_alat">
                                        <option value="{{$p->id_alat}}">{{$p->alat->nama_alat}}</option>
                                        @foreach($alat as $op)
                                        <option value="{{$op->id_alat}}">{{$op->nama_alat}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="km_akhir">KM Akhir</label>
                                <input required 
                                    class="md-input {{$errors->has('laporan-harian') ? ' md-input-danger' : ''}}" 
                                    type="number" 
                                    id="km_akhir" 
                                    name="km_akhir" value="{{$p->km_akhir}}"/>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                <label for="info_taruna">Info Taruna</label>
                                <input required 
                                    class="md-input {{$errors->has('laporan-harian') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="info_taruna" 
                                    name="info_taruna" value="{{$p->info_taruna}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="uk-grid ">
                        <div class="uk-width-medium" >
                            <a href="{{ URL::previous()}}">
                                <button type="button" class="uk-align-right md-btn md-btn-flat md-btn-flat-danger">Cancel</button>
                            </a>
                            <button type="submit" class="uk-align-right md-btn md-btn-flat md-btn-primary ">Save</button>
                        </div>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>

{{-- end update modal --}}
<script>
  @if(Session::has('harian_errval'))
  @if($errors->has('harian'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('harian_notComplete'))
  swal("Maaf!", "Uraian Tidak Boleh Kosong.", "error");
  @elseif(Session::has('harian_notMatch'))
  swal("Maaf!", "Waktu Perbaikan Tidak Sesuai.", "error");
  @elseif(Session::has('harian_notfound'))
  swal("warning!", "ID harian tidak ada.", "info");
  @elseif(Session::has('harian_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('harian_created'))
  swal("sukses!", "harian berhasil ditambahkan.", "success");
  @elseif(Session::has('harian_failed_creared'))
  swal("Maaf!", "gagal menambahkan data harian.", "error");

  @elseif(Session::has('harian_success_updated'))
  swal("Berhasil!", "data harian berhasil di ubah.", "success");
  @elseif(Session::has('harian_failed_updated'))
  swal("Maaf!", "data harian gagal di ubah.", "error");

  @elseif(Session::has('harian_success_deleted'))
  swal("Berhasil!", "data harian berhasil dihapus.", "success");
  @elseif(Session::has('harian_failed_deleted'))
  swal("Maaf!", "data harian gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection