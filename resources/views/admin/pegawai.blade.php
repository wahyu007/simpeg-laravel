@extends('layouts.app') 
@section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection 
@section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Data Pegawai Terbaru
        <button class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect waves-button waves-light" data-uk-modal="{target:'#add_pegawai'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    
    <!-- table -->
    <div class="uk-grid">
      <div class="uk-width-1-1">
        <div class="md-card">
          <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions">
              <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
              <i class="md-icon material-icons">&#xE5D5;</i>
              <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}">
                <i class="md-icon material-icons">&#xE5D4;</i>
                <div class="uk-dropdown uk-dropdown-small">
                  <ul class="uk-nav">
                    <li>
                      <a href="#">Action 1</a>
                    </li>
                    <li>
                      <a href="#">Action 2</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <h3 class="md-card-toolbar-heading-text">
              Pegawai
            </h3>
          </div>
          <div class="md-card-content">
            <div class="mGraph-wrapper">
              <table class="uk-table uk-table-nowrap">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>No KTP</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>Tgl Lahir</th>
                    <th>Status</th>
                    <th>Terdaftar</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($pegawais as $p)
                  <tr>
                    <td>{{$i++}}</td>
                    <td>{{$p->nik}}</td>
                    <td>{{$p->nama}}</td>
                    <td>{{$p->no_ktp}}</td>
                    <td>{{$p->jenis_kelamin}}</td>
                    <td>{{$p->alamat}}</td>
                    <td>{{$p->tgl_lahir}}</td>
                    <td> 
                    @if($p->status_pegawai == 1)
                      <span class="uk-badge">active</span> 
                    @elseif($p->status_pegawai == 2)
                      <span class="uk-badge uk-badge-warning">waiting</span> 
                    @elseif($p->status_pegawai == 0)
                      <span class="uk-badge uk-badge-danger">inactive</span> 
                    @endif
                    </td>
                    {{--
                    <span class="uk-badge uk-badge-danger">{status_pegawai}</span> --}}
                    <td>{{$p->created_at->format('d-m-Y')}}</td>
                    <td class="uk-text-center">
                      <a data-uk-tooltip="{pos:'top'}" title="Update pegawai" href="{{route('get-pegawai-update-id',$p->id_pegawai)}}">
                        <i class="md-icon material-icons uk-text-success">edit</i>
                      </a>
                      <a data-uk-tooltip="{pos:'top'}" title="Hapus pegawai" href="{{route('get-pegawai-delete-id',$p->id_pegawai)}}">
                        <i class="md-icon material-icons uk-text-danger">remove_circle</i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              {{$pegawais->links('pagination.uk')}}
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- end table --}}
  </div>
</div>

{{-- update pegawai modal --}} @if(Session::has('pegawai_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_pegawai" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('post-pegawai-update-id',Session('pegawai')->id_pegawai)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Edit Pegawai</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nik">NIK</label>
          <input readonly value="{{Session('pegawai')->nik}}" class="
                md-input
                {{$errors->has('nik') ? ' md-input-danger' : ''}}
                " type="text" id="nik" name="nik" />
        </div>
        <div class="uk-form-row">
          <label for="nama">Nama Pegawai</label>
          <input value="{{Session('pegawai')->nama}}" class="
                md-input 
                {{$errors->has('nama') ? ' md-input-danger' : ''}}
                " type="text" id="nama" name="nama" />
        </div>
        <div class="uk-form-row">
          <label for="no_ktp">Nomor KTP</label>
          <input readonly value="{{Session('pegawai')->no_ktp}}" class="
                md-input
                {{$errors->has('no_ktp') ? ' md-input-danger' : ''}}
                " type="text" id="no_ktp" name="no_ktp" />
        </div>
        <div class="uk-form-row">
          <label for="tgl_lahir">Tanggal Lahir</label>
          <input value="{{Session('pegawai')->tgl_lahir}}" class="
                md-input
                {{$errors->has('tgl_lahir') ? ' md-input-danger' : ''}}
                " type="text" id="tgl_lahir" name="tgl_lahir" />
        </div>
        <div class="uk-form-row">
          <label for="alamat">Alamat</label>
          <input value="{{Session('pegawai')->alamat}}" class="
                md-input
                {{$errors->has('alamat') ? ' md-input-danger' : ''}}
                " type="text" id="alamat" name="alamat" />
        </div>
        <div class="uk-form-row">
          <label for="agama">Agama</label>
          <input value="{{Session('pegawai')->agama}}" class="
                md-input
                {{$errors->has('agama') ? ' md-input-danger' : ''}}
                " type="text" id="agama" name="agama" />
        </div>
        <div class="uk-form-row">
          <label for="jenis_kelamin">Jenis Kelamin</label>
          <input value="{{Session('pegawai')->jenis_kelamin}}" class="
                md-input
                {{$errors->has('jenis_kelamin') ? ' md-input-danger' : ''}}
                " type="text" id="jenis_kelamin" name="jenis_kelamin" />
        </div>
        <div class="uk-form-row">
          <label for="status_pegawai">Status</label>
          <select class="
                  md-input
                  {{$errors->has('status_pegawai') ? ' md-input-danger' : ''}}
                  " type="text" id="status_pegawai" name="status_pegawai">
                  @if($p->status_pegawai == 1)
                    <option value="{{Session('pegawai')->status_pegawai}}">Active</option> 
                  @elseif($p->status_pegawai == 2)
                    <option value="{{Session('pegawai')->status_pegawai}}">Waiting</option> 
                  @elseif($p->status_pegawai == 0)
                    <option value="{{Session('pegawai')->status_pegawai}}">Non Active</option> 
                  @endif
                  <option value="1">Active</option>
                  <option value="0">Non Active</option>
                  <option value="2">Waiting</option>
          </select>
        </div>
        <div class="uk-form-row">
          <label for="tgl_masuk">Tanggal Masuk</label>
          <input value="{{Session('pegawai')->tgl_masuk}}" class="
                md-input
                {{$errors->has('tgl_masuk') ? ' md-input-danger' : ''}}
                " type="date" id="tgl_masuk" name="tgl_masuk" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_pegawai()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_pegawai() {
              $('#update_pegawai').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update pegawai modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_pegawai" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-pegawai')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah Pegawai</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nik">NIK</label>
          <input class="
                  md-input 
                  {{$errors->has('nik') ? ' md-input-danger' : ''}}
                  " type="text" id="nik" name="nik" />
        </div>
        <div class="uk-form-row">
          <label for="nama">Nama</label>
          <input class="
                  md-input 
                  {{$errors->has('nama') ? ' md-input-danger' : ''}}
                  " type="text" id="nama" name="nama" />
        </div>
        <div class="uk-form-row">
          <label for="no_ktp">Nomor KTP</label>
          <input class="
                  md-input
                  {{$errors->has('no_ktp') ? ' md-input-danger' : ''}}
                  " type="text" id="no_ktp" name="no_ktp" />
        </div>
        <div class="uk-form-row">
          <label>Tanggal Lahir</label>
          <input class="
                  md-input
                  {{$errors->has('tgl_lahir') ? ' md-input-danger' : ''}}
                  " type="date" 
                  id="tgl_lahir" name="tgl_lahir" value="2020-01-01" />
        </div>
        <div class="uk-form-row">
          <label for="alamat">Alamat</label>
          <input class="
                  md-input
                  {{$errors->has('alamat') ? ' md-input-danger' : ''}}
                  " type="text" id="alamat" name="alamat" />
        </div>
        <div class="uk-form-row">
          <label for="agama">Agama</label>
          <select class="
                  md-input
                  {{$errors->has('agama') ? ' md-input-danger' : ''}}
                  " type="text" id="agama" name="agama">
                  <option value="null">Pilih Agama</option>
                  <option value="Hindu">Hindu</option>
                  <option value="Islam">Islam</option>
                  <option value="Budha">Budha</option>
                  <option value="Kristen">Kristen</option>
                  <option value="Katholik">Katholik</option>
          </select>
        </div>
        <div class="uk-form-row">
          <label for="jenis_kelamin">Jenis Kelamin</label>
          <select class="
                  md-input
                  {{$errors->has('jenis_kelamin') ? ' md-input-danger' : ''}}
                  " type="text" id="jenis_kelamin" name="jenis_kelamin">
                  <option value="null">Pilih Jenis Kelamin</option>
                  <option value="Laki-Laki">Laki-Laki</option>
                  <option value="Perempuan">Perempuan</option>
          </select>
        </div>
        <div class="uk-form-row">
          <label for="status_pegawai">Status</label>
          <select class="
                  md-input
                  {{$errors->has('status_pegawai') ? ' md-input-danger' : ''}}
                  " type="text" id="status_pegawai" name="status_pegawai">
                  <option value="null">Pilih Status</option>
                  <option value="1">Active</option>
                  <option value="0">Non Active</option>
          </select>
        </div>
        <div class="uk-form-row">
          <label for="uk_dp_1">Tanggal Masuk</label>
          <input class="
                  md-input
                  {{$errors->has('tgl_masuk') ? ' md-input-danger' : ''}}
                  " type="date" value="2020-01-01" id="uk_dp_1" name="tgl_masuk"/>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}} 
<script>
@if(Session::has('pegawai_errval'))
  @if($errors->has('nama'))
    swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning")
  @endif
  @if($errors->has('alamat'))
    swal("Warning!", "Error Request! {{$errors->first('alamat')}}", "warning")
  @endif
  @if($errors->has('jabatan'))
    swal("Warning!", "Error Request! {{$errors->first('jabatan')}}", "warning")
  @endif
  @if($errors->has('tgl_masuk'))
    swal("Warning!", "Error Request! {{$errors->first('tgl_masuk')}}", "warning")
  @endif
  @if($errors->has('tgl_lahir'))
    swal("Warning!", "Error Request! {{$errors->first('tgl_lahir')}}", "warning")
  @endif
  @if($errors->has('status_pegawai'))
    swal("Warning!", "Error Request! {{$errors->first('status_pegawai')}}", "warning")
  @endif
@elseif(Session::has('pegawai_notfound'))
  swal("warning!", "ID pegawai tidak ada.", "info")
@elseif(Session::has('pegawai_failed'))
  swal("Maaf!","Terjadi kesalahan system","error");

@elseif(Session::has('pegawai_created'))
  swal("sukses!", "pegawai berhasil ditambahkan.", "success")
@elseif(Session::has('pegawai_failed_created'))
  swal("Maaf!", "gagal menambahkan data pegawai.", "error")
@elseif(Session::has('duplikat_pegawai_failed_created'))
  swal("Maaf!", "gagal menambahkan data pegawai,No. KTP duplikat.", "error")
@elseif(Session::has('pegawai_success_updated'))
  swal("Berhasil!","data Pegawai berhasil di ubah.","success")
@elseif(Session::has('pegawai_failed_updated'))
  swal("Maaf!","data Pegawai gagal di ubah.","error")
  
@elseif(Session::has('pegawai_success_deleted'))
  swal("Berhasil!","data Pegawai berhasil dihapus.","success")
@elseif(Session::has('pegawai_failed_deleted'))
  swal("Maaf!","data Pegawai gagal dihapus.","error")

@endif 
</script>
{{-- end content --}} @endsection @section('_addscript')

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection