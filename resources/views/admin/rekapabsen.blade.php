@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Data RekapAbsen Terbaru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah RekapAbsen" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_pegawai'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    <br>
    <center>
      {{$newRekapAbsens->links('pagination.uk')}}
    </center>
    <h4 class="heading_a uk-margin-bottom">List RekapAbsen
      <a href="{{route('get-rekapabsen-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>ID</th>
              <th>Nama</th>
              <th>Total</th>
              <th>bulan</th>
              <th>Tahun</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($RekapAbsens as $ins)
            <tr>
              <td>{{$i++}}</td>
              <td>{{$ins->id_pegawai}}</td>
              <td>{{$ins->pegawai->nama}}</td>
              <td>{{$ins->total}}</td>
              <td>{{$ins->bulan}}</td>
              <td>{{$ins->tahun}}</td>
              <td class="uk-text-center">
                <a data-uk-tooltip="{pos:'top'}" title="view RekapAbsen" href="">
                  <i class="md-icon material-icons uk-text-alert">remove_red_eye</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Update RekapAbsen" href="{{route('get-rekapabsen-update',$ins->id_rekapabsen)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus RekapAbsen" href="{{route('get-rekapabsen-delete',$ins->id_rekapabsen)}}">
                  <i class="md-icon material-icons uk-text-danger">remove_circle</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update pegawai modal --}} @if(Session::has('rekapabsen_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_pegawai" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('get-rekapabsen-updated',Session('rekapabsen')->id_rekapabsen)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update Pegawai</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nik">Nama</label>
          <input required value="{{Session('rekapabsen')->nik}}" class="
                  md-input 
                  {{$errors->has('nik') ? ' md-input-danger' : ''}}
                  " type="text" id="nik" name="nik" />
        </div>
        <div class="uk-form-row">
          <label for="jenis">Jenis Surat</label>
          <input required class="
                  md-input 
                  {{$errors->has('jenis') ? ' md-input-danger' : ''}}
                  " id="jenis" name="jenis" value="{{Session('rekapabsen')->jenis}}"/>
        </div>
        <div class="uk-form-row">
          <label for="alasan">Alasan</label>
          <textarea required class="
                  md-input 
                  {{$errors->has('alasan') ? ' md-input-danger' : ''}}
                  " id="alasan" name="alasan" >{{Session('rekapabsen')->alasan}}</textarea>
        </div>
        <div class="uk-form-row">
          <label for="tanggal">Tanggal Mulai</label>
          <input required value="{{Session('rekapabsen')->tanggal}}" class="
                  md-input 
                  {{$errors->has('tanggal') ? ' md-input-danger' : ''}}
                  " type="date" id="tanggal" name="tanggal" />
        </div>
        <div class="uk-form-row">
          <label for="tanggal_selesai">Tanggal Selesai</label>
          <input required value="{{Session('rekapabsen')->tanggal_selesai}}" class="
                  md-input 
                  {{$errors->has('tanggal_selesai') ? ' md-input-danger' : ''}}
                  " type="date" id="tanggal_selesai" name="tanggal_selesai" />
        </div>
        <div class="uk-form-row">
          <label for="status">Status</label>
          <select required class="
                  md-input 
                  {{$errors->has('status') ? ' md-input-danger' : ''}}
                  " type="text" id="status" name="status">
                  <option value="1">Tunggu</option>
                  <option value="2">Disetujui</option>
                  <option value="3">Ditolak</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_pegawai()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_pegawai() {
              $('#update_pegawai').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Konfirmasi</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update pegawai modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_pegawai" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-rekapabsen-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah rekapabsen</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nik">NIK</label>
          <select required class="
                  md-input 
                  {{$errors->has('nik') ? ' md-input-danger' : ''}}
                  " type="text" id="nik" name="nik">
                  <option value="NULL">Pilih Pegawai</option>
                  @foreach(\App\Pegawai::get() as $op)
                  <option value="{{$op->nik}}">{{$op->nama}}</option>
                  @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="total">Total</label>
          <input required class="
                  md-input 
                  {{$errors->has('total') ? ' md-input-danger' : ''}}
                  " type="number" id="total" name="total" />
        </div>
        <div class="uk-form-row">
          <label for="tanggal">Tanggal</label>
          <input required class="
                  md-input 
                  {{$errors->has('tanggal') ? ' md-input-danger' : ''}}
                  " type="date" value="{{Carbon\Carbon::now()->toDateString()}}" id="tanggal" name="tanggal" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('rekapabsen_errval'))
  @if($errors->has('pegawai_id'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('rekapabsen_notfound'))
  swal("warning!", "ID rekapabsen tidak ada.", "info");
  @elseif(Session::has('rekapabsen_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('rekapabsen_created'))
  swal("sukses!", "rekapabsen berhasil ditambahkan.", "success");
  @elseif(Session::has('rekapabsen_failed_creared'))
  swal("Maaf!", "gagal menambahkan data rekapabsen.", "error");

  @elseif(Session::has('rekapabsen_success_updated'))
  swal("Berhasil!", "data rekapabsen berhasil di ubah.", "success");
  @elseif(Session::has('rekapabsen_failed_updated'))
  swal("Maaf!", "data rekapabsen gagal di ubah.", "error");

  @elseif(Session::has('rekapabsen_success_deleted'))
  swal("Berhasil!", "data rekapabsen berhasil dihapus.", "success");
  @elseif(Session::has('rekapabsen_failed_deleted'))
  swal("Maaf!", "data Pegawai gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection