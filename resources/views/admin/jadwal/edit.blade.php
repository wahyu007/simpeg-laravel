@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
    <div id="page_content_inner">
    <!-- statistics (small charts) -->
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a">
                    Edit jadwal
                </h3>
                <br>
                @foreach($jadwal as $p)
                <form action="{{route('get-jadwal-update',$p->jadwal_id)}}" method="POST">
                    <div class="uk-grid" data-uk-grid-margin>
                        {{ csrf_field() }}
                        <div class="uk-width-medium-1-3 uk-width-1-1">
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="uk-input-group-icon uk-icon-database"></i>
                                </span>
                                <label for="nik">Nik</label>
                                <select 
                                    id="nik" 
                                    name="nik" 
                                    class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}"  
                                    data-md-selectize>
                                    <option value="{{$p->nik}}">{{$p->nama}}</option>
                                    @foreach($pegawai as $op)
                                    <option value="{{$op->id_pegawai}}">_{{$op->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br><br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon">
                                    <i class="uk-input-group-icon uk-icon-home"></i>
                                </span>
                                <label for="tugas">Tugas</label>
                                <select 
                                    id="id_tugas" 
                                    name="id_tugas" 
                                    class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}"  
                                    data-md-selectize>
                                    <option value="{{$p->id_tugas}}">{{$p->nama_tugas}}</option>
                                    @foreach($tugass as $ip)
                                    <option value="{{$ip->id_tugas}}">_{{$ip->tugas}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="uk-input-group">
                                <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-home"></i></span>
                                <label for="tanggal">Tanggal</label>
                                <input required 
                                    class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}" 
                                    type="text" 
                                    id="tanggal" 
                                    name="tanggal" value="{{$p->tanggal}}" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="uk-grid ">
                        <div class="uk-width-medium" >
                            <button type="submit" class="uk-align-left md-btn md-btn-flat md-btn-primary ">Save</button>
                            <a href="{{ URL::previous()}}">
                                <button type="button" class="uk-align-left md-btn md-btn-flat md-btn-flat-danger">Cancel</button>
                            </a>
                        </div>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>

{{-- end update modal --}}
<script>
  @if(Session::has('jadwal_errval'))
  @if($errors->has('jadwal'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('jadwal_notComplete'))
  swal("Maaf!", "Uraian Tidak Boleh Kosong.", "error");
  @elseif(Session::has('jadwal_notMatch'))
  swal("Maaf!", "Waktu Perbaikan Tidak Sesuai.", "error");
  @elseif(Session::has('jadwal_notfound'))
  swal("warning!", "ID jadwal tidak ada.", "info");
  @elseif(Session::has('jadwal_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('jadwal_created'))
  swal("sukses!", "jadwal berhasil ditambahkan.", "success");
  @elseif(Session::has('jadwal_failed_creared'))
  swal("Maaf!", "gagal menambahkan data jadwal.", "error");

  @elseif(Session::has('jadwal_success_updated'))
  swal("Berhasil!", "data jadwal berhasil di ubah.", "success");
  @elseif(Session::has('jadwal_failed_updated'))
  swal("Maaf!", "data jadwal gagal di ubah.", "error");

  @elseif(Session::has('jadwal_success_deleted'))
  swal("Berhasil!", "data jadwal berhasil dihapus.", "success");
  @elseif(Session::has('jadwal_failed_deleted'))
  swal("Maaf!", "data jadwal gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection