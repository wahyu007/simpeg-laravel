@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
    <div id="page_content_inner">
    <!-- statistics (small charts) -->
        <div class="uk-width-medium-7-10 uk-container-center reset-print">
            <div class="uk-grid uk-grid-collapse" data-uk-grid-margin>
                <div class="uk-width-large-8-12">
                    <div class="md-card md-card-single main-print" id="invoice">
                        <div id="invoice_preview">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions hidden-print">
                                <a data-uk-tooltip="{pos:'right'}" title="Cetak" href="#" onClick="window.print()"><i class="md-icon material-icons" id="invoice_print">print</i></a>
                                <a data-uk-tooltip="{pos:'right'}" title="Close" href="{{URL::previous()}}" class="printMe"><i class="md-icon material-icons">close</i></a>
                            </div>
                            <h3 class="md-card-toolbar-heading-text large" id="invoice_name">
                                Laporan Perbaikan
                            </h3>
                        </div>
                        @foreach($perbaikan as $p)
                        <div class="md-card-content">
                            <div class="uk-margin-medium-bottom">
                                <span class="uk-text-muted uk-text-small uk-text-italic">Tanggal:</span> {{Carbon\carbon::parse($p->created_at)->toDateString()}} <br>
                                <span class="uk-text-muted uk-text-small uk-text-italic">Waktu:</span> {{Carbon\carbon::parse($p->w_mati)->format('H:i')}} WIB
                            </div>
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-small-3-5">
                                    <div class="uk-margin-bottom">
                                        <span class="uk-text-muted uk-text-small uk-text-italic">Cabang:</span>
                                        <address>
                                            <p><strong>Ruas {{$p->cabang}}</strong></p>
                                        </address>
                                    </div>
                                    <div class="uk-margin-bottom">
                                        <span class="uk-text-muted uk-text-small uk-text-italic">Gerbang:</span>
                                        <address>
                                            <p><strong>{{$p->gerbang}}</strong></p>
                                        </address>
                                    </div>
                                    <div class="uk-margin-bottom">
                                        <span class="uk-text-muted uk-text-small uk-text-italic">Gardu:</span>
                                        <address>
                                            <p><strong>{{$p->gardu}}</strong></p>
                                        </address>
                                    </div>
                                </div>
                                <div class="uk-width-small-2-5">
                                    <div class="uk-margin-bottom">
                                        <span class="uk-text-muted uk-text-small uk-text-italic">Supervisor:</span>
                                        <address>
                                            <p><strong>M Yudis NA</strong></p>
                                        </address>
                                    </div>
                                    <div class="uk-margin-bottom">
                                        <span class="uk-text-muted uk-text-small uk-text-italic">Teknisi:</span>
                                        <address>
                                            <p><strong>Wahyu Aris M.</strong></p>
                                        </address>
                                    </div>
                                    <div class="uk-grid">
                                        <div class="uk-width-small-1-3">
                                            <div class="uk-margin-bottom">
                                                <span class="uk-text-muted uk-text-small uk-text-italic">Alat:</span>
                                                <address>
                                                    <p><strong>{{$p->alat}}</strong></p>
                                                </address>
                                            </div>
                                        </div>
                                        <div class="uk-width-small-1-2">
                                            <div class="uk-margin-bottom">
                                                <span class="uk-text-muted uk-text-small uk-text-italic">Komponen:</span>
                                                <address>
                                                    <p><strong>{{ $p->komponen }}</strong></p>
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-grid uk-margin-large-bottom">
                                <div class="uk-width-1-1">
                                    <table class="uk-table">
                                        <thead>
                                        <tr class="uk-text-upper">
                                            <th class="uk-table-expand">Uraian Kerusakan</th>
                                            <th class="uk-table-expand">Uraian Perbaikan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="uk-table-middle">
                                                <!-- <td>
                                                    <span class="uk-text-large">wahyu aris munandar</span><br/>
                                                    <span class="uk-text-muted uk-text-small"> description </span>
                                                </td> -->
                                                <td class="uk-text-left">{{$p->uraian_kerusakan}}</td>
                                                <td class="uk-text-left">{{$p->uraian_perbaikan}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <span class="uk-text-muted uk-text-small uk-text-italic">Tutup Laporan :</span>
                                    <p class="uk-margin-top-remove">
                                        
                                    </p>
                                    <p class="uk-text-small">21:00 WIB</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- end update modal --}}

<script>
  @if(Session::has('perbaikan_errval'))
  @if($errors->has('perbaikan'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('perbaikan_notComplete'))
  swal("Maaf!", "Uraian Tidak Boleh Kosong.", "error");
  @elseif(Session::has('perbaikan_notMatch'))
  swal("Maaf!", "Waktu Perbaikan Tidak Sesuai.", "error");
  @elseif(Session::has('perbaikan_notfound'))
  swal("warning!", "ID perbaikan tidak ada.", "info");
  @elseif(Session::has('perbaikan_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('perbaikan_created'))
  swal("sukses!", "perbaikan berhasil ditambahkan.", "success");
  @elseif(Session::has('perbaikan_failed_creared'))
  swal("Maaf!", "gagal menambahkan data perbaikan.", "error");

  @elseif(Session::has('perbaikan_success_updated'))
  swal("Berhasil!", "data perbaikan berhasil di ubah.", "success");
  @elseif(Session::has('perbaikan_failed_updated'))
  swal("Maaf!", "data perbaikan gagal di ubah.", "error");

  @elseif(Session::has('perbaikan_success_deleted'))
  swal("Berhasil!", "data perbaikan berhasil dihapus.", "success");
  @elseif(Session::has('perbaikan_failed_deleted'))
  swal("Maaf!", "data perbaikan gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection