@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>
        <a href="{{route('get-tambah-laporan-harian-index')}}">
            <button data-uk-tooltip="{pos:'right'}" title="Tambah harian" class="
                md-btn 
                md-btn-primary 
                md-btn-small 
                md-btn-wave-light 
                waves-effect 
                waves-button 
                waves-light
                uk-align-right" > tambah
                <span class="menu_icon">
                    <i class="material-icons uk-text-contrast">add</i>
                </span>
            </button>
        </a>
      </h3>
    </div>
    <h4 class="heading_a uk-margin-bottom">List harian
      <a href="{{route('get-laporan-harian-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>ID Jadwal</th>
              <th>ID Mobil</th>
              <th>KM Akhir</th>
              <th>Teknisi Selanjutnya</th>
              <th>Alat Yang Dibawa</th>
              <th>Info Taruna</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($harians as $ins)
            <tr>
              <td>{{$i++}}</td>
              <td>{{$ins->jadwal->pegawai->nama}}</td>
              <td>{{$ins->mobil->nopol}}</td>
              <td>{{$ins->km_akhir}}</td>
              <td>{{$ins->nextPegawai->id_pegawai}}</td>
              <td>{{$ins->alat->nama_alat}}</td>
              <td>{{$ins->info_taruna}}</td>
              <td>
              <a data-uk-tooltip="{pos:'top'}" title="Lihat harian" href="{{route('get-laporan-harian-view',$ins->id_harian)}}">
                  <i class="md-icon material-icons uk-text-info">remove_red_eye</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Update harian" href="{{route('get-laporan-harian-update',$ins->id_harian)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus harian" href="{{route('get-laporan-harian-delete',$ins->id_harian)}}">
                  <i class="md-icon material-icons uk-text-danger">remove_circle</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  @if(Session::has('harian_errval'))
//   @if($errors->has('harian'))
  swal("Warning!", "Error Request!", "warning");
  @endif
  @elseif(Session::has('harian_notfound'))
  swal("warning!", "ID harian tidak ada.", "info");
  @elseif(Session::has('harian_jadwalnotfound'))
  swal("warning!", "Jadwal tidak ada.", "info");
  @elseif(Session::has('harian_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('harian_created'))
  swal("sukses!", "harian berhasil ditambahkan.", "success");
  @elseif(Session::has('harian_failed_creared'))
  swal("Maaf!", "gagal menambahkan data harian.", "error");

  @elseif(Session::has('harian_success_updated'))
  swal("Berhasil!", "data harian berhasil di ubah.", "success");
  @elseif(Session::has('harian_failed_updated'))
  swal("Maaf!", "data harian gagal di ubah.", "error");

  @elseif(Session::has('harian_success_deleted'))
  swal("Berhasil!", "data harian berhasil dihapus.", "success");
  @elseif(Session::has('harian_failed_deleted'))
  swal("Maaf!", "data harian gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection