@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Data Pengambilan Terbaru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah Pengambilan" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_pegawai'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    <br>
    <center>
      {{$newPengambilans->links('pagination.uk')}}
    </center>
    <h4 class="heading_a uk-margin-bottom">List Pengambilan
      <a href="{{route('get-pengambilan-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Jadwal</th>
              <th>Nama</th>
              <th>Alat</th>
              <th>Mobil</th>
              <th>tgl ambil</th>
              <th>tgl kembali</th>
              <th>status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($Pengambilans as $ins)
            <tr>
              <td>{{$i++}}</td>
              <td>{{$ins->jadwal->tugas->tugas}}</td>
              <td>{{$ins->jadwal->pegawai->nama}}</td>
              <td>{{$ins->alat->nama_alat}}</td>
              <td>{{$ins->mobil->nopol}}</td>
              <td>{{$ins->tgl_ambil}}</td>
              <td>{{$ins->tgl_kembali}}</td>
              <td><badge class="uk-badge"> @if($ins->status == 1) Belum Kembali @else Kembali @endif</badge></td>
              <td class="uk-text-center">
                <!-- <a data-uk-tooltip="{pos:'top'}" title="view Pengambilan" href="">
                  <i class="md-icon material-icons uk-text-alert">remove_red_eye</i>
                </a> -->
                @if($ins->status == 1)
                <a data-uk-tooltip="{pos:'top'}" title="Update Pengambilan" href="{{route('get-pengambilan-update',$ins->id_pengambilan)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                @endif
                <!-- <a data-uk-tooltip="{pos:'top'}" title="Hapus Pengambilan" href="{{route('get-pengambilan-delete',$ins->id_pengambilan)}}">
                  <i class="md-icon material-icons uk-text-danger">remove_circle</i>
                </a> -->
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update pegawai modal --}} @if(Session::has('pengambilan_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_pegawai" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('get-pengambilan-updated',Session('pengambilan')->id_pengambilan)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update Pegawai</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <h2>Kembalikan !</h2>
        </div>
        <input type="hidden" name="id_alat" value="{{Session('pengambilan')->id_alat}}" />
        <input type="hidden" name="id_mobil" value="{{Session('pengambilan')->id_mobil}}" />
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_pegawai()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_pegawai() {
              $('#update_pegawai').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Konfirmasi</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update pegawai modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_pegawai" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-pengambilan-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah pengambilan</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="id_jadwal">Id Jadwal</label>
          <select required class="
                  md-input 
                  {{$errors->has('id_jadwal') ? ' md-input-danger' : ''}}
                  " type="text" id="id_jadwal" name="id_jadwal">
                  <option value="NULL">Pilih Jadwal</option>
                  @foreach($jadwal as $op)
                  <option value="{{$op->jadwal_id}}">{{$op->pegawai->nama}} {{$op->tugas->tugas}}</option>
                  @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="id_mobil">Mobil</label>
          <select required class="
                  md-input 
                  {{$errors->has('id_mobil') ? ' md-input-danger' : ''}}
                  " type="text" id="id_mobil" name="id_mobil">
                  <option value="NULL">Pilih Mobil</option>
                  <option value="NULL">---</option>
                  @foreach($mobil as $mo)
                  <option value="{{$mo->id}}">{{$mo->nopol}}</option>
                  @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="id_alat">Alat</label>
          <select required class="
                  md-input 
                  {{$errors->has('id_alat') ? ' md-input-danger' : ''}}
                  " type="text" id="id_alat" name="id_alat">
                  <option value="NULL">Pilih Alat</option>
                  <option value="NULL">---</option>
                  @foreach($alat as $al)
                  <option value="{{$al->id_alat}}">{{$al->nama_alat}}</option>
                  @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="tgl_ambil">Tanggal Ambil</label>
          <input required class="
                  md-input 
                  {{$errors->has('tgl_ambil') ? ' md-input-danger' : ''}}
                  " type="date" value="{{Carbon\Carbon::now()->toDateString()}}" id="tgl_ambil" name="tgl_ambil" />
        </div>
        <div class="uk-form-row">
          <label for="tgl_kembali">Tanggal Kembali</label>
          <input required class="
                  md-input 
                  {{$errors->has('tgl_kembali') ? ' md-input-danger' : ''}}
                  " type="date" value="{{Carbon\Carbon::now()->toDateString()}}" id="tgl_kembali" name="tgl_kembali" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('pengambilan_errval'))
  @if($errors->has('pegawai_id'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('pengambilan_notfound'))
  swal("warning!", "ID pengambilan tidak ada.", "info");
  @elseif(Session::has('pengambilan_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('pengambilan_created'))
  swal("sukses!", "pengambilan berhasil ditambahkan.", "success");
  @elseif(Session::has('pengambilan_failed_creared'))
  swal("Maaf!", "gagal menambahkan data pengambilan.", "error");

  @elseif(Session::has('pengambilan_success_updated'))
  swal("Berhasil!", "data pengambilan berhasil di ubah.", "success");
  @elseif(Session::has('pengambilan_failed_updated'))
  swal("Maaf!", "data pengambilan gagal di ubah.", "error");

  @elseif(Session::has('pengambilan_success_deleted'))
  swal("Berhasil!", "data pengambilan berhasil dihapus.", "success");
  @elseif(Session::has('pengambilan_failed_deleted'))
  swal("Maaf!", "data Pegawai gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection