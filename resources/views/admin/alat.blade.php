@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Data alat Terbaru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah alat" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_alat'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
    </div>
    <div class="uk-grid uk-grid-width-large-1-6 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show"
      data-uk-sortable data-uk-grid-margin>
      @php $i=1; $colors = ['pink','teal','red','cyan','orange','teal']; @endphp @foreach($newalats as $np)
      <div>
        <div class="md-card md-card-hover md-card-overlay" data-snippet-title="Smooth scrolling to top of page">
          <div class="md-card-content md-bg-light-blue-800 uk-text-left " style="cursor: pointer; ">
            <br>
            <br>
            <h3 class="uk-text-contrast">{{$np->alat}}</h3>
            <hr class="divider">
          </div>
          <div class="md-card-overlay-content">
            <div class="uk-clearfix md-card-overlay-header">
              <i title="detail" data-uk-tooltip="{pos:'left'}" class="md-icon 
                  md-icon 
                  material-icons 
                  md-card-overlay-toggler"></i>
              <h4>
                <i class="material-icons md-20 uk-text-success">nature</i> ID : {{$np->id}}
              </h4>
            </div>
          </div>
        </div>
      </div>
      @php $i++; if($i>5){$i=1;}@endphp @endforeach
    </div>
    <br>
    <center>
      {{$newalats->links('pagination.uk')}}
    </center>
    <h4 class="heading_a uk-margin-bottom">List alat
      <a href="{{route('get-alat-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>ID</th>
              <th>Alat / Pekakas</th>
              <th>Jumlah</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($alats as $ins)
            <tr>
              <td>{{$ii++}}</td>
              <td>{{$ins->nama_alat}}</td>
              <td>{{$ins->jumlah}}</td>
              <td class="uk-text-center">
                <a data-uk-tooltip="{pos:'top'}" title="Update alat" href="{{route('get-alat-update',$ins->id)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus alat" href="{{route('get-alat-delete',$ins->id)}}">
                  <i class="md-icon material-icons uk-text-danger">remove_circle</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update alat modal --}} @if(Session::has('alat_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_alat" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('get-alat-updated',Session('alat')->id)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update Alat</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nama">Nama Alat / Pekakas</label>
          <input required  value="{{Session('alat')->nama_alat}}" class="
                  md-input 
                  {{$errors->has('alat') ? ' md-input-danger' : ''}}
                  " type="text" id="nama" name="nama" />
        </div>
        <div class="uk-form-row">
          <label for="jumlah">Jumlah</label>
          <input required  value="{{Session('alat')->jumlah}}" class="
                  md-input 
                  {{$errors->has('alat') ? ' md-input-danger' : ''}}
                  " type="number" id="jumlah" name="jumlah" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_alat()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_alat() {
              $('#update_alat').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update alat modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_alat" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-alat-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah alat</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="nama">Nama alat</label>
          <input required class="
                  md-input 
                  {{$errors->has('alat') ? ' md-input-danger' : ''}}
                  " type="text" id="nama" name="nama" />
        </div>
        <div class="uk-form-row">
          <label for="jumlah">Jumlah</label>
          <input required class="
                  md-input 
                  {{$errors->has('alat') ? ' md-input-danger' : ''}}
                  " type="number" id="jumlah" name="jumlah" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('alat_errval'))
  @if($errors->has('alat'))
  swal("Warning!", "Error Request! {{$errors->first('nama')}}", "warning");
  @endif
  @elseif(Session::has('alat_notfound'))
  swal("warning!", "ID alat tidak ada.", "info");
  @elseif(Session::has('alat_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('alat_created'))
  swal("sukses!", "alat berhasil ditambahkan.", "success");
  @elseif(Session::has('alat_failed_creared'))
  swal("Maaf!", "gagal menambahkan data alat.", "error");

  @elseif(Session::has('alat_success_updated'))
  swal("Berhasil!", "data alat berhasil di ubah.", "success");
  @elseif(Session::has('alat_failed_updated'))
  swal("Maaf!", "data alat gagal di ubah.", "error");

  @elseif(Session::has('alat_success_deleted'))
  swal("Berhasil!", "data alat berhasil dihapus.", "success");
  @elseif(Session::has('alat_failed_deleted'))
  swal("Maaf!", "data alat gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection