@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Tambah Data tugas Baru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah tugas" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_tugas'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
      <h4 class="heading_a uk-margin-right">List tugas
      <a href="{{route('get-tugas-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    </div>
    
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Id Tugas</th>
              <th>Tugas / Wilayah</th>
              <th>Jam Masuk</th>
              <th>Jam Selesai</th>
              <th>Update</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($tugass as $ins)
            <tr>
              <td>{{$ins->id_tugas}}</td>
              <td>{{$ins->tugas}}</td>
              <td>{{$ins->jam_masuk}}</td>
              <td>{{$ins->jam_selesai}}</td>
              <td>{{$ins->updated_at}}</td>
              <td class="uk-text-left">
                <a data-uk-tooltip="{pos:'top'}" title="Update tugas" href="{{route('get-tugas-update',$ins->id_tugas)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus tugas" href="{{route('get-tugas-delete',$ins->id_tugas)}}">
                  <i class="md-icon material-icons uk-text-danger">delete</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update tugas modal --}} @if(Session::has('tugas_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_tugas" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('get-tugas-updated',Session('tugas')->id_tugas)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update Alat</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="tugas">Nama tugas</label>
          <input required  value="{{Session('tugas')->tugas}}" class="
                  md-input 
                  {{$errors->has('tugas') ? ' md-input-danger' : ''}}
                  " type="text" id="tugas" name="tugas" />
        </div>
        <div class="uk-form-row">
          <label for="jam_masuk">Jam Masuk</label>
          <input required value="{{Session('tugas')->jam_masuk}}" class="
                  md-input 
                  {{$errors->has('tugas') ? ' md-input-danger' : ''}}
                  " type="text" id="jam_masuk" name="jam_masuk" />
        </div>
        <div class="uk-form-row">
          <label for="jam_selesai">Jam Selesai</label>
          <input required value="{{Session('tugas')->jam_selesai}}" class="
                  md-input 
                  {{$errors->has('tugas') ? ' md-input-danger' : ''}}
                  " type="text" id="jam_selesai" name="jam_selesai" />
        </div>
        
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_tugas()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_tugas() {
              $('#update_tugas').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update tugas modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_tugas" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-tugas-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah tugas</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="tugas">Nama tugas</label>
          <input required class="
                  md-input 
                  {{$errors->has('tugas') ? ' md-input-danger' : ''}}
                  " type="text" id="tugas" name="tugas" />
        </div>
        <div class="uk-form-row">
          <label for="jam_masuk">Jam Masuk</label>
          <input required class="
                  md-input 
                  {{$errors->has('tugas') ? ' md-input-danger' : ''}}
                  " type="text" id="jam_masuk" name="jam_masuk" />
        </div>
        <div class="uk-form-row">
          <label for="jam_selesai">Jam Selesai</label>
          <input required class="
                  md-input 
                  {{$errors->has('tugas') ? ' md-input-danger' : ''}}
                  " type="text" id="jam_selesai" name="jam_selesai" />
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('tugas_errval'))
  @if($errors->has('tugas'))
  swal("Warning!", "Error Request! {{$errors->first('tugas')}}", "warning");
  @endif
  @elseif(Session::has('tugas_notfound'))
  swal("warning!", "ID tugas tidak ada.", "info");
  @elseif(Session::has('tugas_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('tugas_created'))
  swal("sukses!", "tugas berhasil ditambahkan.", "success");
  @elseif(Session::has('tugas_failed_creared'))
  swal("Maaf!", "gagal menambahkan data tugas.", "error");

  @elseif(Session::has('tugas_success_updated'))
  swal("Berhasil!", "data tugas berhasil di ubah.", "success");
  @elseif(Session::has('tugas_failed_updated'))
  swal("Maaf!", "data tugas gagal di ubah.", "error");

  @elseif(Session::has('tugas_success_deleted'))
  swal("Berhasil!", "data tugas berhasil dihapus.", "success");
  @elseif(Session::has('tugas_failed_deleted'))
  swal("Maaf!", "data tugas gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection