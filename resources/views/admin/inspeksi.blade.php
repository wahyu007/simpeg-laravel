@extends('layouts.app') @section('_addmeta')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
@endsection @section('content') {{-- content --}}
<div id="page_content">
  <div id="page_content_inner">
    <!-- statistics (small charts) -->
    <div>
      <h3>Tambah Data inspeksi Baru
        <button data-uk-tooltip="{pos:'right'}" title="Tambah inspeksi" class="
            md-btn 
            md-btn-warning 
            md-btn-small 
            md-btn-wave-light 
            waves-effect 
            waves-button 
            waves-light" data-uk-modal="{target:'#add_inspeksi'}">
          <span class="menu_icon">
            <i class="material-icons uk-text-contrast">add</i>
          </span>
        </button>
      </h3>
      <h4 class="heading_a uk-margin-right">List inspeksi
      <a href="{{route('get-inspeksi-downloadrecord')}}" data-uk-tooltip="{pos:'right'}" title="Download Record">
        <i class="md-icon material-icons uk-text-primary">cloud_download</i>
      </a>
    </h4>
    </div>
    
    <div class="md-card uk-margin-medium-bottom">
      <div class="md-card-content">
        <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Id Inspeksi</th>
              <th>ID Jadwal</th>
              <th>Perangkat</th>
              <th>status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($inspeksis as $ins)
            <tr>
              <td>{{$ins->id_inspeksi}}</td>
              <td>{{$ins->jadwal->tugas->tugas}}</td>
              <td>{{$ins->perangkat}}</td>
              <td>{{$ins->status}}</td>
              <td class="uk-text-left">
                <a data-uk-tooltip="{pos:'top'}" title="Update inspeksi" href="{{route('get-inspeksi-update',$ins->id_inspeksi)}}">
                  <i class="md-icon material-icons uk-text-success">edit</i>
                </a>
                <a data-uk-tooltip="{pos:'top'}" title="Hapus inspeksi" href="{{route('get-inspeksi-delete',$ins->id_inspeksi)}}">
                  <i class="md-icon material-icons uk-text-danger">delete</i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- update inspeksi modal --}} @if(Session::has('inspeksi_update'))
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="update_inspeksi" aria-hidden="true" style="display: block; overflow-y: auto;">
    <div class="uk-modal-dialog">
      <form action="{{route('get-inspeksi-updated',Session('inspeksi')->id_inspeksi)}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Update Alat</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="id_jadwal">ID Jadwal</label>
          <select 
              id="id_jadwal" 
              name="id_jadwal" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="{{Session('inspeksi')->id_jadwal}}">{{Session('inspeksi')->jadwal->tugas->tugas}}</option>
              @foreach($id_jadwal as $op)
              <option value="{{$op->jadwal_id}}">{{$op->tugas->tugas}}</option>
              @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="perangkat">Perangkat</label>
          <select 
              id="perangkat" 
              name="perangkat" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="{{Session('inspeksi')->perangkat}}">{{Session('inspeksi')->perangkat}}</option>
              <option value="sam">sam card</option>
              <option value="modem">modem</option>
              <option value="epdp">epdp</option>
          </select>
        </div>
        <div class="uk-form-row">
          <label for="status">Status</label>
          <select 
              id="status" 
              name="status" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="{{Session('inspeksi')->status}}">{{Session('inspeksi')->status}}</option>
              <option value="normal">normal</option>
              <option value="perbaikan">perbaikan</option>
              <option value="error">error</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button onclick="close_update_inspeksi()" type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Batal</button>
          <script>
            function close_update_inspeksi() {
              $('#update_inspeksi').hide()
              console.log("hidden");

            }
          </script>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endif {{-- end update inspeksi modal --}} {{-- update modal --}}
<div class="uk-width-medium-2-3">
  <div class="uk-modal uk-open" id="add_inspeksi" aria-hidden="false" style="display: none; overflow-y: auto;">
    <div class="uk-modal-dialog" style="top: 269.5px;">
      <form action="{{route('post-inspeksi-add')}}" method="POST">
        <div class="uk-modal-header">
          <h3 class="uk-modal-title">Tambah inspeksi</h3>
        </div>
        {{ csrf_field() }}
        <div class="uk-form-row">
          <label for="id_jadwal">ID Jadwal</label>
          <select 
              id="id_jadwal" 
              name="id_jadwal" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="NULL">ID Jadwal</option>
              @foreach($id_jadwal as $op)
              <option value="{{$op->jadwal_id}}">{{$op->tugas->tugas}}</option>
              @endforeach
          </select>
        </div>
        <div class="uk-form-row">
          <label for="perangkat">Perangkat</label>
          <select 
              id="perangkat" 
              name="perangkat" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="NULL">perangkat</option>
              <option value="sam">sam card</option>
              <option value="modem">modem</option>
              <option value="epdp">epdp</option>
          </select>
        </div>
        <div class="uk-form-row">
          <label for="status">Status</label>
          <select 
              id="status" 
              name="status" 
              class="md-input {{$errors->has('jadwal') ? ' md-input-danger' : ''}}">
              <option value="NULL">status</option>
              <option value="normal">normal</option>
              <option value="perbaikan">perbaikan</option>
              <option value="error">error</option>
          </select>
        </div>
        <div class="uk-modal-footer uk-text-right">
          <button type="button" class="md-btn md-btn-flat md-btn-flat-danger uk-modal-close">Close</button>
          <button type="submit" class="md-btn md-btn-flat md-btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- end update modal --}}
<script>
  @if(Session::has('inspeksi_errval'))
  @if($errors->has('inspeksi'))
  swal("Warning!", "Error Request! {{$errors->first('inspeksi')}}", "warning");
  @endif
  @elseif(Session::has('inspeksi_notfound'))
  swal("warning!", "ID inspeksi tidak ada.", "info");
  @elseif(Session::has('inspeksi_failed'))
  swal("Maaf!", "Terjadi kesalahan system", "error");
  @elseif(Session::has('inspeksi_created'))
  swal("sukses!", "inspeksi berhasil ditambahkan.", "success");
  @elseif(Session::has('inspeksi_failed_creared'))
  swal("Maaf!", "gagal menambahkan data inspeksi.", "error");

  @elseif(Session::has('inspeksi_success_updated'))
  swal("Berhasil!", "data inspeksi berhasil di ubah.", "success");
  @elseif(Session::has('inspeksi_failed_updated'))
  swal("Maaf!", "data inspeksi gagal di ubah.", "error");

  @elseif(Session::has('inspeksi_success_deleted'))
  swal("Berhasil!", "data inspeksi berhasil dihapus.", "success");
  @elseif(Session::has('inspeksi_failed_deleted'))
  swal("Maaf!", "data inspeksi gagal dihapus.", "error");

  @endif
</script>
{{-- end content --}} @endsection @section('_addscript')
<!-- page specific plugins -->
<!-- datatables -->
<script src="{{asset('altair/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<!-- datatables tableTools-->
<script src="{{asset('altair/bower_components/datatables-tabletools/js/dataTables.tableTools.js')}}"></script>
<!-- datatables custom integration -->
<script src="{{asset('altair/assets/js/custom/datatables_uikit.min.js')}}"></script>
<!--  datatables functions -->
<script src="{{asset('altair/assets/js/pages/plugins_datatables.min.js')}}"></script>

<!--  dashbord functions -->
<script src="{{asset('altair/assets/js/pages/dashboard.min.js')}}"></script>
@endsection