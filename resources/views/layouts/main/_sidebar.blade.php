
  <!-- main sidebar -->
  <aside id="sidebar_main">
      <div class="sidebar_main_header">
        <div class="sidebar_logo">
          <a href="{{url('/')}}" class="sSidebar_hide">
            <img src="{{asset('images/delameta.png')}}" alt="PT. Delameta Bilano" />
          </a>
          <a href="{{url('/')}}" class="sSidebar_show">
            <img class="md-user-image" src="{{asset('images/delameta.png')}}" alt="PT. Delameta Bilano" height="50" width="50" />
          </a>
        </div>
        <div class="sidebar_actions">
          <h3>PT. Delameta Bilano</h3>
        </div>
      </div>
      <div class="menu_section">
        <ul>
        <li title="Dashboard" data-uk-tooltip="{pos:'right'}">
            <a href="{{route('dashboard')}}">
              <span class="menu_icon">
                <i class="material-icons">dashboard</i>
              </span>
              <span class="menu_title">Dashboard</span>
            </a>
          </li>
          <li>
            <a href="#">
              <span class="menu_icon">
                <i class="material-icons">fingerprint</i>
              </span>
              <span class="menu_title">Absen</span>
            </a>
            <ul>
              <li>
                <a href="{{route('absens')}}">List Hari Ini</a>
              </li>
              @if(Auth::user()->role == 1)
              <li>
                <a href="{{route('get-absen-records')}}">Records</a>
              </li>
              <li>
                <a href="{{route('get-rekapabsen-index')}}">Rekap Absen</a>
              </li>
              @endif
            </ul>
          </li>
          <li>
            <a href="{{route('get-jadwal-index')}}">
              <span class="menu_icon">
                <i class="material-icons">view_agenda</i>
              </span>
              <span class="menu_title">Jadwal Teknisi</span>
            </a>
            <ul>
              <li>
                <a href="{{route('get-jadwal-index')}}">
                  <span class="menu_icon">
                    <i class="material-icons">assignment</i>
                  </span>
                  <span class="menu_title">Jadwal</span>
                </a>
              </li>
              <li>
                <a href="{{route('get-tugas-index')}}">
                  <span class="menu_icon">
                    <i class="material-icons">build</i>
                  </span>
                  <span class="menu_title">Tugas</span>
                </a>
              </li>
              <li title="Ijin" data-uk-tooltip="{pos:'right'}">
                <a href="{{route('get-cuti-index')}}">
                  <span class="menu_icon">
                    <i class="material-icons">today</i>
                  </span>
                  <span class="menu_title">Ijin</span>
                </a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">
              <span class="menu_icon">
                <i class="material-icons">book</i>
              </span>
              <span class="menu_title">Laporan Kerja</span>
            </a>
            <ul>
              @if(Auth::user()->role == 1)
              <li>
                <a href="{{route('get-pengambilan-index')}}">Laporan Pengambilan</a>
              </li>
              @endif
              <li>
                <a href="{{route('get-perbaikan-index')}}">Laporan Perbaikan</a>
              </li>
              <li>
                <a href="{{route('get-laporan-harian-index')}}">Laporan Harian</a>
              </li>
              <li>
                <a href="{{route('get-rutin-index')}}">Laporan Perawatan Rutin</a>
              </li>
              <li>
                <a href="{{route('get-inspeksi-index')}}">Laporan Inspeksi Etoll</a>
              </li>
            </ul>
          </li>
          @if(Auth::user()->role == 2)
          <li title="Info Gaji" data-uk-tooltip="{pos:'right'}">
            <a href="{{route('get-gaji-index')}}">
              <span class="menu_icon">
                <i class="material-icons">account_balance_wallet</i>
              </span>
              <span class="menu_title">Info Gaji</span>
            </a>
          </li>
          @endif
          @if(Auth::user()->role == 1)
          <li>
            <a href="#">
              <span class="menu_icon">
                <i class="material-icons">group</i>
              </span>
              <span class="menu_title">Informasi Pegawai</span>
            </a>
            <ul>
              <li title="Pegawai" data-uk-tooltip="{pos:'right'}">
                <a href="{{route('get-pegawai-index')}}">
                  <span class="menu_icon">
                    <i class="material-icons">recent_actors</i>
                  </span>
                  <span class="menu_title">Data Pegawai</span>
                </a>
              </li>
              <li title="Info Gaji" data-uk-tooltip="{pos:'right'}">
                <a href="{{route('get-gaji-index')}}">
                  <span class="menu_icon">
                    <i class="material-icons">attach_money</i>
                  </span>
                  <span class="menu_title">Info Gaji</span>
                </a>
              </li>
              <!-- <li title="Jabatan" data-uk-tooltip="{pos:'right'}">
                <a href="{{route('get-jabatan-index')}}">
                  <span class="menu_icon">
                    <i class="material-icons">work</i>
                  </span>
                  <span class="menu_title">Jabatan</span>
                </a>
              </li> -->
            </ul>
          </li>
          <li>
            <a href="#">
              <span class="menu_icon">
                <i class="material-icons">archive</i>
              </span>
              <span class="menu_title">Sarana Kerja</span>
            </a>
            <ul>
              <li>
                <a href="{{route('get-alat-index')}}">Peralatan</a>
              </li>
              <li>
                <a href="{{route('get-component-index')}}">Komponen</a>
              </li>
              <li>
                <a href="{{route('get-mobil-index')}}">Kendaraan</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">
              <span class="menu_icon">
                <i class="material-icons">location_on</i>
              </span>
              <span class="menu_title">Informasi Lokasi</span>
            </a>
            <ul>
              <li>
                <a href="{{route('get-lokasi-index')}}">
                  <span class="menu_icon">
                    <i class="material-icons">location_on</i>
                  </span>
                  <span class="menu_title">Data Lokasi</span></a>
              </li>
            </ul>
          </li>
          <li title="User" data-uk-tooltip="{pos:'right'}">
            <a href="{{route('get-user-index')}}">
              <span class="menu_icon">
                <i class="material-icons">person</i>
              </span>
              <span class="menu_title">User</span>
            </a>
          </li>
          @endif
          <li title="Logout" data-uk-tooltip="{pos:'right'}">
            <a href="{{route('logout')}}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();"">
              <span class="menu_icon">
                <i class="material-icons">exit_to_app</i>
              </span>
              <span class="menu_title">Logout</span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <!-- main sidebar end -->
  