<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perbaikan extends Model
{
    //
    protected $table = 'perbaikans';
    protected $fillable = ['id_jadwal','id_lokasi',
                            'id_component','w_kerusakan',
                            'w_perbaikan','w_selesai_perbaikan',
                            'kerusakan','perbaikan','teknisi_id'];
    
    public function jadwal()
    {
        return $this->belongsTo('App\Jadwal','id_jadwal','jadwal_id');
    }

    public function lokasi()
    {
        return $this->belongsTo('App\Lokasi','id_lokasi','id_lokasi');
    }

    public function component()
    {
        return $this->belongsTo('App\Component','id_component','id_component');
    }
}
