<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekapAbsen extends Model
{
    //
    protected $fillable=['id_pegawai','bulan','tahun','tanggal','total'];

    public function pegawai()
	{
		return $this->belongsTo('App\Pegawai','id_pegawai','id_pegawai');
	}
}
