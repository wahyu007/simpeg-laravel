<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengambilan extends Model
{
    //
    protected $fillable=['id_jadwal','id_alat','id_mobil','tgl_kembali','tgl_ambil','status'];

    public function jadwal()
	{
		return $this->belongsTo('App\Jadwal','id_jadwal','jadwal_id');
    }

    public function alat()
	{
		return $this->belongsTo('App\Alat','id_alat','id_alat');
    }

    public function mobil()
	{
		return $this->belongsTo('App\Mobil','id_mobil','id');
    }

    public function get_jadwal()
    {
        return $this->hasMany('App\Jadwal');
    }
}
