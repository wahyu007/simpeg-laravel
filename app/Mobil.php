<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    //
    protected $table = 'tblmobil';
    protected $fillable = ['nopol','odometer','status_pajak','merk','type','tahun'];
}
