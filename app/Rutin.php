<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rutin extends Model
{
    //
    protected $fillable = ['id_component','kondisi','id_jadwal','kelas'];

    public function jadwal()
    {
        return $this->belongsTo('App\Jadwal','id_jadwal','jadwal_id');
    }

    public function lokasi()
    {
        return $this->belongsTo('App\Lokasi','id_lokasi','id_lokasi');
    }

    public function komponen()
    {
        return $this->belongsTo('App\Component','id_component','id_component');
    }

}
