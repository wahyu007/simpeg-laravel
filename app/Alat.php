<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alat extends Model
{
    //
    protected $table = 'tblalat';

    protected $fillable = ['nama_alat','jumlah'];
}
