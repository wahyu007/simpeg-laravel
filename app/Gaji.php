<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gaji extends Model
{
    //
    protected $table="gajis";
    protected $fillable = ["id_rekapabsen", 'id_pegawai',"gaji_pokok","tunjangan","potongan","total_absen","pendapatan","tanggal"];

    public function rekapabsen()
    {
        return $this->belongsTo('App\RekapAbsen','id_rekapabsen','id_rekapabsen');
    }

    // public function rekapabsen()
    // {
    //     return $this->belongsTo('App\RekapAbsen','id_rekapabsen','id_rekapabsen');
    // }
}
