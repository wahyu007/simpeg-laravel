<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $table = 'tblcuti';
    protected $fillable = [
        'id_pegawai','alasan','status_cuti','tanggal_mulai','tanggal_selesai','jenis'
    ];

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai','id_pegawai','id_pegawai');
    }
}
