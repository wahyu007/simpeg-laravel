<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    //
    protected $fillable = ['jadwal_id','id_pegawai','id_tugas','tanggal'];

    public function pegawai()
	{
		return $this->belongsTo('App\Pegawai','id_pegawai','id_pegawai');
    }

    public function tugas()
	{
		return $this->belongsTo('App\Tugas','id_tugas','id_tugas');
    }
}
