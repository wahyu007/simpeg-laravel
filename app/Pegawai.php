<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawais';
    protected $fillable = [
        'no_ktp','jenis_kelamin','alamat','tgl_masuk','tgl_lahir','status_pegawai','nama','nik','agama'
    ];

    public function absen()
    {
        return $this->hasMany('App\Absen','id_pegawai','id');
    }

    public static function null($pegawai)
    {
        return $pegawai == null;
    }
}
