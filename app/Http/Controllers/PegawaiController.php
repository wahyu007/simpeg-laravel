<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\Gaji;
use Carbon\Carbon;

use Redirect,Session,Validator;

class PegawaiController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
    }
  public function getPegawaiAll()
  {
    $pegawais = Pegawai::select('*')->paginate(6);
    $i = 1;
    $newPegawais = Pegawai::orderBy('created_at','desc')->take(5)->get();
    // return $newPegawai;
    return view('admin.pegawai',compact(
        'pegawais','newPegawais','i'
    ));
    // $newPegawais = Pegawai::all();
    // return view('admin.pegawai',compact('newPegawais'));
  }

  public function getPegawaiAllIndex()
  {
    $pegawais = Pegawai::all();
    $i = 1;
    return view('absen',compact('pegawais','i'));
  }

  public function getPegawaiByID($id)
  {
    $pegawai = Pegawai::find($id);
    if(Pegawai::null($pegawai)){
      Session::flash('pegawai_notfound',true);
      return Redirect::back();
      // return ['msg'=>'error : pegawai not found'];
    }
    Session::flash('pegawai_update',true);
    return Redirect::back();
    // return $pegawai;
  }

  public function postPegawai(Request $req)
  {
    // $validator = Validator::make($req->all(),[
    //   'nama'=>'required|string',
    //   'no_ktp'=>'required|string|max:225|unique:pegawais',
    //   'status_pegawai'=>'required|integer|min:1|max:2'
    // ]);
    // if($validator->fails()){
    //   // return $validator->errors();
    //   Session::flash('pegawai_errval',true);
    //   return Redirect::back()->withErrors('err',$validator->errors());
    // }
    $cek_ktp = Pegawai::where('no_ktp',$req->no_ktp)->count();
    if($cek_ktp != null){
      Session::flash('duplikat_pegawai_failed_created',true);
      return Redirect::back();
    }
    $pegawaiSaved = Pegawai::create([
      'nama'=>$req->nama,
      'nik' => $req->nik,
      'jenis_kelamin'=>$req->jenis_kelamin,
      'agama' => $req->agama,
      'no_ktp'=>$req->no_ktp,
      'status_pegawai'=>$req->status_pegawai,
      'tgl_lahir'=>$req->tgl_lahir,
      'alamat'=>$req->alamat,
      'tgl_masuk'=>$req->tgl_masuk,
    ]);
    if($pegawaiSaved){
      // $id = Pegawai::select('id_pegawai')->where('no_ktp',$req->no_ktp)->pluck('id_pegawai');
      // $savedgaji = Gaji::create([
      //   'id_pegawai'=>$id[0],
      //   'gaji_pokok'=>4200000
      // ]);
      Session::flash('pegawai_created',true);
      return Redirect::back();
      } 
    Session::flash('pegawai_failed_created',true);
    return Redirect::back();
    // dd($req);
  }

  public function getUpdate($id)
  {
    $pegawai = Pegawai::where('id_pegawai',$id)->first();
    if(Pegawai::null($pegawai)){
      Session::flash('pegawai_notfound',true);
      return Redirect::back();
      // return ['msg'=>'error : pegawai not found'];
    }
    Session::flash('pegawai_update',true);
    return Redirect::back()->with('pegawai',$pegawai);
    // return $pegawai;
  }
  
  public function postUpdate(Request $req,$id)
  {
    $validator = Validator::make($req->all(),[
      'nama'          => 'required|string|max:225',
      'status_pegawai'=> 'required|integer|max:2|min:0'
    ]);
    if($validator->fails()){
      Session::flash('pegawai_errval',true);
      return Redirect::back()->withErrors($validator->errors());
      // return ['msg'=>'error','data'=>$validator->errors()];
    }
    
    $pegawai = Pegawai::where('id_pegawai',$id);
    if($pegawai == null){
      Session::flash('pegawai_notfound',true);
      return Redirect::back();
      // return ['msg'=>'error : pegawai not found'];
    }

    $pegawai_update = $pegawai->update([
      'nama'=>$req->nama,
      'nik' => $req->nik,
      'jenis_kelamin'=>$req->jenis_kelamin,
      'agama' => $req->agama,
      'no_ktp'=>$req->no_ktp,
      'status_pegawai'=>$req->status_pegawai,
      'tgl_lahir'=>$req->tgl_lahir,
      'alamat'=>$req->alamat,
      'tgl_masuk'=>$req->tgl_masuk
    ]);

    if($pegawai_update){
      Session::flash('pegawai_success_updated',true);
      return Redirect::back();
      // return ['msg'=>'success','data'=>$pegawai];
    }Session::flash('pegawai_failed_deleted',true);
    return Redirect::back();
    // return ['msg'=>'error : update failed'];
  }

  public function getDeleteByID($id)
  {
    $pegawai = Pegawai::where('id_pegawai',$id);
    if(Pegawai::null($pegawai)){
      Session::flash('pegawai_notfound',true);
      return Redirect::back();
      // return ['msg'=>'error : pegawai not found'];
    }
    // $deleted_absen = \App\Absen::deleteAllCollection($pegawai->absen);
    if($pegawai->delete()){
      Session::flash('pegawai_success_deleted',true);
      return Redirect::back();
      // return ['msg'=>'success delete record'];
    }
    Session::flash('pegawai_failed_deleted',true);
    return Redirect::back();
    // return ['msg'=>'failed delete record'];
  }
}
