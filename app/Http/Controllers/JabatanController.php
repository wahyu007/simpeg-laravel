<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Jabatan as Ansi;
use Carbon\Carbon;

class JabatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getJabatan()
    {
        $newjabatans = Ansi::orderBy('id','desc')->take(12)->paginate(6);
        $jabatans = Ansi::all();
        return view('admin.jabatan',compact('jabatans','newjabatans'));
    }

    public function postJabatanAdd(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'jabatan' => 'required|string|max:225'
        ]);if($validator->fails()){
            Session::flash('jabatan_errval',true);
            return Redirect::bask();
            // return $validator->errors();
        }
        
        $newjabatan = Ansi::create($req->all());
        if($newjabatan){
            Session::flash('jabatan_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('jabatan_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditJabatan($id)
    {
        $jabatan = Ansi::find($id);if($jabatan == null){
            Session::flash('jabatan_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('jabatan_update',true);
        return Redirect::back()->with('jabatan',$jabatan);
    }

    public function postUpdateJabatan(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'jabatan' => 'required'
        ]);if($validator->fails()){
            Session::flash('jabatan_errval',true);
            return Redirect::back()->withErrors($validator->errors());
        }

        $upjabatan = Ansi::find($id)->update([
            'jabatan' => $req->jabatan
        ]);if($upjabatan){
            Session::flash('jabatan_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('jabatan_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteJabatanByID($id)
    {
        $jabatan = Ansi::find($id);if($jabatan == null){
            Session::flash('jabatan_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($jabatan->delete()){
            Session::flash('jabatan_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('jabatan_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
