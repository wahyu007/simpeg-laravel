<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Pengambilan as Ansi;
use App\Jadwal,App\Mobil,App\Alat;
use Carbon\Carbon;

class PengambilanController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getPengambilan()
    {
        $newPengambilans = Ansi::orderBy('id_pengambilan','desc')->take(12)->paginate(6);
        $Pengambilans = Ansi::all();
        $jadwal = Jadwal::all();
        $mobil = Mobil::where('status', '1')->get();
        $alat = Alat::where('jumlah', '!=', '0')->get();
        $i=1;
        return view('admin.pengambilan',compact('alat','Pengambilans','newPengambilans','i','jadwal','mobil'));
        // $tes = Alat::select('jumlah')->where('id_alat', '9')->first()->jumlah;
        // echo $tes;
    }

    public function postPengambilanAdd(Request $req)
    {
        // $validator = Validator::make($req->all(),[
        //     'nama' => 'required|string|min:5|max:225'
        // ]);if($validator->fails()){
        //     Session::flash('pengambilan_errval',true);
        //     return Redirect::back();
        //     // return $validator->errors();
        // }

        $jumlah_alat = Alat::select('jumlah')->where('id_alat', $req->id_alat)->first()->jumlah;
        Alat::where('id_alat', $req->id_alat)->update([
            'jumlah' => $jumlah_alat-1
        ]);

        Mobil::where('id', $req->id_mobil)->update(['status' => '2']);
        $newpengambilan = Ansi::create([
            'id_jadwal' => $req->id_jadwal,
            'id_mobil' => $req->id_mobil,
            'id_alat' => $req->id_alat,
            'status' => '1',
            'tgl_ambil' => $req->tgl_ambil,
            'tgl_kembali' => $req->tgl_kembali
        ]);
        if($newpengambilan){
            Session::flash('pengambilan_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('pengambilan_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
        // echo $req;
    }

    public function getEditPengambilan($id)
    {
        $pengambilan = Ansi::where('id_pengambilan',$id)->first();
        if($pengambilan == null){
            Session::flash('pengambilan_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('pengambilan_update',true);
        return Redirect::back()->with('pengambilan',$pengambilan);
    }

    public function postUpdatePengambilan(Request $req, $id)
    {
        $jumlah_alat = Alat::select('jumlah')->where('id_alat', $req->id_alat)->first()->jumlah;
        Alat::where('id_alat', $req->id_alat)->update([
            'jumlah' => $jumlah_alat+1
        ]);

        Mobil::where('id', $req->id_mobil)->update(['status' => '1']);

        $uppengambilan = Ansi::where('id_pengambilan',$id)->update([
            'status' => 2
        ]);
        if($uppengambilan){
            Session::flash('pengambilan_success_updated',true);
            return Redirect::back();
        }
        Session::flash('pengambilan_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeletePengambilanByID($id)
    {
        $pengambilan = Ansi::find($id);if($pengambilan == null){
            Session::flash('pengambilan_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($pengambilan->delete()){
            Session::flash('pengambilan_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('pengambilan_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
