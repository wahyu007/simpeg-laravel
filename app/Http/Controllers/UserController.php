<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User as Ansi;
use App\Pegawai;
use Auth;
use Redirect,Session,Validator;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getUser()
    {
        $data = [];
        $data['user'] = Ansi::all();
        $data['i'] = 1;
        $data['pegawai'] = Pegawai::all();
        return view('admin.user.user',$data);
    }

    public function getProfile()
    {
        $user = Ansi::where('pegawai_id',Auth::user()->pegawai_id)->get();
        $pegawai = Pegawai::where('id_pegawai', Auth::user()->pegawai_id)->get();
        return view('admin.user.profile',compact('pegawai', 'user'));
    }
    public function postUser(Request $req)
    {
        // $validator = Validator::make($req->all(),[
        // 'nama'=>'required|string',
        // 'no_ktp'=>'required|string|max:225|unique:pegawais',
        // 'status_pegawai'=>'required|integer|min:1|max:2'
        // ]);if($validator->fails()){
        // // return $validator->errors();
        // Session::flash('pegawai_errval',true);
        // return Redirect::back()->withErrors('err',$validator->errors());
        // }

        // return "Kambing";

        $pegawaiSaved = Ansi::create([
        'name'=>$req->nama,
        'email'=>$req->email,
        'password'=> Hash::make($req->password),
        'pegawai_id'=>$req->pegawai_id,
        'role'=>$req->role
        ]);
        
        if($pegawaiSaved){
            Session::flash('pegawai_created',true);
            return Redirect::back();
        }

        // Session::flash('pegawai_failed_creared',true);
        // return Redirect::back();
    }

    public function getUserByID($id)
    {
        $usera = Ansi::find($id);
        $pegawai = Pegawai::all();
        // if(!$user){
        //     Session::flash('user_notfound',true);
        //     return Redirect::back();
        //     // return ['msg'=>'error : user not found'];
        // }
        Session::flash('user_update',true);
        return Redirect::back()->with('usera',$usera,'pegawai',$pegawai);
        // return $user;
    }

    public function postUpdate(Request $req,$id)
    {
        // $validator = Validator::make($req->all(),[
        // 'nama'          => 'required|string|max:225',
        // 'status_user'=> 'required|integer|max:2|min:0'
        // ]);
        // if($validator->fails()){
        // Session::flash('user_errval',true);
        // return Redirect::back()->withErrors($validator->errors());
        // // return ['msg'=>'error','data'=>$validator->errors()];
        // }
        
        $user = Ansi::find($id);
        if(!$user){
            Session::flash('user_notfound',true);
            return Redirect::back();
            // return ['msg'=>'error : user not found'];
        }
        if($req->password == null){
            $password = $req->password_old;
        } else {
            $password = Hash::make($req->password);
        }

        $user_update = $user->update([
        'name'=>$req->nama,
        'email'=>$req->email,
        'password'=>$password,
        'pegawai_id'=>$req->pegawai_id,
        'role'=>$req->role
        ]);

        if($user_update){
        Session::flash('user_updated',true);
        return Redirect::back();
        // return ['msg'=>'success','data'=>$user];
        }Session::flash('user_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'error : update failed'];
    }


}
