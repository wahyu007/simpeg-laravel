<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator,Auth;
// use App\Alat as Ansi;
use Carbon\Carbon;
use App\Gaji;
use App\RekapAbsen;

class GajiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getGaji()
    {
        $no=1;
        $tanggal = Carbon::now()->toDateString();
        if( Auth::user()->role == '1'){
            $gaji = Gaji::all();
        } else {
        //     $rekap_absen_id = RekapAbsen::select('id_rekapabsen')->where('id_pegawai', Auth::user()->pegawai_id)->get();
        //     $gaji = Gaji::where('id_rekapabsen', Auth::user()->pegawai_id)->get();

            
            $rekap_absen_id = RekapAbsen::select('id_rekapabsen')->where('id_pegawai', Auth::user()->pegawai_id)->get();
            $gaji = Gaji::whereIn('id', $rekap_absen_id)->get();
        }
        // $join = Gaji::leftJoin('pegawais', 'gajis.id_pegawai', '=', 'pegawais.id_pegawai')->get();
        // $gajipertanggal = Gaji::where('tanggal',$tanggal)->get();
        return view('admin.gaji',compact('gaji','no'));
        // echo $gaji;

        // foreach($gaji as $g){
        //     echo $g->id;
        //     echo $g->rekapabsen->id_pegawai;
        //     echo $g->rekapabsen->pegawai->nama;
        // }
    }
}
