<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Alat as Ansi;
use Carbon\Carbon;

class AlatController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getAlat()
    {
        $newalats = Ansi::orderBy('id_alat','desc')->take(12)->paginate(6);
        $alats = Ansi::all();
        $ii = 1;
        return view('admin.alat',compact('alats','newalats','ii'));
    }

    public function postAlatAdd(Request $req)
    {
        // $validator = Validator::make($req->all(),[
        //     'alat' => 'required|string|max:225'
        // ]);if($validator->fails()){
        //     Session::flash('alat_errval',true);
        //     return Redirect::bask();
        //     // return $validator->errors();
        // }
        
        $newalat = Ansi::create([
            'nama_alat' => $req->nama,
            'jumlah' => $req->jumlah,
            ]);
        if($newalat){
            Session::flash('alat_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('alat_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditAlat($id)
    {
        $alat = Ansi::find($id);if($alat == null){
            Session::flash('alat_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('alat_update',true);
        return Redirect::back()->with('alat',$alat);
    }

    public function postUpdateAlat(Request $req, $id)
    {
        // $validator = Validator::make($req->all(),[
        //     'alat' => 'required'
        // ]);
        // if($validator->fails()){
        //     Session::flash('alat_errval',true);
        //     return Redirect::back()->withErrors($validator->errors());
        // }

        $upalat = Ansi::find($id)->update([
            'nama_alat' => $req->nama,
            'jumlah' => $req->jumlah,
        ]);
        if($upalat){
            Session::flash('alat_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('alat_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteAlatByID($id)
    {
        $alat = Ansi::where('id_alat',$id);
        if($alat == null){
            Session::flash('alat_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($alat->delete()){
            Session::flash('alat_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('alat_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
