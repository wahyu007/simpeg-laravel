<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Tugas as Ansi;
use Carbon\Carbon;

class TugasController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getTugas()
    {
        $newtugass = Ansi::orderBy('id_tugas','desc')->take(12)->paginate(6);
        $tugass = Ansi::all();
        return view('admin.tugas',compact('tugass','newtugass'));
    }

    public function postTugasAdd(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'tugas' => 'required|string|max:23'
        ]);if($validator->fails()){
            Session::flash('tugas_errval',true);
            return Redirect::bask();
            // return $validator->errors();
        }
        
        $newtugas = Ansi::create([
            'tugas' => $req->tugas,
            'jam_masuk' => $req->jam_masuk,
            'jam_selesai'=>$req->jam_selesai
            ]);
        if($newtugas){
            Session::flash('tugas_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('tugas_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditTugas($id)
    {
        $tugas = Ansi::where('id_tugas',$id)->first();
        if($tugas == null){
            Session::flash('tugas_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('tugas_update',true);
        return Redirect::back()->with('tugas',$tugas);
    }

    public function postUpdateTugas(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'tugas' => 'required'
        ]);
        if($validator->fails()){
            Session::flash('tugas_errval',true);
            return Redirect::back()->withErrors($validator->errors());
        }

        $uptugas = Ansi::where('id_tugas',$id)->update([
            'tugas' => $req->tugas,
            'jam_masuk' => $req->jam_masuk,
            'jam_selesai'=>$req->jam_selesai
        ]);
        if($uptugas){
            Session::flash('tugas_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('tugas_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteTugasByID($id)
    {
        $tugas = Ansi::where('id_tugas',$id);
        if($tugas == null){
            Session::flash('tugas_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($tugas->delete()){
            Session::flash('tugas_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('tugas_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}

