<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Lokasi as Ansi;
use Carbon\Carbon;

class LokasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLokasi()
    {
        $newlokasis = Ansi::orderBy('id_lokasi','desc')->take(12)->paginate(6);
        $lokasis = Ansi::all();
        $i = 1;
        return view('admin.lokasi',compact('lokasis','newlokasis','i'));
    }

    public function postLokasiAdd(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'cabang' => 'required|string|max:23',
            'gerbang' => 'required|string|max:23',
            'gardu' => 'required|string|max:23'
        ]);
        $cek_gardu = Ansi::where([
            'cabang' => $req->cabang,
            'gerbang' => $req->gerbang,
            'gardu' => $req->gardu
        ])->first();
        if($validator->fails()){
            Session::flash('lokasi_errval',true);
            return Redirect::bask();
            // return $validator->errors();
        }
        if($cek_gardu == null){
            $newlokasi = Ansi::create([
                'cabang' => $req->cabang,
                'gerbang' => $req->gerbang,
                'gardu' => $req->gardu
            ]);
            if($newlokasi){
                Session::flash('lokasi_created',true);
                return Redirect::back();
                // return ['msg'=>'success add'];
            } else{
                Session::flash('lokasi_failed',true);
                return Redirect::back();
            }
            
        }
        Session::flash('lokasi_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditLokasi($id)
    {
        $lokasi = Ansi::where('id_lokasi',$id)->first();
        if($lokasi == null){
            Session::flash('lokasi_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('lokasi_update',true);
        return Redirect::back()->with('lokasi',$lokasi);
    }

    public function postUpdateLokasi(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'cabang' => 'required'
        ]);if($validator->fails()){
            Session::flash('lokasi_errval',true);
            return Redirect::back()->withErrors($validator->errors());
        }

        $uplokasi = Ansi::where('id_lokasi',$id)->update([
            'cabang' => $req->cabang,
            'gerbang' => $req->gerbang,
            'gardu' => $req->gardu
        ]);if($uplokasi){
            Session::flash('lokasi_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('lokasi_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteLokasiByID($id)
    {
        $lokasi = Ansi::where('id_lokasi','=',$id);
        if($lokasi == null){
            Session::flash('lokasi_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($lokasi->delete()){
            Session::flash('lokasi_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('lokasi_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}

