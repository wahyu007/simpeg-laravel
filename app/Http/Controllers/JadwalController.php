<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal as Ansi;
use Carbon\Carbon;
use App\Pegawai;
use App\Tugas;
use Auth,Session,Redirect;

class JadwalController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getJadwal()
    {
        $newjadwals = Ansi::orderBy('jadwal_id','desc')->take(12)->paginate(6);
        $jadwals = Ansi::orderBy('tanggal', 'desc')->take(12)->paginate(6);
        $i = 1;
        return view('admin.jadwal',compact('jadwals','newjadwals','alat','i'));
    }
    public function getJadwalTambah()
    {
        $newjadwals = Ansi::orderBy('jadwal_id','desc')->take(12)->paginate(6);
        $jadwals = Ansi::all();
        $tugas = Tugas::all();
        $pegawai = Pegawai::all();
        return view('admin.jadwal.tambah',compact('jadwals','newjadwals','tugas','pegawai'));
    }

    public function getJadwalView()
    {
        $newjadwals = Ansi::orderBy('jadwal_id','desc')->take(12)->paginate(6);
        $jadwals = Ansi::all();
        $alat = Alat::all();
        return view('admin.jadwal.view',compact('jadwals','newjadwals','alat'));
    }

    public function getJadwalTugas($id)
    {   
        // $tugas = Tugas::where('id_tugas','=',$id)->first();
        // if($tugas == null){
        //     Session::flash('tugas_notfound',true);
        //     return Redirect::back();
        //     // return ['obj'=>null];
        // }
        $tugas = Ansi::join('tugas', 'jadwals.id_tugas', '=', 'tugas.id_tugas')
            ->join('pegawais', 'jadwals.nik', '=', 'pegawais.id_pegawai')
            ->select('jadwals.tanggal', 'tugas.tugas', 'tugas.jam_masuk', 'tugas.jam_selesai','pegawais.nama')
            ->where('jadwals.jadwal_id',$id)->first();
        if($tugas == null){
            Session::flash('tugas_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }

        Session::flash('tugas_view',true);
        return Redirect::back()->with('tugas',$tugas);
        // echo $tugas;
    }

    public function postJadwalAdd(Request $req)
    {
        // $validator = Validator::make($req->all(),[
        //     'uraian_jadwal' => 'string|min:1',
        //     'uraian_kerusakan' => 'required',
        // ]);
        
        // if($validator->fails()){
        //     Session::flash('jadwal_notComplete',true);
        //     return Redirect::back();
        //     // return $validator->errors();
        // }
        // $teknisi = Pegawai::where('id',$req->teknisi_id)->get();
        // if($req->pulang <= $req->masuk){
        //     Session::flash('jadwal_notMatch',true);
        //     return Redirect::back();
        // } 
        // if(!$teknisi){
        //     Session::flash('jadwal_notMatch',true);
        //     return Redirect::back();
        // } else{
            $newjadwal = Ansi::create([
                'id_pegawai' =>$req->nik,
                'id_tugas' => $req->id_tugas,
                'tanggal' => $req->tanggal,
                ]);
        // }

        if($newjadwal){
            Session::flash('jadwal_created',true);
            return Redirect()->route('get-jadwal-index');
            // return ['msg'=>'success add'];
        }
        Session::flash('jadwal_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditJadwal($id)
    {   
        $jadwal = Ansi::join('tugas', 'jadwals.id_tugas', '=', 'tugas.id_tugas')
            ->join('pegawais', 'jadwals.nik', '=', 'pegawais.id_pegawai')
            ->select('jadwals.jadwal_id','jadwals.tanggal','pegawais.id_pegawai','pegawais.nama','tugas.id_tugas', 'tugas.tugas as nama_tugas','tugas.jam_masuk', 'tugas.jam_selesai')
            ->where('jadwals.jadwal_id',$id)->get();
        // $jadwal = Ansi::where('jadwal_id',$id)->get();
        $tugass = Tugas::all();
        $pegawai = Pegawai::all();
        if($jadwal == null){
            Session::flash('jadwal_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        // Session::flash('jadwal_update',true);
        return view('admin.jadwal.edit',compact('jadwal','tugass','pegawai'));
        // echo $jadwal;
    }
    public function getViewJadwal($id)
    {
        $jadwal = Ansi::where('jadwal_id',$id)->get();
        $supervisor = Pegawai::where('jabatan','supervisor')->get();
        $teknisi = Pegawai::where('jadwal_id','1')->get();
        if($jadwal == null){
            Session::flash('jadwal_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        // Session::flash('jadwal_update',true);
        return view('admin.jadwal.view',compact('jadwal','supervisor','teknisi'));
    }

    public function postUpdateJadwal(Request $req, $id)
    {
        // var_dump($req);
        // echo "kambing";
        $upjadwal = Ansi::where('jadwal_id',$id)->update([
            'nik' =>$req->id,
            'id_tugas' => $req->id_tugas,
            'tanggal' => $req->tanggal,
        ]);
        if($upjadwal){
            Session::flash('jadwal_success_updated',true);
            return Redirect()->route('get-jadwal-index');
            // return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('jadwal_failed_updated');
        return Redirect::back();
        return 'kambing';
    }

    public function getDeleteJadwalByID($id)
    {
        $jadwal = Ansi::where('jadwal_id','=',$id);
        if($jadwal == null){
            Session::flash('jadwal_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($jadwal->delete()){
            Session::flash('jadwal_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('jadwal_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
