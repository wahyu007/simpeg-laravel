<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perbaikan as Ansi;
use App\Alat;
use Carbon\Carbon;
use App\Pegawai;
use App\Jadwal;
use App\Lokasi;
use App\Component;
use Auth;
// use App\Mobil as Ansi;

use Redirect,Session,Validator;

class PerbaikanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getPerbaikan()
    {
        $newperbaikans = Ansi::orderBy('id_perbaikan','desc')->take(12)->paginate(6);
        $perbaikans = Ansi::all();
        $i = 1;
        return view('admin.perbaikan',compact('perbaikans','newperbaikans','alat','i'));
    }
    public function getPerbaikanTambah()
    {
        $newperbaikans = Ansi::orderBy('id_perbaikan','desc')->take(12)->paginate(6);
        $perbaikans = Ansi::all();
        $alat = Alat::all();
        $jadwal = Jadwal::all();
        $lokasi = \App\Lokasi::all();
        $component = \App\Component::all();
        return view('admin.perbaikan.tambah',compact('component','perbaikans','newperbaikans','alat','jadwal','lokasi'));
    }

    public function getPerbaikanView()
    {
        $newperbaikans = Ansi::orderBy('id_perbaikan','desc')->take(12)->paginate(6);
        $perbaikans = Ansi::all();
        $alat = Alat::all();
        return view('admin.perbaikan.view',compact('perbaikans','newperbaikans','alat'));
    }

    public function postPerbaikanAdd(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'perbaikan' => 'string|min:1',
            'kerusakan' => 'required',
        ]);
        
        if($validator->fails()){
            Session::flash('perbaikan_notComplete',true);
            return Redirect::back();
            // return $validator->errors();
        }
        if($req->waktu_selesai <= $req->waktu_perbaikan && $req->waktu_perbaikan <= $req->waktu_kerusakan){
            Session::flash('perbaikan_notMatch',true);
            return Redirect::back();
        } 
        // elseif($req->waktu_perbaikan <= $req->waktu_kerusakan){
        //     Session::flash('perbaikan_notMatch',true);
        //     return Redirect::back();
        // }
        // $teknisi_id = Auth::user()->pegawai_id;
        $newperbaikan = Ansi::create([
            'id_jadwal' =>$req->id_jadwal,
            'id_lokasi' => $req->id_lokasi,
            'id_component' => $req->id_component,
            'w_kerusakan' => $req->waktu_perbaikan,
            'w_perbaikan' => $req->waktu_kerusakan,
            'w_selesai_perbaikan' => $req->waktu_selesai,
            'kerusakan' => $req->kerusakan,
            'perbaikan' => $req->perbaikan
            ]);

        if($newperbaikan){
            Session::flash('perbaikan_created',true);
            return Redirect::route('get-perbaikan-index');
            // return ['msg'=>'success add'];
        }
        Session::flash('perbaikan_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditPerbaikan($id)
    {
        $perbaikan = Ansi::where('id_jadwal',$id)->get();
        $alat = Alat::all();
        $lokasi = Lokasi::all();
        $component = Component::all();
        $pegawai = Pegawai::all();
        $id_jadwal = Jadwal::where('id_pegawai', 23)->get();
        if($perbaikan == null){
            Session::flash('perbaikan_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        // Session::flash('perbaikan_update',true);
        return view('admin.perbaikan.edit',compact('perbaikan','alat','id_jadwal','lokasi','component'));
    }
    public function getViewPerbaikan($id)
    {
        $perbaikan = Ansi::where('id',$id)->get();
        $supervisor = Pegawai::where('jabatan','supervisor')->get();
        $teknisi = Pegawai::where('id','1')->get();
        if($perbaikan == null){
            Session::flash('perbaikan_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        // Session::flash('perbaikan_update',true);
        return view('admin.perbaikan.view',compact('perbaikan','supervisor','teknisi'));
    }

    public function postUpdatePerbaikan(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'perbaikan' => 'string|min:1',
            'kerusakan' => 'required',
        ]);
        
        if($validator->fails()){
            Session::flash('perbaikan_notComplete',true);
            return Redirect::back();
            // return $validator->errors();
        }
        if($req->waktu_selesai <= $req->waktu_perbaikan && $req->waktu_perbaikan <= $req->waktu_kerusakan){
            Session::flash('perbaikan_notMatch',true);
            return Redirect::back();
        } 

        $upperbaikan = Ansi::where('id_jadwal',$id)->update([
            'id_jadwal' =>$req->id_jadwal,
            'id_lokasi' => $req->id_lokasi,
            'id_component' => $req->id_component,
            'w_kerusakan' => $req->waktu_perbaikan,
            'w_perbaikan' => $req->waktu_kerusakan,
            'w_selesai_perbaikan' => $req->waktu_selesai,
            'kerusakan' => $req->kerusakan,
            'perbaikan' => $req->perbaikan
        ]);
        if($upperbaikan){
            Session::flash('perbaikan_success_updated',true);
            return Redirect()->route('get-perbaikan-index');
            // return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('perbaikan_failed_updated');
        return Redirect::back();
        // return 'kambing';
    }

    public function getDeletePerbaikanByID($id)
    {
        $perbaikan = Ansi::where('id_jadwal',$id);
        if($perbaikan == null){
            Session::flash('perbaikan_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($perbaikan->delete()){
            Session::flash('perbaikan_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('perbaikan_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
