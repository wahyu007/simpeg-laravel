<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Session,Redirect,Validator,Auth;
use App\Absen,App\Cuti,App\Instansi,App\Mutasi,App\Pegawai,App\Stugas,App\User;
use App\Gaji;
use App\Mobil;
use App\Alat;
use App\Lokasi;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(){
        $total_pegawai = Pegawai::count();
        $total_mobil = Mobil::count();
        $total_alat = Alat::count();
        $total_lokasi = Lokasi::count();

        // echo $total_lokasi;
        return view('admin.dashboard', compact('total_pegawai','total_alat','total_mobil','total_lokasi'));
    }
    public function pegawai()
    {
        $pegawais = Pegawai::select('*')->paginate(6);
        $newPegawais = Pegawai::orderBy('created_at','desc')->take(5)->get();
        $allpegawai = Pegawai::all();
        // return $newPegawai;
        return view('admin.pegawai',compact(
            'pegawais','newPegawais','allpegawai'
        ));
    }

    public function absen()
    {
        $absens = Absen::select('*')->paginate(10);
        // return $absens;
        $no = 1;
        $pegawais = Pegawai::select('*')->paginate(6);
        $all = Pegawai::where('id_pegawai', Auth::user()->pegawai_id)->get();
        $absenRecordsToday = Absen::where(
            'created_at','>=',Carbon::today()
            )->get();
        
        return view('admin.absen_today',compact('pegawais','absens','absenRecordsToday','all','no'));
        // return $absenRecordsToday;
    }

    public function recordAbsens()
    {
        $recordGroup = Absen::select('id','pegawai_id','created_at','updated_at')
            ->get()
            ->groupBy(function($date){
                return Carbon::parse($date->created_at)->format('Y-m-d');
            });
        
        return view('admin.record_absen',compact('recordGroup'));
    }
}