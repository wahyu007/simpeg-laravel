<?php

namespace App\Http\Controllers;
use Session,Redirect,Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Absen,App\Cuti,App\Instansi,App\Mutasi,App\Pegawai,App\Stugas,App\User,App\RekapAbsen;
use App\Gaji;

class AbsenController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
    }

  public function getAbsenAll()
  {
    $pegawais = Pegawai::get();
    // // $pegawais = Pegawai::select('*')->paginate(6);
    // $newPegawais = Pegawai::orderBy('created_at','desc')->take(5)->get();
    $absens = Absen::orderBy('created_at','desc')->get();
    return $absen;
    // return view('admin.absen_today',compact('absens','pegawais','newPegawais'));
  }

  public function postAbsen(Request $req)
  {
    $validator = Validator::make($req->all(), [
      'pegawai_id'  => 'required'
    ]);
    if($validator->fails()){
      // return ['msg'=>'error','error'=>$validator->errors()];
      Session::flash('req_error',true);
      return Redirect::back();
    }
    $bulan = Carbon::now()->month;
    $tahun = Carbon::now()->year;
    $date = Carbon::now()->toDateString();
    $pegawai = \App\Pegawai::where('pegawai_id',$req->pegawai_id);
    if(\App\Pegawai::null($pegawai)){
      return ['msg'=>'error : pegawai not found']; 
    } 
    $cari_absen = RekapAbsen::where([
      'bulan' => $bulan,
      'tahun' => $tahun,
      'id_pegawai' => $req->pegawai_id
      ])->first();

      function create_gaji($id_pegawai, $bulan , $tahun){
        RekapAbsen::create([
          'id_pegawai' => $id_pegawai,
          'bulan'=> $bulan,
          'tahun' => $tahun,
          'total' => 1
        ]);
        $id_rekapabsen = RekapAbsen::select('id_rekapabsen')->where([
          'id_pegawai' => $id_pegawai,
          'bulan'=> $bulan,
          'tahun' => $tahun,
        ])->first()->id_rekapabsen;
  
        Gaji::create([
          'id_rekapabsen' => $id_rekapabsen,
          'gaji_pokok'=> 4100000
        ]);
      }
      function update_gaji($cari_absen){
        $total_absen = $cari_absen->total + 1;
        RekapAbsen::where([
          'id_rekapabsen' => $cari_absen->id_rekapabsen
        ])->update(['total' => $total_absen]);
        
        if($total_absen <= 22){
          $lembur = 0;
          $potongan = (22 - $total_absen )* 80000;
        } else if($total_absen >= 22){
          $lembur = (22 - $total_absen) * 80000;
          $potongan = 0;
        }
  
        $gaji = Gaji::where([
          'id_rekapabsen' => $cari_absen->id_rekapabsen
        ])->first();
  
        $gaji->update([
          'lembur' => $lembur ,
          'potongan' => $potongan,
          'pendapatan' => $gaji->gaji_pokok + $lembur - $potongan  
        ]);
      }

    if(!$cari_absen){
      create_gaji($req->pegawai_id, $bulan , $tahun);
    } else{
      update_gaji($cari_absen);
    }

    $sudahAbsen = Absen::whereDate('created_at',$date)->where('pegawai_id',$req->pegawai_id)->count();
    if($sudahAbsen != null){
      Session::flash('sudah_absen',true);
      return Redirect::back();
    } else{
      $absen = Absen::create([
        'pegawai_id' => $req->pegawai_id,
        'jam_masuk' => Carbon::now()->toTimeString()
      ]);
    }
    if($absen){
      Session::flash('absen_created',true);
      return Redirect::back();
    }
    Session::flash('absen_failed',true);
    return Redirect::back();
  }

  public function getAbsenByID($id)
  {
    $absen = Absen::find($id);
    if(Absen::null($absen)){
      return ['msg'=>'error : absen not found'];
    }
    return $absen;
  }

  public function updateAbsenByID(Request $req)
  {
    $absen = Absen::where('id',$req->id)->update([
      'jam_selesai' => Carbon::now()->toTimeString()
    ]);
    if($absen){
      Session::flash('absen_success_updated',true);
      return Redirect::back();
    }
  }

  public function getDelete($id)
  {
    $delete_absen = Absen::find($id)->delete();
    if($delete_absen){
      Session::flash('absen_success_deleted',true);
      return Redirect::back();
      // return ['msg'=>'success : absen deleted'];
    }
    Session::flash('absen_failed_deleted',true);
    return Redirect::back();
    // return ['msg'=>'error : can\'t delete absen'];
  }

  public function getDeleteAll()
  {
    $absens = Absen::all();
    foreach($absens as $absen){
      if(!$absen->delete()){
        break;
        Session::flash('absen_failed_deleteall',true);
        return Redirect::back();
        // return ['msg'=>'error : failed delete record'];
      }
      Session::flash('absen_deleteall',true);
      return Redirect::back();
    // }return ['msg'=>'success : success delete all record'];
    }
  }
  public function getDeleteAllByDate($date)
  {
    $deleteAbsen = Absen::whereDate('created_at','=',$date)->get();
    foreach($deleteAbsen as $absen){
      if(!$absen->delete()){
        break;
        Session::flash('absen_failed_deleteall',true);
        return Redirect::back();
        // return ['msg'=>'error : failed delete record'];
      }
      // }return ['msg'=>'success : success delete all record'];
    }
    Session::flash('absen_success_deleteall',true);
    return Redirect::back();
  }

  public function recordAbsensDownload($date)
  {
    $absensRecord = Absen::whereDate('created_at','=',$date)->get();
    // return $absensRecord;
    $file="record_absen_".$date.".xls";
    
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    echo "
      <table  >
        <thead>
          <tr>
            <th>ID</td>
            <th>Pegawai ID</td>
            <th>Nama</td>
            <th>Tercatat</td>
            <th>TerUpdate</td>
          </tr>
        </thead>
        <tbody>
        ";
      foreach($absensRecord as $data){
        echo "
          <tr>
            <td>$data->id</td>
            <td>$data->pegawai_id</td>
            <td>".$data->pegawai->nama."</td>
            <td>$data->created_at</td>
            <td>$data->updated_at</td>
          </tr>
        ";
        }
    echo "</tbody></table>";
  }

  public function recordAbsensDownloadAll()
  {
    $absensRecord = Absen::all();
    // return $absensRecord;
    $file="record_absen_".Carbon::now()->toDateString().".xls";
    
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$file");
    echo "
      <table  >
        <thead>
          <tr>
            <th>ID</td>
            <th>Pegawai ID</td>
            <th>Nama</td>
            <th>Tercatat</td>
            <th>TerUpdate</td>
          </tr>
        </thead>
        <tbody>
        ";
      foreach($absensRecord as $data){
        echo "
          <tr>
            <td>$data->id</td>
            <td>$data->pegawai_id</td>
            <td>".$data->pegawai->nama."</td>
            <td>$data->created_at</td>
            <td>$data->updated_at</td>
          </tr>
        ";
        }
    echo "</tbody></table>";
  }
}