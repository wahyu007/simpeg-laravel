<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Harian as Ansi;
use App\Alat;
use Carbon\Carbon;
use App\Pegawai;
use App\Jadwal;
use App\Mobil;
use Auth;
use Session,Redirect,Validator;

class LaporanHarianController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getLaporanHarian()
    {
        $newharians = Ansi::orderBy('id_harian','desc')->take(12)->paginate(6);
        $harians = Ansi::all();
        $i = 1;
        return view('admin.harian',compact('harians','newharians','alat','i'));
    }
    public function getLaporanHarianTambah()
    {
        $newharians = Ansi::orderBy('id_harian','desc')->take(12)->paginate(6);
        $harians = Ansi::all();
        $alat = Alat::all();
        $mobil = Mobil::all();
        $pegawai = Pegawai::all();
        $id_jadwal = Jadwal::where('id_pegawai', 23)->get();
        // echo $id_jadwal;
        if(!$id_jadwal){
            Session::flash('harian_jadwalnotfound',true);
            Redirect()->route('get-laporan-harian-index');
        }
        return view('admin.harian.tambah',compact('harians','newharians','alat','id_jadwal','mobil','pegawai'));
    }

    public function getLaporanHarianView()
    {
        $newharians = Ansi::orderBy('id_harian','desc')->take(12)->paginate(6);
        $harians = Ansi::all();
        $alat = Alat::all();
        return view('admin.harian.view',compact('harians','newharians','alat'));
    }

    public function postLaporanHarianAdd(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'id_jadwal' => 'integer|min:1',
            'id_mobil' => 'required',
        ]);
        
        if($validator->fails()){
            Session::flash('harian_notComplete',true);
            return Redirect::back();
            // return $validator->errors();
        }
        
        $newharian = Ansi::create([
            'id_jadwal' =>$req->id_jadwal,
            'id_mobil' => $req->id_mobil,
            'id_next_teknisi' => $req->next_teknisi,
            'id_alat' => $req->id_alat,
            'km_akhir' => $req->km_akhir,
            'info_taruna' => $req->info_taruna
            ]);

        if($newharian){
            Session::flash('harian_created',true);
            return redirect('laporan-harian');
            // return ['msg'=>'success add'];
            // return 'kambing';
        }
        Session::flash('harian_failed',true);
        return Redirect::back();
        // // return ['msg'=>'failed add'];
    }

    public function getEditLaporanHarian($id)
    {
        $harian = Ansi::where('id_harian',$id)->get();
        $newharians = Ansi::orderBy('id_harian','desc')->take(12)->paginate(6);
        // $harians = Ansi::all();
        $alat = Alat::all();
        $mobil = Mobil::all();
        $pegawai = Pegawai::all();
        $id_jadwal = Jadwal::where('id_pegawai', 23)->get();
        if($harian == null){
            Session::flash('harian_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        // Session::flash('harian_update',true);
        return view('admin.harian.edit',compact('harian','id_jadwal','mobil','pegawai','alat'));
    }
    public function getViewLaporanHarian($id)
    {
        $harian = Ansi::where('id',$id)->get();
        $supervisor = Pegawai::where('jabatan','supervisor')->get();
        $teknisi = Pegawai::where('id','1')->get();
        if($harian == null){
            Session::flash('harian_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        // Session::flash('harian_update',true);
        return view('admin.harian.view',compact('harian','supervisor','teknisi'));
    }

    public function postUpdateLaporanHarian(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'id_jadwal' => 'string|min:1',
            'id_mobil' => 'required',
        ]);
        
        if($validator->fails()){
            Session::flash('harian_notComplete',true);
            return Redirect::back();
            // return $validator->errors();
        }
        $total_km = $req->km_akhir - $req->km_awal;
        $upharian = Ansi::where('id_harian','=',$id)->update([
            'id_jadwal' =>$req->id_jadwal,
            'id_mobil' => $req->id_mobil,
            'id_next_teknisi' => $req->next_teknisi,
            'id_alat' => $req->id_alat,
            'km_akhir' => $req->km_akhir,
            'info_taruna' => $req->info_taruna
        ]);
        if($upharian){
            Session::flash('harian_success_updated',true);
            return redirect('laporan-harian');
            // return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('harian_failed_updated');
        return Redirect::back();
        // return 'kambing';
    }

    public function getDeleteLaporanHarianByID($id)
    {
        $harian = Ansi::where('id_harian','=',$id)->delete();if($harian == null){
            Session::flash('harian_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($harian){
            Session::flash('harian_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('harian_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
