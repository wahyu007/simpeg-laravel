<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\RekapAbsen as Ansi;
use Carbon\Carbon;

class RekapAbsenController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getRekapAbsen()
    {
        $newRekapAbsens = Ansi::orderBy('id_rekapabsen','desc')->take(12)->paginate(6);
        $RekapAbsens = Ansi::all();
        $i=1;
        return view('admin.rekapabsen',compact('RekapAbsens','newRekapAbsens','i'));
    }

    public function postRekapAbsenAdd(Request $req)
    {
        // $validator = Validator::make($req->all(),[
        //     'nama' => 'required|string|min:5|max:225'
        // ]);if($validator->fails()){
        //     Session::flash('rekapabsen_errval',true);
        //     return Redirect::back();
        //     // return $validator->errors();
        // }
        
        $newrekapabsen = Ansi::create([
            'nik' => $req->nik,
            'alasan' => $req->alasan,
            'jenis' => $req->jenis,
            'status_rekapabsen' => '1',
            'tanggal_mulai' => $req->tanggal_mulai,
            'tanggal_selesai' => $req->tanggal_selesai
        ]);
        if($newrekapabsen){
            Session::flash('rekapabsen_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('rekapabsen_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditRekapAbsen($id)
    {
        $rekapabsen = Ansi::where('id_rekapabsen',$id)->first();
        if($rekapabsen == null){
            Session::flash('rekapabsen_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('rekapabsen_update',true);
        return Redirect::back()->with('rekapabsen',$rekapabsen);
    }

    public function postUpdateRekapAbsen(Request $req, $id)
    {
        // $validator = Validator::make($req->all(),[
        //     'nama' => 'required'
        // ]);if($validator->fails()){
        //     Session::flash('rekapabsen_errval',true);
        //     return Redirect::back()->withErrors($validator->errors());
        // }

        $uprekapabsen = Ansi::where('id_rekapabsen',$id)->update([
            'nik' => $req->nik,
            'alasan' => $req->alasan,
            'jenis' => $req->jenis,
            'status_rekapabsen' => $req->status,
            'tanggal_mulai' => $req->tanggal_mulai,
            'tanggal_selesai' => $req->tanggal_selesai
        ]);
        if($uprekapabsen){
            Session::flash('rekapabsen_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('rekapabsen_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteRekapAbsenByID($id)
    {
        $rekapabsen = Ansi::find($id);if($rekapabsen == null){
            Session::flash('rekapabsen_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($rekapabsen->delete()){
            Session::flash('rekapabsen_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('rekapabsen_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
