<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\cuti as Ansi;
use Carbon\Carbon;

class CutiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getCuti()
    {
        $newCutis = Ansi::orderBy('id_cuti','desc')->take(12)->paginate(6);
        $Cutis = Ansi::all();
        $i=1;
        return view('admin.cuti',compact('Cutis','newCutis','i'));
    }

    public function postCutiAdd(Request $req)
    {
        // $validator = Validator::make($req->all(),[
        //     'nama' => 'required|string|min:5|max:225'
        // ]);if($validator->fails()){
        //     Session::flash('cuti_errval',true);
        //     return Redirect::back();
        //     // return $validator->errors();
        // }
        
        $newcuti = Ansi::create([
            'id_pegawai' => $req->nik,
            'alasan' => $req->alasan,
            'jenis' => $req->jenis,
            'status_cuti' => '1',
            'tanggal_mulai' => $req->tanggal_mulai,
            'tanggal_selesai' => $req->tanggal_selesai
        ]);
        if($newcuti){
            Session::flash('cuti_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('cuti_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditCuti($id)
    {
        $cuti = Ansi::where('id_cuti',$id)->first();
        if($cuti == null){
            Session::flash('cuti_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('cuti_update',true);
        return Redirect::back()->with('cuti',$cuti);
    }

    public function postUpdateCuti(Request $req, $id)
    {
        // $validator = Validator::make($req->all(),[
        //     'nama' => 'required'
        // ]);if($validator->fails()){
        //     Session::flash('cuti_errval',true);
        //     return Redirect::back()->withErrors($validator->errors());
        // }

        $upcuti = Ansi::where('id_cuti',$id)->update([
            'nik' => $req->nik,
            'alasan' => $req->alasan,
            'jenis' => $req->jenis,
            'status_cuti' => $req->status,
            'tanggal_mulai' => $req->tanggal_mulai,
            'tanggal_selesai' => $req->tanggal_selesai
        ]);
        if($upcuti){
            Session::flash('cuti_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('cuti_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteCutiByID($id)
    {
        $cuti = Ansi::find($id);if($cuti == null){
            Session::flash('cuti_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($cuti->delete()){
            Session::flash('cuti_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('cuti_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
