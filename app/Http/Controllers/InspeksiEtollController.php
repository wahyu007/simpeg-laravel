<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Inspeksi as Ansi;
use App\Jadwal;
use Carbon\Carbon;

class InspeksiEtollController extends Controller
{
    //
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getInspeksi()
    {
        $newinspeksis = Ansi::orderBy('id_inspeksi','desc')->take(12)->paginate(6);
        $inspeksis = Ansi::all();
        $id_jadwal = Jadwal::where('id_pegawai', 23)->get();
        return view('admin.inspeksi',compact('inspeksis','newinspeksis','id_jadwal'));
    }

    public function postInspeksiAdd(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'id_jadwal' => 'required'
        ]);
        if($validator->fails()){
            Session::flash('inspeksi_errval',true);
            return Redirect::back();
            // return $validator->errors();
        }
        
        $newinspeksi = Ansi::create([
            'id_jadwal' => $req->id_jadwal,
            'perangkat' => $req->perangkat,
            'status' => $req->status
            ]);
        if($newinspeksi){
            Session::flash('inspeksi_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('inspeksi_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditInspeksi($id)
    {
        $inspeksi = Ansi::where('id_inspeksi',$id)->first();
        if($inspeksi == null){
            Session::flash('inspeksi_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('inspeksi_update',true);
        return Redirect::back()->with('inspeksi',$inspeksi);
    }

    public function postUpdateInspeksi(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'id_jadwal' => 'required'
        ]);
        if($validator->fails()){
            Session::flash('inspeksi_errval',true);
            return Redirect::back()->withErrors($validator->errors());
        }

        $upinspeksi = Ansi::where('id_inspeksi',$id)->update([
            'id_jadwal' => $req->id_jadwal,
            'perangkat' => $req->perangkat,
            'status' => $req->status
        ]);
        if($upinspeksi){
            Session::flash('inspeksi_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('inspeksi_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteInspeksiByID($id)
    {
        $inspeksi = Ansi::where('id_inspeksi',$id);
        if($inspeksi == null){
            Session::flash('inspeksi_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($inspeksi->delete()){
            Session::flash('inspeksi_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('inspeksi_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
