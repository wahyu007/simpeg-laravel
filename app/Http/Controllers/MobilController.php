<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Mobil as Ansi;
use Carbon\Carbon;

class MobilController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getMobil()
    {
        $newmobils = Ansi::orderBy('id','desc')->take(12)->paginate(6);
        $mobils = Ansi::all();
        return view('admin.mobil',compact('mobils','newmobils'));
    }

    public function postMobilAdd(Request $req)
    {
        // $validator = Validator::make($req->all(),[
        //     'mobil' => 'required|string|max:225'
        // ]);if($validator->fails()){
        //     Session::flash('mobil_errval',true);
        //     return Redirect::bask();
        //     // return $validator->errors();
        // }
        
        $newmobil = Ansi::create([
            'nopol' => $req->nopol,
            'odometer' => $req->odometer,
            'merk' => $req->merk,
            'type' => $req->type,
            'tahun' => $req->tahun,
            'status_pajak' => $req->pajak,
            ]);
        if($newmobil){
            Session::flash('mobil_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('mobil_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditMobil($id)
    {
        $mobil = Ansi::find($id);if($mobil == null){
            Session::flash('mobil_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('mobil_update',true);
        return Redirect::back()->with('mobil',$mobil);
    }

    public function postUpdateMobil(Request $req, $id)
    {
        // $validator = Validator::make($req->all(),[
        //     'mobil' => 'required'
        // ]);
        // if($validator->fails()){
        //     Session::flash('mobil_errval',true);
        //     return Redirect::back()->withErrors($validator->errors());
        // }

        $upmobil = Ansi::find($id)->update([
            'nopol' => $req->nopol,
            'odometer' => $req->odometer,
            'merk' => $req->merk,
            'type' => $req->type,
            'tahun' => $req->tahun,
            'status_pajak' => $req->pajak,
        ]);
        if($upmobil){
            Session::flash('mobil_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('mobil_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteMobilByID($id)
    {
        $mobil = Ansi::find($id);if($mobil == null){
            Session::flash('mobil_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($mobil->delete()){
            Session::flash('mobil_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('mobil_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
