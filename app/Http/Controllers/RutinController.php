<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Rutin as Ansi;
use App\Jadwal;
use App\Component;
use Carbon\Carbon;

class RutinController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getRutin()
    {
        $newrutins = Ansi::orderBy('id_rutin','desc')->take(12)->paginate(6);
        $rutins = Ansi::all();
        $id_jadwal = Jadwal::where('id_pegawai', 23)->get();
        $component = Component::all();
        return view('admin.rutin',compact('rutins','newrutins','id_jadwal','component'));
    }

    public function postRutinAdd(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'id_jadwal' => 'required'
        ]);
        if($validator->fails()){
            Session::flash('rutin_errval',true);
            return Redirect::back();
            // return $validator->errors();
        }
        
        $newrutin = Ansi::create([
            'id_jadwal' => $req->id_jadwal,
            'id_component' => $req->id_component,
            'kelas' => $req->kelas,
            'kondisi'=>$req->kondisi
            ]);
        if($newrutin){
            Session::flash('rutin_created',true);
            return Redirect::back();
            // return ['msg'=>'success add'];
        }
        Session::flash('rutin_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditRutin($id)
    {
        $rutin = Ansi::where('id_rutin',$id)->first();
        $id_jadwal = Jadwal::where('id_pegawai', 23)->get();
        $component = Component::all();
        if($rutin == null){
            Session::flash('rutin_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('rutin_update',true);
        return Redirect::back()->with('rutin',$rutin,$component,'id_jadwal');
    }

    public function postUpdateRutin(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'id_jadwal' => 'required'
        ]);
        if($validator->fails()){
            Session::flash('rutin_errval',true);
            return Redirect::back()->withErrors($validator->errors());
        }

        $uprutin = Ansi::where('id_rutin',$id)->update([
            'id_jadwal' => $req->id_jadwal,
            'id_component' => $req->id_component,
            'kelas' => $req->kelas,
            'kondisi'=>$req->kondisi
        ]);
        if($uprutin){
            Session::flash('rutin_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('rutin_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteRutinByID($id)
    {
        $rutin = Ansi::where('id_rutin',$id);
        if($rutin == null){
            Session::flash('rutin_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($rutin->delete()){
            Session::flash('rutin_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('rutin_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}