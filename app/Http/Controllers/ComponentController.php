<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,Redirect,Validator;
use App\Component as Ansi;
use Carbon\Carbon;

class ComponentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getComponent()
    {
        $newcomponents = Ansi::orderBy('id_component','desc')->take(12)->paginate(6);
        $components = Ansi::all();
        $i = 1;
        return view('admin.component',compact('components','newcomponents','i'));
    }

    public function postComponentAdd(Request $req)
    {
        $validator = Validator::make($req->all(),[
            'alat' => 'required|string|max:23',
            'component' => 'required|string|max:23'
        ]);
        $cek_component = Ansi::where([
            'alat' => $req->alat,
            'component' => $req->component
        ])->first();
        if($validator->fails()){
            Session::flash('component_errval',true);
            return Redirect::bask();
            // return $validator->errors();
        }
        if($cek_component == null){
            $newcomponent = Ansi::create([
                'alat' => $req->alat,
                'component' => $req->component
            ]);
            if($newcomponent){
                Session::flash('component_created',true);
                return Redirect::back();
                // return ['msg'=>'success add'];
            } else{
                Session::flash('component_failed',true);
                return Redirect::back();
            }
            
        }
        Session::flash('component_failed',true);
        return Redirect::back();
        // return ['msg'=>'failed add'];
    }

    public function getEditComponent($id)
    {
        $component = Ansi::where('id_component',$id)->first();
        if($component == null){
            Session::flash('component_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }
        Session::flash('component_update',true);
        return Redirect::back()->with('component',$component);
    }

    public function postUpdateComponent(Request $req, $id)
    {
        $validator = Validator::make($req->all(),[
            'alat' => 'required'
        ]);if($validator->fails()){
            Session::flash('component_errval',true);
            return Redirect::back()->withErrors($validator->errors());
        }

        $upcomponent = Ansi::where('id_component',$id)->update([
            'alat' => $req->alat,
            'component' => $req->component,
        ]);if($upcomponent){
            Session::flash('component_success_updated',true);
            return Redirect::back();
            // return [
            //     'msg'=>'success update',
            //     'obj'=>Ansi::find($id)
            // ];
        }
        Session::flash('component_failed_updated');
        return Redirect::back();
        // return ['msg'=>'failed update'];
    }

    public function getDeleteComponentByID($id)
    {
        $component = Ansi::where('id_component','=',$id);
        if($component == null){
            Session::flash('component_notfound',true);
            return Redirect::back();
            // return ['obj'=>null];
        }if($component->delete()){
            Session::flash('component_success_deleted',true);
            return Redirect::back();
            // return ['msg'=>'deleted'];
        }
        Session::flash('component_failed_deleted',true);
        return Redirect::back();
        // return ['msg'=>'failed to delete'];
    }
}
