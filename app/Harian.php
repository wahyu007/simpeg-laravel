<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Harian extends Model
{
    //
    protected $table= 'harians';
    protected $fillable = ['id_harian', 'id_jadwal', 'id_mobil', 'id_alat', 'info_taruna', 'km_akhir', 'id_next_teknisi'];

    // public function pegawai()
    // {
    //     return $this->belongsTo('App\Pegawai','id_pegawai','id_pegawai');
    // }

    public function nextPegawai()
    {
        return $this->belongsTo('App\Pegawai','id_next_teknisi','id_pegawai');
    }

    public function mobil()
    {
        return $this->belongsTo('App\Mobil','id_mobil','id');
    }

    public function jadwal()
    {
        return $this->belongsTo('App\Jadwal','id_jadwal','jadwal_id');
    }

    public function alat()
    {
        return $this->belongsTo('App\Alat','id_alat','id_alat');
    }
}
