<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    //
    protected $table = 'tbljabatan';
    protected $fillable = [
        'jabatan'
    ];
}
