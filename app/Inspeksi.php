<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inspeksi extends Model
{
    //
    protected $fillable =['id_jadwal','perangkat','status'];

    public function jadwal()
    {
        return $this->belongsTo('App\Jadwal','id_jadwal','jadwal_id');
    }

    public function lokasi()
    {
        return $this->belongsTo('App\Lokasi','id_lokasi','id_lokasi');
    }
}
