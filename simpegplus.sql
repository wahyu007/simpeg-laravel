-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2020 at 12:26 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simpegplus`
--

-- --------------------------------------------------------

--
-- Table structure for table `absens`
--

CREATE TABLE `absens` (
  `id` int(10) UNSIGNED NOT NULL,
  `pegawai_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `absens`
--

INSERT INTO `absens` (`id`, `pegawai_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-09-20 17:00:00', '2019-09-20 17:00:00'),
(2, 2, '2019-09-20 17:00:00', '2019-09-20 17:00:00'),
(3, 3, '2019-09-20 17:00:00', '2019-09-20 17:00:00'),
(4, 4, '2019-09-20 17:00:00', '2019-09-20 17:00:00'),
(7, 6, '2019-09-21 18:24:08', '2019-09-21 18:24:08'),
(8, 1, '2019-09-29 01:35:00', '2019-09-29 01:35:00'),
(9, 2, '2019-09-29 01:35:13', '2019-09-29 01:35:13'),
(10, 1, '2019-10-29 16:45:14', '2019-10-29 16:45:14'),
(19, 18, '2019-11-17 06:01:14', '2019-11-17 06:01:14'),
(20, 18, '2019-11-17 06:02:10', '2019-11-17 06:02:10'),
(21, 18, '2019-11-17 06:02:45', '2019-11-17 06:02:45'),
(22, 18, '2019-11-17 06:02:56', '2019-11-17 06:02:56'),
(23, 18, '2019-11-17 06:03:19', '2019-11-17 06:03:19'),
(24, 18, '2019-11-17 06:04:59', '2019-11-17 06:04:59'),
(25, 18, '2019-11-17 06:06:52', '2019-11-17 06:06:52'),
(26, 18, '2019-11-17 06:08:10', '2019-11-17 06:08:10'),
(27, 18, '2019-11-17 06:08:56', '2019-11-17 06:08:56'),
(28, 18, '2019-11-17 06:09:18', '2019-11-17 06:09:18'),
(29, 18, '2019-11-17 06:21:22', '2019-11-17 06:21:22'),
(30, 18, '2019-11-17 06:23:17', '2019-11-17 06:23:17'),
(33, 10, '2019-11-17 06:58:19', '2019-11-17 06:58:19'),
(34, 10, '2019-11-17 06:59:13', '2019-11-17 06:59:13'),
(35, 10, '2019-11-17 07:00:46', '2019-11-17 07:00:46'),
(36, 1, '2019-11-17 07:01:52', '2019-11-17 07:01:52'),
(37, 10, '2019-11-17 07:06:20', '2019-11-17 07:06:20'),
(38, 10, '2019-11-17 07:11:10', '2019-11-17 07:11:10'),
(39, 18, '2019-11-17 07:12:33', '2019-11-17 07:12:33'),
(40, 10, '2019-11-17 07:12:39', '2019-11-17 07:12:39'),
(41, 10, '2019-11-17 07:16:07', '2019-11-17 07:16:07'),
(42, 10, '2019-11-17 07:21:08', '2019-11-17 07:21:08'),
(43, 10, '2019-11-17 07:23:27', '2019-11-17 07:23:27'),
(44, 10, '2019-11-17 07:24:25', '2019-11-17 07:24:25'),
(45, 18, '2019-11-17 09:06:00', '2019-11-17 09:06:00'),
(46, 10, '2019-11-17 09:06:33', '2019-11-17 09:06:33'),
(47, 10, '2019-11-17 09:08:21', '2019-11-17 09:08:21'),
(48, 10, '2019-11-17 09:09:54', '2019-11-17 09:09:54'),
(49, 10, '2019-11-17 09:10:20', '2019-11-17 09:10:20'),
(50, 18, '2019-11-17 09:10:39', '2019-11-17 09:10:39'),
(51, 10, '2019-11-17 09:13:32', '2019-11-17 09:13:32');

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

CREATE TABLE `components` (
  `id_component` int(10) UNSIGNED NOT NULL,
  `alat` char(23) COLLATE utf8mb4_unicode_ci NOT NULL,
  `component` char(23) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `components`
--

INSERT INTO `components` (`id_component`, `alat`, `component`, `created_at`, `updated_at`) VALUES
(1, 'Lane Printer', 'Dinamo 24v', '2020-03-01 03:37:06', '2020-03-01 03:47:11'),
(2, 'Lane Printer', 'Gear Nanas', '2020-03-01 03:37:21', '2020-03-01 03:48:30'),
(3, 'Lane Printer', 'RS 232', '2020-03-01 03:37:37', '2020-03-01 03:48:39'),
(4, 'Lane Printer', 'Power Supply 24v', '2020-03-01 03:38:02', '2020-03-01 03:48:49'),
(5, 'Lane Printer', 'Mainboard Epson', '2020-03-01 03:38:26', '2020-03-01 03:48:59'),
(6, 'Lane Printer', 'Fuse 2.3 A', '2020-03-01 03:38:51', '2020-03-01 03:49:07'),
(7, 'Automatic Lane Barrier', 'Portal', '2020-03-01 03:39:14', '2020-03-01 03:46:21'),
(8, 'Automatic Lane Barrier', 'Sarung Portal', '2020-03-01 03:39:29', '2020-03-01 03:46:13'),
(9, 'Automatic Lane Barrier', 'Bracket', '2020-03-01 03:39:52', '2020-03-01 03:46:05'),
(10, 'Automatic Lane Barrier', 'Power Supply 5v + 48v', '2020-03-01 03:40:33', '2020-03-01 03:45:34'),
(11, 'ALB', 'Power Supply 5v + 24v', '2020-03-01 03:40:53', '2020-03-01 03:40:53'),
(12, 'Customer Display Panel', 'IC maxim 232', '2020-03-01 03:50:02', '2020-03-01 03:50:02'),
(13, 'Customer Display Panel', 'Segment 7', '2020-03-01 03:50:37', '2020-03-01 03:50:37');

-- --------------------------------------------------------

--
-- Table structure for table `cutis`
--

CREATE TABLE `cutis` (
  `id` int(10) UNSIGNED NOT NULL,
  `pegawai_id` int(10) UNSIGNED NOT NULL,
  `alasan` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_cuti` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cutis`
--

INSERT INTO `cutis` (`id`, `pegawai_id`, `alasan`, `status_cuti`, `tanggal_mulai`, `tanggal_selesai`, `created_at`, `updated_at`) VALUES
(2, 2, 'maling', '2', '2019-01-02', '2019-01-03', '2019-09-28 22:29:58', '2019-09-29 00:19:51'),
(3, 1, 'tes', '1', '2019-01-01', '2019-01-05', '2019-09-28 22:51:07', '2019-09-28 22:51:07'),
(4, 2, 'sakit', '1', '2019-09-29', '2019-09-30', '2019-09-28 23:06:45', '2019-09-28 23:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `gajis`
--

CREATE TABLE `gajis` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `gaji_pokok` double NOT NULL,
  `tunjangan` double DEFAULT NULL,
  `potongan` double DEFAULT NULL,
  `total_absen` int(11) DEFAULT NULL,
  `pendapatan` double DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gajis`
--

INSERT INTO `gajis` (`id`, `id_pegawai`, `gaji_pokok`, `tunjangan`, `potongan`, `total_absen`, `pendapatan`, `tanggal`, `created_at`, `updated_at`) VALUES
(2, 18, 4200000, 0, 750000, 15, 3450000, '2019-11-17', '2019-11-17 05:43:27', '2019-11-17 09:10:39'),
(6, 10, 4200000, 0, 750000, 15, 3450000, '2019-11-17', '2019-11-17 09:09:54', '2019-11-17 09:13:32');

-- --------------------------------------------------------

--
-- Table structure for table `harians`
--

CREATE TABLE `harians` (
  `id_harian` int(10) UNSIGNED NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `inventaris` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_mobil` int(11) NOT NULL,
  `km_awal` int(11) NOT NULL,
  `km_akhir` int(11) NOT NULL,
  `total_km` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `harians`
--

INSERT INTO `harians` (`id_harian`, `id_jadwal`, `inventaris`, `no_mobil`, `km_awal`, `km_akhir`, `total_km`, `created_at`, `updated_at`) VALUES
(1, 121, 'kambing', 12, 223, 432, 209, '2019-12-29 08:17:36', '2019-12-29 10:18:15'),
(2, 121, 'kambing', 21, 12, 32, 20, '2019-12-29 09:23:31', '2019-12-29 09:23:31'),
(3, 121, 'kambing', 3, 21, 32, 11, '2019-12-29 09:24:48', '2019-12-29 09:24:48'),
(4, 121, 'kambing', 12, 32, 43, 11, '2019-12-29 09:26:47', '2019-12-29 09:26:47'),
(5, 12, 'sayuran', 1, 12, 32, 20, '2019-12-29 09:30:55', '2019-12-29 09:30:55'),
(8, 121, 'kambing', 2, 44, 55, 11, '2019-12-29 10:06:50', '2019-12-29 10:06:50'),
(9, 121, 'kambing', 12, 44, 365, 321, '2019-12-29 10:07:15', '2019-12-29 10:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `inspeksis`
--

CREATE TABLE `inspeksis` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `instansis`
--

CREATE TABLE `instansis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_instansi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `instansis`
--

INSERT INTO `instansis` (`id`, `nama_instansi`, `created_at`, `updated_at`) VALUES
(1, 'Pemeliharaan', '2019-09-20 17:00:00', '2019-09-21 09:49:23'),
(2, 'Keuangan', '2019-09-20 17:00:00', '2019-09-21 09:49:34'),
(3, 'Enginering', '2019-09-20 17:00:00', '2019-09-21 09:49:50'),
(4, 'Project', '2019-09-20 17:00:00', '2019-09-21 09:50:26'),
(5, 'Instalasi', '2019-09-20 17:00:00', '2019-09-21 09:50:45'),
(6, 'E-toll', '2019-09-20 17:00:00', '2019-09-21 09:51:07'),
(7, 'Bussiness Development', '2019-09-20 17:00:00', '2019-09-21 09:51:57'),
(8, 'Team Support', '2019-09-20 17:00:00', '2019-09-21 09:52:21'),
(9, 'Produksi', '2019-09-20 17:00:00', '2019-09-21 09:53:24'),
(10, 'Sarana Kerja', '2019-09-20 17:00:00', '2019-09-21 09:54:53');

-- --------------------------------------------------------

--
-- Table structure for table `jadwals`
--

CREATE TABLE `jadwals` (
  `jadwal_id` int(10) UNSIGNED NOT NULL,
  `nik` int(11) NOT NULL,
  `id_tugas` varchar(23) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jadwals`
--

INSERT INTO `jadwals` (`jadwal_id`, `nik`, `id_tugas`, `tanggal`, `created_at`, `updated_at`) VALUES
(3, 1, '1', '2020-03-29', '2020-02-29 19:45:06', '2020-02-29 19:45:06');

-- --------------------------------------------------------

--
-- Table structure for table `lokasis`
--

CREATE TABLE `lokasis` (
  `id_lokasi` int(10) UNSIGNED NOT NULL,
  `cabang` char(23) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gerbang` char(23) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gardu` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lokasis`
--

INSERT INTO `lokasis` (`id_lokasi`, `cabang`, `gerbang`, `gardu`, `created_at`, `updated_at`) VALUES
(1, 'Tangerang', 'Karang Tengah Barat 2', '01', '2020-03-01 01:23:19', '2020-03-01 01:23:19'),
(2, 'Tangerang', 'Karang Tengah Barat 2', '02', '2020-03-01 01:28:21', '2020-03-01 01:28:21'),
(3, 'Tangerang', 'Karang Tengah Barat 2', '03', '2020-03-01 01:28:34', '2020-03-01 01:28:34'),
(5, 'Tangerang', 'Tangerang2', '02', '2020-03-01 01:29:15', '2020-03-01 01:29:15'),
(6, 'Tangerang', 'Tangerang2', '03', '2020-03-01 01:29:27', '2020-03-01 01:29:27'),
(7, 'Tangerang', 'Tangerang2', '04', '2020-03-01 01:40:26', '2020-03-01 01:40:26'),
(8, 'Tangerang', 'Tangerang2', '05', '2020-03-01 01:45:43', '2020-03-01 01:45:43'),
(9, 'Tangerang', 'Tangerang2', '06', '2020-03-01 01:46:01', '2020-03-01 01:46:01'),
(10, 'Tangerang', 'Tangerang2', '07', '2020-03-01 01:46:11', '2020-03-01 01:46:11'),
(11, 'Tangerang', 'Tangerang2', '08', '2020-03-01 01:46:22', '2020-03-01 01:46:22'),
(12, 'Tangerang', 'Bitung 1', '01', '2020-03-01 01:46:39', '2020-03-01 01:49:43'),
(13, 'Tangerang', 'Bitung 1', '02', '2020-03-01 01:49:54', '2020-03-01 01:49:54'),
(14, 'Tangerang', 'Bitung 1', '03', '2020-03-01 01:50:04', '2020-03-01 01:50:04'),
(15, 'Tangerang', 'Bitung 1', '04', '2020-03-01 01:50:15', '2020-03-01 01:50:15'),
(16, 'Tangerang', 'Bitung 1', '05', '2020-03-01 01:50:26', '2020-03-01 01:50:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(100, '2014_10_12_000000_create_users_table', 1),
(101, '2014_10_12_100000_create_password_resets_table', 1),
(102, '2018_06_24_163004_create_pegawais_table', 1),
(103, '2018_06_25_000525_create_instansis_table', 1),
(104, '2018_06_26_162531_create_mutasis_table', 1),
(105, '2018_06_27_161708_create_absens_table', 1),
(106, '2018_06_27_162215_create_cutis_table', 1),
(107, '2018_06_27_163313_create_stugas_table', 1),
(108, '2018_06_29_131019_create_send_texts_table', 1),
(109, '2019_09_29_004254_create_jabatan_table', 2),
(110, '2019_10_02_163245_create_alat_table', 3),
(111, '2019_10_04_000944_create_mobil_table', 4),
(113, '2019_10_26_161959_create_perbaikans_table', 5),
(114, '2019_10_30_000006_create_gajis_table', 6),
(115, '2019_10_30_235944_create_harians_table', 6),
(116, '2019_10_31_000236_create_inspeksis_table', 6),
(117, '2019_10_31_000308_create_rutins_table', 6),
(118, '2020_01_01_081818_create_jadwals_table', 7),
(119, '2020_03_01_004821_create_tugas_table', 8),
(120, '2020_03_01_080016_create_lokasis_table', 9),
(121, '2020_03_01_102225_create_components_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `mutasis`
--

CREATE TABLE `mutasis` (
  `id` int(10) UNSIGNED NOT NULL,
  `pegawai_id` int(10) UNSIGNED NOT NULL,
  `status_mutasi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instansi_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mutasis`
--

INSERT INTO `mutasis` (`id`, `pegawai_id`, `status_mutasi`, `instansi_id`, `created_at`, `updated_at`) VALUES
(2, 1, '1', 1, '2019-09-21 09:57:22', '2019-09-21 09:57:22');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pegawais`
--

CREATE TABLE `pegawais` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_ktp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_masuk` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_pegawai` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pegawais`
--

INSERT INTO `pegawais` (`id`, `nama`, `no_ktp`, `jabatan`, `alamat`, `tgl_lahir`, `tgl_masuk`, `status_pegawai`, `created_at`, `updated_at`) VALUES
(1, 'Yayan Andriani Saputra', 'beteJNjUvTNC', 'Teknisi Pemeliharaan', 'Tegal', '21091998', '12082017', '2', '2019-09-20 17:00:00', '2019-09-26 16:51:03'),
(2, 'nakoKAh377', 'B3qg88G5vNzJ', '', '', '', '', 'cBL', '2019-09-20 17:00:00', '2019-09-20 17:00:00'),
(3, 'Gj7rd0UoFS', 'OPoVpIImBxpn', '', '', '', '', 'AJ6', '2019-09-20 17:00:00', '2019-09-20 17:00:00'),
(4, 'SXIuvnkDVO', 'Pb3RMO3E6zKP', '', '', '', '', 'TjY', '2019-09-20 17:00:00', '2019-09-20 17:00:00'),
(6, 'wahyu', '02918hshsdswecds', 'Teknisi Pemeliharaan', 'jakarta barat', '21091998', '12082017', '0', '2019-09-21 09:44:27', '2019-09-26 16:49:30'),
(8, 'wahyu', '02918hshsds', 'Teknisi Pemeliharaan', 'tes', '26.09.2019', '12082017', '2', '2019-09-25 17:19:06', '2019-09-25 17:19:06'),
(9, 'khoirudin', '2192012832', 'Teknisi Pemeliharaan', 'bekasi', '21091998', '12082017', '2', '2019-09-25 17:22:23', '2019-09-25 17:22:23'),
(10, 'Wahidin Jamal', '21938710001', 'Teknisi Pemeliharaan', 'Tangerang', '21091998', '12082017', '2', '2019-09-28 18:26:43', '2019-09-28 18:26:43'),
(18, 'imron', '1243123213121', 'Teknisi Sipil', 'brebes', '2019-01-01', '12082017', '2', '2019-11-17 05:43:27', '2019-11-17 05:43:27');

-- --------------------------------------------------------

--
-- Table structure for table `perbaikans`
--

CREATE TABLE `perbaikans` (
  `id` int(10) UNSIGNED NOT NULL,
  `teknisi_id` int(10) NOT NULL,
  `gerbang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cabang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gardu` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `w_datang` time NOT NULL,
  `w_gardu_mati` time NOT NULL,
  `w_gardu_hidup` time NOT NULL,
  `alat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `komponen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kerusakan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uraian_kerusakan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `uraian_perbaikan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perbaikans`
--

INSERT INTO `perbaikans` (`id`, `teknisi_id`, `gerbang`, `cabang`, `gardu`, `w_datang`, `w_gardu_mati`, `w_gardu_hidup`, `alat`, `komponen`, `kerusakan`, `uraian_kerusakan`, `uraian_perbaikan`, `created_at`, `updated_at`) VALUES
(1, 0, 'karawaci 2', 'jakarta - tangerang', '01', '00:30:00', '00:00:00', '16:00:00', 'LPR', 'gear nanas', 'patah', 'gir nanas patah karena kertas sering nyangkut', 'solder ulang gir', '2019-10-26 12:42:53', '2019-10-27 01:33:59'),
(2, 0, 'aaa', 'aaa', 'aaa', '00:00:00', '00:30:00', '01:00:00', 'LPR', 'gear nanas', 'patah', 'aaa', 'aaa', '2019-10-26 19:41:00', '2019-10-26 19:41:00'),
(3, 0, 'karawaci 4', 'jakarta - tangerang', '01', '01:00:00', '00:00:00', '02:00:00', 'LPR', 'gear nanas', 'patah', 'gir nanas patah karena kertas sering nyangkut', 'solder ulang gir', '2019-10-26 20:57:01', '2019-10-26 21:13:17'),
(4, 0, 'karawaci 2', 'jakarta - tangerang', '01', '00:30:00', '00:00:00', '01:00:00', 'LPR', 'gear nanas', 'patah', 'gir nanas patah karena kertas sering nyangkut', 'solder ulang gir', '2019-10-26 20:59:17', '2019-10-26 20:59:17'),
(5, 0, 'karawaci 2', 'jakarta - tangerang', '01', '00:30:00', '00:00:00', '01:00:00', 'LPR', 'gear nanas', 'patah', 'gir nanas patah karena kertas sering nyangkut', 'solder ulang gir', '2019-10-26 21:01:16', '2019-10-26 21:01:16'),
(6, 0, 'karawaci 2', 'jakarta - tangerang', '01', '00:30:00', '00:00:00', '01:00:00', 'LPR', 'gear nanas', 'patah', 'gir nanas patah karena kertas sering nyangkut', 'solder ulang gir', '2019-10-26 21:09:33', '2019-10-26 21:09:33'),
(7, 6, 'tangerang 1', 'jakarta - tangerang', '07', '01:00:00', '00:00:00', '02:00:00', 'LPR', 'gear nanas', 'patah', 'xcdsacsa', 'ascasca', '2019-12-29 09:44:02', '2019-12-29 09:44:02');

-- --------------------------------------------------------

--
-- Table structure for table `rutins`
--

CREATE TABLE `rutins` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `send_texts`
--

CREATE TABLE `send_texts` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stugas`
--

CREATE TABLE `stugas` (
  `id` int(10) UNSIGNED NOT NULL,
  `pegawai_id` int(10) UNSIGNED NOT NULL,
  `tempat_bertugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_bertugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblalat`
--

CREATE TABLE `tblalat` (
  `id_alat` int(10) UNSIGNED NOT NULL,
  `nama_alat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblalat`
--

INSERT INTO `tblalat` (`id_alat`, `nama_alat`, `jumlah`, `created_at`, `updated_at`) VALUES
(3, 'Bor Listrik', 2, '2020-03-01 03:56:53', '2020-03-01 03:56:53'),
(4, 'Soldier 48w', 4, '2020-03-01 03:59:31', '2020-03-01 03:59:31'),
(5, 'Bor Duduk', 1, '2020-03-01 03:59:44', '2020-03-01 03:59:44'),
(6, 'Grinda', 2, '2020-03-01 04:00:03', '2020-03-01 04:00:03'),
(7, 'Keyboard', 2, '2020-03-01 04:00:15', '2020-03-01 04:00:15'),
(8, 'Toolset Kunci Pas', 2, '2020-03-01 04:00:55', '2020-03-01 04:00:55'),
(9, 'Multitester', 3, '2020-03-01 04:01:11', '2020-03-01 04:01:11'),
(10, 'Kunci L set', 2, '2020-03-01 04:01:57', '2020-03-01 04:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbljabatan`
--

CREATE TABLE `tbljabatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbljabatan`
--

INSERT INTO `tbljabatan` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'Teknisi Sipil', '2019-09-28 18:06:57', '2019-09-28 18:07:22'),
(2, 'Teknisi Pemeliharaan', '2019-09-28 18:15:45', '2019-09-28 18:15:45'),
(3, 'Produksi', '2019-09-28 18:16:15', '2019-09-28 18:16:15'),
(4, 'Asisten Kepala Wilayah', '2019-09-28 18:16:55', '2019-09-28 18:16:55'),
(5, 'Kepala Wilayah', '2019-09-28 18:17:08', '2019-09-28 18:17:08'),
(6, 'staff', '2019-09-28 18:18:27', '2019-09-28 18:18:27'),
(7, 'Manager Etoll', '2019-09-28 18:18:41', '2019-09-28 18:18:41'),
(8, 'Manager Pemeliharaan', '2019-09-28 18:18:59', '2019-09-28 18:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `tblmobil`
--

CREATE TABLE `tblmobil` (
  `id` int(10) UNSIGNED NOT NULL,
  `nopol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `odometer` int(11) NOT NULL,
  `status_pajak` datetime NOT NULL,
  `merk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblmobil`
--

INSERT INTO `tblmobil` (`id`, `nopol`, `odometer`, `status_pajak`, `merk`, `type`, `tahun`, `created_at`, `updated_at`) VALUES
(3, 'B 3233 TKB', 44, '2020-12-26 00:00:00', 'Panther', 'Turbo', 2014, '2019-10-05 18:02:44', '2019-10-05 18:02:44'),
(4, 'B 2301 TKP', 61245, '2019-01-01 00:00:00', 'Panther', 'Turbo', 2015, '2020-03-01 02:01:58', '2020-03-01 02:01:58'),
(5, 'B 9104 TAK', 21901, '2019-01-01 00:00:00', 'Panther', 'Pick Up', 2014, '2020-03-01 02:03:03', '2020-03-01 02:03:03'),
(6, 'B 201 TFG', 21099, '2019-01-01 00:00:00', 'Panther', 'Turbo', 2017, '2020-03-01 02:03:38', '2020-03-01 02:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE `tugas` (
  `id_tugas` int(10) UNSIGNED NOT NULL,
  `tugas` char(23) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tugas`
--

INSERT INTO `tugas` (`id_tugas`, `tugas`, `jam_masuk`, `jam_selesai`, `created_at`, `updated_at`) VALUES
(1, 'BARAT 1', '09:00:00', '21:00:00', '2020-02-29 18:42:00', '2020-02-29 18:42:00'),
(2, 'TIMUR 1', '09:00:00', '21:00:00', '2020-02-29 21:48:35', '2020-02-29 21:48:35'),
(3, 'BARAT 2', '21:00:00', '09:00:00', '2020-03-01 01:51:16', '2020-03-01 01:58:43'),
(5, 'TIMUR 2', '09:00:00', '21:00:00', '2020-03-01 02:00:06', '2020-03-01 02:00:06'),
(6, 'RUTIN', '21:00:00', '09:00:00', '2020-03-01 02:00:27', '2020-03-01 02:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(2) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `pegawai_id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'admin', 'admin@laravel.com', '$2y$10$eHHG/uZRS.3yed0T3mmwmuZbKReSAFX6aSg0NZ5GT/LYcm7AqE.L2', 1, 'm8ptPmh7Fe5eU5Wzu9VbD8Yp7swuFRff8NhTNqfaz83p41L0A7sbguk3rD4R', '2019-09-21 09:31:26', '2019-11-03 01:08:03'),
(2, 6, 'wahyuam', 'wahyuam007@gmail.com', '$2y$10$eHHG/uZRS.3yed0T3mmwmuZbKReSAFX6aSg0NZ5GT/LYcm7AqE.L2', 2, 'MasxnOXIv1fV5PxhiIOPujEFnCbjBDugthpsOMsPPJuQ4ej5JAIhARVb07HG', '2019-11-02 23:29:55', '2019-11-02 23:29:55'),
(4, 1, 'yayan', 'admin@my.tuta.com', '$2y$10$ToT0giIuvlj./i2s9qF4quxowjADVEvfOKNzYaFgTP9LJut.j6Aaq', 2, NULL, '2019-11-02 23:46:38', '2019-11-02 23:46:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absens`
--
ALTER TABLE `absens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `absens_pegawai_id_foreign` (`pegawai_id`);

--
-- Indexes for table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id_component`);

--
-- Indexes for table `cutis`
--
ALTER TABLE `cutis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cutis_pegawai_id_foreign` (`pegawai_id`);

--
-- Indexes for table `gajis`
--
ALTER TABLE `gajis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harians`
--
ALTER TABLE `harians`
  ADD PRIMARY KEY (`id_harian`);

--
-- Indexes for table `inspeksis`
--
ALTER TABLE `inspeksis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instansis`
--
ALTER TABLE `instansis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwals`
--
ALTER TABLE `jadwals`
  ADD PRIMARY KEY (`jadwal_id`);

--
-- Indexes for table `lokasis`
--
ALTER TABLE `lokasis`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mutasis`
--
ALTER TABLE `mutasis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mutasis_pegawai_id_foreign` (`pegawai_id`),
  ADD KEY `mutasis_instansi_id_foreign` (`instansi_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pegawais`
--
ALTER TABLE `pegawais`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pegawais_no_ktp_unique` (`no_ktp`);

--
-- Indexes for table `perbaikans`
--
ALTER TABLE `perbaikans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rutins`
--
ALTER TABLE `rutins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `send_texts`
--
ALTER TABLE `send_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stugas`
--
ALTER TABLE `stugas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stugas_pegawai_id_foreign` (`pegawai_id`);

--
-- Indexes for table `tblalat`
--
ALTER TABLE `tblalat`
  ADD PRIMARY KEY (`id_alat`);

--
-- Indexes for table `tbljabatan`
--
ALTER TABLE `tbljabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblmobil`
--
ALTER TABLE `tblmobil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tugas`
--
ALTER TABLE `tugas`
  ADD PRIMARY KEY (`id_tugas`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `pegawai_id` (`pegawai_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absens`
--
ALTER TABLE `absens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `components`
--
ALTER TABLE `components`
  MODIFY `id_component` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cutis`
--
ALTER TABLE `cutis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `gajis`
--
ALTER TABLE `gajis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `harians`
--
ALTER TABLE `harians`
  MODIFY `id_harian` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `inspeksis`
--
ALTER TABLE `inspeksis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `instansis`
--
ALTER TABLE `instansis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `jadwals`
--
ALTER TABLE `jadwals`
  MODIFY `jadwal_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lokasis`
--
ALTER TABLE `lokasis`
  MODIFY `id_lokasi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `mutasis`
--
ALTER TABLE `mutasis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pegawais`
--
ALTER TABLE `pegawais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `perbaikans`
--
ALTER TABLE `perbaikans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rutins`
--
ALTER TABLE `rutins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `send_texts`
--
ALTER TABLE `send_texts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stugas`
--
ALTER TABLE `stugas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblalat`
--
ALTER TABLE `tblalat`
  MODIFY `id_alat` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbljabatan`
--
ALTER TABLE `tbljabatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblmobil`
--
ALTER TABLE `tblmobil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tugas`
--
ALTER TABLE `tugas`
  MODIFY `id_tugas` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `absens`
--
ALTER TABLE `absens`
  ADD CONSTRAINT `absens_pegawai_id_foreign` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawais` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cutis`
--
ALTER TABLE `cutis`
  ADD CONSTRAINT `cutis_pegawai_id_foreign` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawais` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mutasis`
--
ALTER TABLE `mutasis`
  ADD CONSTRAINT `mutasis_instansi_id_foreign` FOREIGN KEY (`instansi_id`) REFERENCES `instansis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mutasis_pegawai_id_foreign` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawais` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stugas`
--
ALTER TABLE `stugas`
  ADD CONSTRAINT `stugas_pegawai_id_foreign` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawais` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
