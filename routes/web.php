<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/csrf', function () {
  return csrf_token();
});
  
// Route::get('admin','PegawaiController@getPegawaiAllIndex')->name('get-absen-index');


// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::group(['prefix' => '/',['middleware'=>'auth']], function () {
  Route::get('/', 'HomeController@dashboard')->name('dashboard');
  Route::get('/absens', 'HomeController@absen')->name('absens');
  
  Route::group(['prefix'=>'absen'],function(){
    // Route::get('/','PegawaiController@getPegawaiAllIndex')->name('get-pegawai-index');
    Route::get('/get','AbsenController@getAbsenAll')->name('get-absen');
    Route::get('/get/{id}','AbsenController@getAbsenByID')->name('get-absen-id');
    Route::post('/update/{id}','AbsenController@updateAbsenByID')->name('get-absen-update-id');
    Route::get('/get/delete/{id}','AbsenController@getDelete')->name('get-absen-delete-id');
    Route::get('/get/deleteall','AbsenController@getDeleteAll')->name('get-absen-delete-all');
    Route::get('/get/deleteall/{date}','AbsenController@getDeleteAllByDate')->name('get-absen_deleteall-by-date');
    Route::post('/post','AbsenController@postAbsen')->name('post-absen');
    Route::get('/records','HomeController@recordAbsens')->name('get-absen-records');
    Route::get('/records/download/{date}','AbsenController@recordAbsensDownload')->name('get-absen-records-download');
    Route::get('/records/download','AbsenController@recordAbsensDownloadAll')->name('get-absen-records-download-all');
  });

  Route::group(['prefix'=>'pegawais'],function(){
    Route::get('/','PegawaiController@getPegawaiAll')->name('get-pegawai-index');
    Route::post('/post','PegawaiController@postPegawai')->name('post-pegawai');
    Route::get('/get/{id}','PegawaiController@getPegawaiByID')->name('get-pegawai-id');
    Route::get('/get/delete/{id}','PegawaiController@getDeleteByID')->name('get-pegawai-delete-id');
    Route::get('/get/deleteall','PegawaiController@getDeleteAll')->name('get-pegawai-delete-all');
    Route::get('/get/update/{id}','PegawaiController@getUpdate')->name('get-pegawai-update-id');
    Route::post('/post/update/{id}','PegawaiController@postUpdate')->name('post-pegawai-update-id');

  });

  Route::group(['prefix'=>'user'],function(){
    Route::get('/','UserController@getUser')->name('get-user-index');
    Route::get('/profile','UserController@getProfile')->name('get-profile-index');
    Route::post('/post','UserController@postUser')->name('post-user');
    Route::get('/get/{id}','UserController@getUserByID')->name('get-user');
    Route::get('/get/delete/{id}','UserController@getDeleteByID')->name('get-user-delete-id');
    Route::get('/get/deleteall','UserController@getDeleteAll')->name('get-user-delete-all');
    Route::get('/get/update/{id}','UserController@getUpdate')->name('get-user-update-id');
    Route::post('/post/update/{id}','UserController@postUpdate')->name('post-user-update-id');

  });

  Route::group(['prefix'=>'mutasi'],function(){
    Route::get('/','MutasiController@getMutasi')->name('get-mutasi-index');
    Route::get('/get','MutasiController@getMutasiAll')->name('get-mutasi');
    Route::post('/post','MutasiController@postMutasi')->name('post-mutasi');
  });

  Route::group(['prefix' => 'cuti'], function () {
    Route::get('/','CutiController@getCuti')->name('get-cuti-index');
    Route::post('/','CutiController@postCutiAdd')->name('post-cuti-add');
    Route::get('/get/update/{id}','CutiController@getEditCuti')->name('get-cuti-update');
    Route::post('/get/update/{id}','CutiController@postUpdateCuti')->name('get-cuti-updated');
    Route::get('/get/delete/{id}','CutiController@getDeleteCutiByID')->name('get-cuti-delete');
    Route::get('/get/download','CutiController@recordDownloadCuti')->name('get-cuti-downloadrecord');
  });

  Route::group(['prefix' => 'instansi'], function () {
    Route::get('/','InstansiController@getInstansi')->name('get-instansi-index');
    Route::post('/','InstansiController@postInstansiAdd')->name('post-instansi-add');
    Route::get('/get/update/{id}','InstansiController@getEditInstansi')->name('get-instansi-update');
    Route::post('/get/update/{id}','InstansiController@postUpdateInstansi')->name('get-instansi-updated');
    Route::get('/get/delete/{id}','InstansiController@getDeleteInstansiByID')->name('get-instansi-delete');
    Route::get('/get/download','InstansiController@recordDownloadInstansi')->name('get-instansi-downloadrecord');
  });
  Route::group(['prefix' => 'jabatan'], function () {
    Route::get('/','JabatanController@getJabatan')->name('get-jabatan-index');
    Route::post('/','JabatanController@postJabatanAdd')->name('post-jabatan-add');
    Route::get('/get/update/{id}','JabatanController@getEditJabatan')->name('get-jabatan-update');
    Route::post('/get/update/{id}','JabatanController@postUpdateJabatan')->name('get-jabatan-updated');
    Route::get('/get/delete/{id}','JabatanController@getDeleteJabatanByID')->name('get-jabatan-delete');
    Route::get('/get/download','JabatanController@recordDownloadJabatan')->name('get-jabatan-downloadrecord');
  });

  Route::group(['prefix' => 'laporan-harian'], function () {
    Route::get('/','LaporanHarianController@getLaporanHarian')->name('get-laporan-harian-index');
    Route::post('/','LaporanHarianController@postLaporanHarianAdd')->name('post-laporan-harian-add');
    Route::get('/tambah','LaporanHarianController@getLaporanHarianTambah')->name('get-tambah-laporan-harian-index');
    Route::get('/view','LaporanHarianController@getLaporanHarianView')->name('get-view-laporan-harian-index');
    Route::get('/edit','LaporanHarianController@getLaporanHarianTambah')->name('get-edit-laporan-harian-index');
    Route::get('/get/update/{id}','LaporanHarianController@getEditLaporanHarian')->name('get-laporan-harian-update');
    Route::get('/get/view/{id}','HarianController@getViewLaporanHarian')->name('get-laporan-harian-view');
    Route::post('/get/update/{id}','LaporanHarianController@postUpdateLaporanHarian')->name('get-laporan-harian-updated');
    Route::get('/get/delete/{id}','LaporanHarianController@getDeleteLaporanHarianByID')->name('get-laporan-harian-delete');
    Route::get('/get/download','LaporanHarianController@recordDownloadLaporanHarian')->name('get-laporan-harian-downloadrecord');
  });

  Route::group(['prefix' => 'laporan-perbaikan'], function () {
    Route::get('/','LaporanperbaikanController@getLaporanperbaikan')->name('get-laporan-perbaikan-index');
    Route::post('/','LaporanperbaikanController@postLaporanperbaikanAdd')->name('post-laporan-perbaikan-add');
    Route::get('/get/update/{id}','LaporanperbaikanController@getEditLaporanperbaikan')->name('get-laporan-perbaikan-update');
    Route::post('/get/update/{id}','LaporanperbaikanController@postUpdateLaporanperbaikan')->name('get-laporan-perbaikan-updated');
    Route::get('/get/delete/{id}','LaporanperbaikanController@getDeleteLaporanperbaikanByID')->name('get-laporan-perbaikan-delete');
    Route::get('/get/download','LaporanperbaikanController@recordDownloadLaporanperbaikan')->name('get-laporan-perbaikan-downloadrecord');
  });

  Route::group(['prefix' => 'alat'], function () {
    Route::get('/','AlatController@getAlat')->name('get-alat-index');
    Route::post('/','AlatController@postAlatAdd')->name('post-alat-add');
    Route::get('/get/update/{id}','AlatController@getEditAlat')->name('get-alat-update');
    Route::post('/get/update/{id}','AlatController@postUpdateAlat')->name('get-alat-updated');
    Route::get('/get/delete/{id}','AlatController@getDeleteAlatByID')->name('get-alat-delete');
    Route::get('/get/download','AlatController@recordDownloadAlat')->name('get-alat-downloadrecord');
  });

  Route::group(['prefix' => 'tugas'], function () {
    Route::get('/','TugasController@getTugas')->name('get-tugas-index');
    Route::post('/','TugasController@postTugasAdd')->name('post-tugas-add');
    Route::get('/get/update/{id}','TugasController@getEditTugas')->name('get-tugas-update');
    Route::post('/get/update/{id}','TugasController@postUpdateTugas')->name('get-tugas-updated');
    Route::get('/get/delete/{id}','TugasController@getDeleteTugasByID')->name('get-tugas-delete');
    Route::get('/get/download','TugasController@recordDownloadTugas')->name('get-tugas-downloadrecord');
  });

  Route::group(['prefix' => 'mobil'], function () {
    Route::get('/','MobilController@getMobil')->name('get-mobil-index');
    Route::post('/','MobilController@postMobilAdd')->name('post-mobil-add');
    Route::get('/get/update/{id}','MobilController@getEditMobil')->name('get-mobil-update');
    Route::post('/get/update/{id}','MobilController@postUpdateMobil')->name('get-mobil-updated');
    Route::get('/get/delete/{id}','MobilController@getDeleteMobilByID')->name('get-mobil-delete');
    Route::get('/get/download','MobilController@recordDownloadMobil')->name('get-mobil-downloadrecord');
  });

  Route::group(['prefix' => 'gaji'], function () {
    Route::get('/','GajiController@getGaji')->name('get-gaji-index');
    Route::get('/tambah','GajiController@getGajiTambah')->name('get-tambah-gaji-index');
    Route::get('/view','GajiController@getGajiView')->name('get-view-gaji-index');
    Route::get('/edit','GajiController@getGajiTambah')->name('get-edit-gaji-index');
    Route::post('/','GajiController@postGajiAdd')->name('post-gaji-add');
    Route::get('/get/update/{id}','GajiController@getEditGaji')->name('get-gaji-update');
    Route::get('/get/view/{id}','GajiController@getViewGaji')->name('get-gaji-view');
    Route::post('/get/update/{id}','GajiController@postUpdateGaji')->name('get-gaji-updated');
    Route::get('/get/delete/{id}','GajiController@getDeleteGajiByID')->name('get-gaji-delete');
    Route::get('/get/download','GajiController@recordDownloadGaji')->name('get-gaji-downloadrecord');
  });
  Route::group(['prefix' => 'perbaikan'], function () {
    Route::get('/','PerbaikanController@getPerbaikan')->name('get-perbaikan-index');
    Route::get('/tambah','PerbaikanController@getPerbaikanTambah')->name('get-tambah-perbaikan-index');
    Route::get('/view','PerbaikanController@getPerbaikanView')->name('get-view-perbaikan-index');
    Route::get('/edit','PerbaikanController@getPerbaikanTambah')->name('get-edit-perbaikan-index');
    Route::post('/','PerbaikanController@postPerbaikanAdd')->name('post-perbaikan-add');
    Route::get('/get/update/{id}','PerbaikanController@getEditPerbaikan')->name('get-perbaikan-update');
    Route::get('/get/view/{id}','PerbaikanController@getViewPerbaikan')->name('get-perbaikan-view');
    Route::post('/get/update/{id}','PerbaikanController@postUpdatePerbaikan')->name('get-perbaikan-updated');
    Route::get('/get/delete/{id}','PerbaikanController@getDeletePerbaikanByID')->name('get-perbaikan-delete');
    Route::get('/get/download','PerbaikanController@recordDownloadPerbaikan')->name('get-perbaikan-downloadrecord');
  });

  Route::group(['prefix' => 'jadwal'], function () {
    Route::get('/','JadwalController@getJadwal')->name('get-jadwal-index');
    Route::get('/get/jadwal/tugas/{id}','JadwalController@getJadwalTugas')->name('get-jadwal-tugas');
    Route::get('/tambah','JadwalController@getJadwalTambah')->name('get-tambah-jadwal-index');
    Route::get('/view','JadwalController@getJadwalView')->name('get-view-jadwal-index');
    Route::get('/edit','JadwalController@getJadwalTambah')->name('get-edit-jadwal-index');
    Route::post('/','JadwalController@postJadwalAdd')->name('post-jadwal-add');
    Route::get('/get/update/{id}','JadwalController@getEditJadwal')->name('get-jadwal-update');
    Route::get('/get/view/{id}','JadwalController@getViewJadwal')->name('get-jadwal-view');
    Route::post('/get/update/{id}','JadwalController@postUpdateJadwal')->name('get-jadwal-updated');
    Route::get('/get/delete/{id}','JadwalController@getDeleteJadwalByID')->name('get-jadwal-delete');
    Route::get('/get/download','JadwalController@recordDownloadJadwal')->name('get-jadwal-downloadrecord');
  });

  Route::group(['prefix' => 'lokasi'], function () {
    Route::get('/','LokasiController@getLokasi')->name('get-lokasi-index');
    Route::post('/','LokasiController@postLokasiAdd')->name('post-lokasi-add');
    Route::get('/get/update/{id}','LokasiController@getEditLokasi')->name('get-lokasi-update');
    Route::post('/get/update/{id}','LokasiController@postUpdateLokasi')->name('get-lokasi-updated');
    Route::get('/get/delete/{id}','LokasiController@getDeleteLokasiByID')->name('get-lokasi-delete');
    Route::get('/get/download','LokasiController@recordDownloadLokasi')->name('get-lokasi-downloadrecord');
  });

  Route::group(['prefix' => 'component'], function () {
    Route::get('/','ComponentController@getComponent')->name('get-component-index');
    Route::post('/','ComponentController@postComponentAdd')->name('post-component-add');
    Route::get('/get/update/{id}','ComponentController@getEditComponent')->name('get-component-update');
    Route::post('/get/update/{id}','ComponentController@postUpdateComponent')->name('get-component-updated');
    Route::get('/get/delete/{id}','ComponentController@getDeleteComponentByID')->name('get-component-delete');
    Route::get('/get/download','ComponentController@recordDownloadComponent')->name('get-component-downloadrecord');
  });
  Route::group(['prefix' => 'rutin'], function () {
    Route::get('/','RutinController@getRutin')->name('get-rutin-index');
    Route::post('/','RutinController@postRutinAdd')->name('post-rutin-add');
    Route::get('/get/update/{id}','RutinController@getEditRutin')->name('get-rutin-update');
    Route::post('/get/update/{id}','RutinController@postUpdateRutin')->name('get-rutin-updated');
    Route::get('/get/delete/{id}','RutinController@getDeleteRutinByID')->name('get-rutin-delete');
    Route::get('/get/download','RutinController@recordDownloadRutin')->name('get-rutin-downloadrecord');
  });

  Route::group(['prefix' => 'inspeksi'], function () {
    Route::get('/','InspeksiEtollController@getInspeksi')->name('get-inspeksi-index');
    Route::post('/','InspeksiEtollController@postInspeksiAdd')->name('post-inspeksi-add');
    Route::get('/get/update/{id}','InspeksiEtollController@getEditInspeksi')->name('get-inspeksi-update');
    Route::post('/get/update/{id}','InspeksiEtollController@postUpdateInspeksi')->name('get-inspeksi-updated');
    Route::get('/get/delete/{id}','InspeksiEtollController@getDeleteInspeksiByID')->name('get-inspeksi-delete');
    Route::get('/get/download','InspeksiEtollController@recordDownloadInspeksi')->name('get-inspeksi-downloadrecord');
  });

  Route::group(['prefix' => 'rekapabsen'], function () {
    Route::get('/','RekapAbsenController@getRekapAbsen')->name('get-rekapabsen-index');
    Route::post('/','RekapAbsenController@postRekapAbsenAdd')->name('post-rekapabsen-add');
    Route::get('/get/update/{id}','RekapAbsenController@getEditRekapAbsen')->name('get-rekapabsen-update');
    Route::post('/get/update/{id}','RekapAbsenController@postUpdateRekapAbsen')->name('get-rekapabsen-updated');
    Route::get('/get/delete/{id}','RekapAbsenController@getDeleteRekapAbsenByID')->name('get-rekapabsen-delete');
    Route::get('/get/download','RekapAbsenController@recordDownloadRekapAbsen')->name('get-rekapabsen-downloadrecord');
  });

  Route::group(['prefix' => 'pengambilan'], function () {
    Route::get('/','PengambilanController@getPengambilan')->name('get-pengambilan-index');
    Route::post('/','PengambilanController@postPengambilanAdd')->name('post-pengambilan-add');
    Route::get('/get/update/{id}','PengambilanController@getEditPengambilan')->name('get-pengambilan-update');
    Route::post('/get/update/{id}','PengambilanController@postUpdatePengambilan')->name('get-pengambilan-updated');
    Route::get('/get/delete/{id}','PengambilanController@getDeletePengambilanByID')->name('get-pengambilan-delete');
    Route::get('/get/download','PengambilanController@recordDownloadPengambilan')->name('get-pengambilan-downloadrecord');
  });

});
